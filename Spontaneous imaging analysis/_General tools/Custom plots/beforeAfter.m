function h = beforeAfter(inputVar,colour,varargin)
%BEFOREAFTER Before-after plot from a n-by-m array
%   n being the samples
%   m being the conditions/groups

% deal with missing entries
if ~exist('colour')
    colour = 'b';
end

n_samples = size(inputVar,1);
n_conditions = size(inputVar,2);

% plot lines
hold on;
for sampleCounter = 1:n_samples
    h = plot(inputVar(sampleCounter,:),'-o',...
    'Color',colour,...
    'LineWidth',1.5,...
    'MarkerSize',8,...
    'MarkerEdgeColor', colour);
    
    %h = plot(inputVar(sampleCounter,:),colour);
end



% plot points
%for conditionCounter = 1:n_conditions
%    scatter(ones(n_samples,1)*conditionCounter,inputVar(:,conditionCounter),colour)
%end

xlim([0.8 n_conditions+0.2])
xticks(1:size(inputVar,2))

% write conditions on x axis
condition = cell(n_conditions,1);
for conditionCounter =1:n_conditions
    if ~isempty(varargin)
        condition(conditionCounter) = {varargin{1,conditionCounter}};
    else
        condition(conditionCounter) = {conditionCounter};
    end
end
xticklabels(condition)
hold off;

end

