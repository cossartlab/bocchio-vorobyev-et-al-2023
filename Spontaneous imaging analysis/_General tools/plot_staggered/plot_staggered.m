function [] = plot_staggered(data,time,chan_list,scale,spacing,h)
% plot_stagged(data,time,chan_list,scale,spacing,h)
%
%plot <time> V.S. each row of the matrix <data> and arrange them vertically
%in a staggered way. If a <chan_list> is provided each channel has its own
%label (on the right), otherwise an automatic (and progressive) labeling is
%provided. The parameter <scale> is a scaling factor, the <spacing>
%parameter is the space between each row-plot and <h> is  Note that only <data>
%argument is necessary: the others are automatically estimate if not
%provided.
% INPUT:    <data>: matrix with N rows and M columns;
%           <time>: vector with M elements;
%           <chan_list>: cell array with N elements;
%           <scale>: nonzero number;
%           <spacing>: non-negative number;
%           <h>: axes;
%
%%Author:       Marco Bilucaglia, M.Sc.Eng. (Electronics)
%Contacts:      m.bilucaglia@ieee.org
%Update:        (1) 05/18/2018
[channels,samples]=size(data);
if nargin<6
    h=axes;
end
if nargin<5
    spacing=1.8;
end
if nargin <4
    scale=3*max(std(data,1,2));
end
if nargin<3
    chan_list=num2str((1:channels),'-#%.0f@');
    chan_list=chan_list(~isspace(chan_list));
    chan_list=strsplit(chan_list,'@');
    chan_list(end)=[];
    chan_list=regexprep(chan_list,'-','');
end
    
if nargin<2
    time=(1:samples)';
end
data=flipud(data); %1row on top of the plot
data_mean=(mean(data,2));
data=bsxfun(@minus,data,data_mean); %subtraction of the mean value, so signal are centered to zero
data=data/scale; %scaling, so data "limits" are +/scale
vec=(2:spacing+2:(channels*(spacing+2)))'; %zeros of each channel is mapped into integers w.r.t. the spacing
data=bsxfun(@plus, data,vec);
plot(h,time,(flipud(data))','-');
ylim(h,[vec(1)-2, vec(end)+2]);
xlim(h,[time(1), time(end)]);
vec_temp=sort([vec; (vec-1); (vec+1)],'ascend');
h.YTick=vec_temp;
h.YTickLabel=reshape(num2cell(scale*[-1 0 +1]+data_mean)',[],1);
h.YGrid='on';
h.GridLineStyle='-';
h.TickDir='both';
h.YColor=[0.15 0.15 0.15];
h.TickLength=[0.002 0.002];
color_order=get(h,'colororder');
[r,~]=size(color_order);
n_copies=ceil(channels/r);
color_order=repmat(color_order,n_copies,1);
color_order(channels+1:end,:)=[];
for i=1:channels
    text(samples,vec(channels-i+1),sprintf('  %s',chan_list{i}),'HorizontalAlignment','left','color',color_order(i,:));
end
end