# README
This README file provides an overview of the code's functionality, its main components, and how to use it for different types of neural network simulations and analyses. 
It simulates the effect of single inhibitory neuron perturbations in networks of excitatory-inhibitory neurons with various architectures. 

See the corresponding manuscript here: 
Functional networks of inhibitory neurons orchestrate synchrony in the hippocampus
https://www.biorxiv.org/content/10.1101/2023.09.18.558335v1

## Usage:
Run the script pert_experiments.m to see the results of single inhibitory perturbations.
To use this code, modify the 'structure_type' and 'simulation_type' variables to choose the desired network structure and simulation method. Then run the code to generate the specified simulations and figures.
Feel free to customize the code for your specific neural network simulations and analyses.

## Simulation Types:
The code offers different types of network structures and perturbations, including:
1. `structure_type = 'default_params'`: Simulates single interneuron perturbations with default connectivity parameters. It has subnetwork structure within E-E and E-I connections, but no specific connectivity within I-I connections.
2. `structure_type = 'no_subnetworks'`: Simulates single interneuron perturbations without subnetwork structure in any connection type.
3. `structure_type = 'less_global_II'`: Simulates single interneuron perturbations with less global inhibitory-inhibitory (I-I) connectivity, making it less dense and more specific.
4. `structure_type = 'denser_specific_II'`: Simulates single interneuron perturbations with denser inhibitory-inhibitory (I-I) connectivity, while maintaining specificity.

The effect of single inhibitory neuron perturbations can be simulated within each structure, either by simulating the rate based dynamics of the network (simulation_type = 'rate_based'), or by analyzing the linear dynamics of the rate-based network from the weight matrix (simulation_type = 'LA_weights'). 

Choose the combination of structure_type and simulation_type that you want to see and run the code to see the results and the corresponding figure, which illustrates histograms of resulting response changes in other excitatory and inhibitory neurons. 

## Functions:

### conn_matrix.m
This function calculates the connectivity matrix 'w' for a neural network with specified parameters, including the number of excitatory and inhibitory neurons, connection probabilities, and connection specificity.

### net_pert_RBS.m
This code simulates the response of a neural network to perturbations in input currents based on rate-based simulations of network activity. It calculates the differences in response rates between the perturbed and baseline conditions for a range of perturbed single inhibitory neurons.

### net_pert_LAW.m
This code calculates the differences in response for a neural network based on its connectivity matrix 'w'. It uses linear analysis of network dynamics based on the weight matrix to compute these differences.

### sim_net_rate_based.m
This code simulates the responses of a network of rate-based units over time, given the input to 'neurons and their specified weight matrix.

### plot_figure.m
This code illustrates histograms of response changes for excitatory and inhibitory neurons.
