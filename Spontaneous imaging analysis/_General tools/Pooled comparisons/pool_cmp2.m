function pool_cmp2 (variable, subVariable , path, group1, group2)

%path = 'E:\GAD67 results\Recordings\';
%variable = 'pair_corr';
%subVariable = 'tot';
%group1 = 'pyr';
%group2 = 'int';

files = dir(path);
fileNames = {files.name};
fileIsDir = {files.isdir};
fileNames = fileNames(cell2mat(fileIsDir)==0); %size of files in MB

data1 = zeros(1,length(fileNames));
data2 = zeros(1,length(fileNames));

for fileCounter = 1:length(fileNames)
    loadedVar = load([path fileNames{fileCounter}],variable);
    if isempty(subVariable)
        data1(fileCounter) = median(loadedVar.(variable).(group1));
        data2(fileCounter) = median(loadedVar.(variable).(group2));  
    else
        data1(fileCounter) = median(loadedVar.(variable).(subVariable).(group1));
        data2(fileCounter) = median(loadedVar.(variable).(subVariable).(group2));  
    end
end

arrange_boxplot(data1,data2)
xticklabels({group1,group2})
p = ranksum(data1,data2);
disp([subVariable ' ' group1 ' vs ' group2])
disp(['p = ' num2str(p)])

end