
[Patterns] = extract_variables ('ICA_results',[],resultsPath, 'Patterns',[],conditionType,conditionValue);
[int_id,pyr_id] = extract_variables ('nCells',[],resultsPath, 'int','pyr',conditionType,conditionValue);


int_weights = zeros(1,size(Patterns,2));
pyr_weights = zeros(1,size(Patterns,2));
int_weights_clust_rec = zeros(1,size(Patterns,2));
pyr_weights_clust_rec = zeros(1,size(Patterns,2));

% pyr vs int comparisons
for recCounter = 1:size(Patterns,2)
int_weight = 0;
pyr_weight = 0;
int_weight_clust_rec = 0;
pyr_weight_clust_rec = 0;

    for assemblyCounter = 1:size(Patterns{recCounter},2)
        int_weight = int_weight + mean(Patterns{recCounter}(int_id{recCounter},assemblyCounter));
        pyr_weight = pyr_weight + mean(Patterns{recCounter}(pyr_id{recCounter},assemblyCounter));
        int_weight_clust_rec = int_weight_clust_rec + max(Patterns{recCounter}(int_id{recCounter},assemblyCounter))/abs(mean(Patterns{recCounter}(int_id{recCounter},assemblyCounter)));
        pyr_weight_clust_rec = pyr_weight_clust_rec + max(Patterns{recCounter}(pyr_id{recCounter},assemblyCounter))/abs(mean(Patterns{recCounter}(pyr_id{recCounter},assemblyCounter)));
        
        int_weights_clust_neuron{recCounter}(assemblyCounter) = max(Patterns{recCounter}(int_id{recCounter},assemblyCounter))/abs(mean(Patterns{recCounter}(int_id{recCounter},assemblyCounter)));
        pyr_weights_clust_neuron{recCounter}(assemblyCounter) =  max(Patterns{recCounter}(pyr_id{recCounter},assemblyCounter))/abs(mean(Patterns{recCounter}(pyr_id{recCounter},assemblyCounter)));
    end
int_weights(recCounter) = int_weight/size(Patterns{recCounter},2);
pyr_weights(recCounter) = pyr_weight/size(Patterns{recCounter},2);
int_weights_clust_rec(recCounter) = int_weight_clust_rec/size(Patterns{recCounter},2);
pyr_weights_clust_rec(recCounter) = pyr_weight_clust_rec/size(Patterns{recCounter},2);
end

int_weights_clust_neuron2 = cell2mat(int_weights_clust_neuron);
pyr_weights_clust_neuron2 = cell2mat(pyr_weights_clust_neuron);

figure
arrange_boxplot(pyr_weights,int_weights)
title('Assembly weight')
xticklabels({'Pyr','Int'})
ylabel('Mean weight')
p = ranksum(pyr_weights,int_weights);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

figure
mdl = fitlm(int_weights,pyr_weights);
plot(mdl)
%disp(['p = ' num2str(coefTest(mdl))])
%disp(['R2 = ' num2str(mdl.Rsquared.Adjusted)])
xlabel('Int weights')
ylabel('Pyr weights')
text(0.45,0.9,['p = ' num2str(coefTest(mdl)), ' R2 = ' num2str(mdl.Rsquared.Adjusted) ],'Units','normalized')

figure
arrange_boxplot(pyr_weights_clust_rec,int_weights_clust_rec)
title('Weight clustering by recording')
ylabel('max weights / abs mean weight')

xticklabels({'Pyr','Int'})
p = ranksum(pyr_weights_clust_rec,int_weights_clust_rec);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

figure
arrange_boxplot(pyr_weights_clust_neuron2,int_weights_clust_neuron2)
title('Weight clustering by assembly')
ylabel('max weights / abs mean weight')
xticklabels({'Pyr','Int'})
p = ranksum(pyr_weights_clust_neuron2,int_weights_clust_neuron2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')




