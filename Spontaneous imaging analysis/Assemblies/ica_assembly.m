%Tr = traces.all(:,mvm.restEpochsIndex);
Tr = traces.all;


clear opts;
opts.threshold.permutations_percentile = 95;
opts.threshold.number_of_permutations = 1000;
opts.Patterns.number_of_iterations = 500;
opts.threshold.method = 'circularshift';
opts.Patterns.method = 'ICA';

% Assembly patterns
ICA_results.Patterns = assembly_patterns(Tr,opts);
ICA_results.Activities = assembly_activity(ICA_results.Patterns,Tr);
ICA_results.nAssemblies = size(ICA_results.Patterns,2);

% Plot assembly weights
for assemblyCounter = 1:ICA_results.nAssemblies
subplot(ICA_results.nAssemblies,1,assemblyCounter)
stem(ICA_results.Patterns(:,assemblyCounter))
end

% Plot assembly activities
figure
subplot(211)
imagesc(Tr,[0 prctile(max(Tr,[],2),90)])
colormap(gray)
xlim([0 length(Tr)])
subplot(212)
plot(ICA_results.Activities')
xlim([0 length(Tr)])
legend