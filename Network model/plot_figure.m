% - figure

dth = 0;

% Extract response data for excitatory (z1) and inhibitory (z2) neurons
z1 = dr_all(1:NE, :);
z1 = reshape(z1, 1, []);

z2 = dr_all(1+NE:end, :);
z2 = reshape(z2, 1, []);

figure('Position',[100,100,250,450])

subplot(211);
title('exc'); hold on

% Plot the histogram bars for positive and negative changes in excitatory neurons
bar([1], [sum(z1 > dth) / length(z1)] * 100, 'b')
bar([2], [sum(z1 < -dth) / length(z1)] * 100, 'r')

xticks([1,2])
xticklabels({'Pos.', 'Neg.'})
ylabel('%')
set(gca, 'LineWidth', 1, 'FontSize', 15, 'TickDir', 'out', 'TickLength', [.01, .01])

% Create the second subplot for inhibitory neurons
subplot(212);
title('inh'); hold on

% Plot the histogram bars for positive and negative changes in inhibitory neurons
b = bar([1], [sum(z2 > dth) / length(z2)] * 100, 'b');
b = bar([2], [sum(z2 < -dth) / length(z2)] * 100, 'r');

xticks([1,2])
xticklabels({'Pos.', 'Neg.'})
xlabel('changes')
ylabel('%')
set(gca, 'LineWidth', 1, 'FontSize', 15, 'TickDir', 'out', 'TickLength', [.01, .01])
