% Define a function 'sim_net' that simulates the response of a neural network
% to a given input 'I' over time, using the specified connectivity matrix 'w',
% time constant 'tau', and time step 'dt'.
function [r_all] = sim_net(I, w, tau, dt)

    % Determine the number of neurons in the network
    nn = length(w);

    % Determine the number of time steps in the input 'I'
    nt = size(I, 2);
    
    % Initialize the firing rates of neurons ('r') and a matrix to store all rates over time ('r_all')
    r = zeros(1, nn);
    r_all = zeros(nn, nt); 
    
    % Loop through time steps (from 1 to nt-1)
    for i = 1:nt-1
        
        % Calculate the recurrent input received by each neuron ('inp_rec')
        inp_rec = r * w;
        
        % Extract the external input at the current time step ('inp_ffw')
        inp_ffw = I(:, i)';
        
        % Calculate the change in firing rate ('dr') for each neuron at this time step
        dr = dt / tau * (-r + rectify(inp_rec + inp_ffw));
        
        % Update the firing rates for all neurons
        r = r + dr;
        
        % Store the firing rates for this time step in the 'r_all' matrix
        r_all(:, i+1) = r;
    end
end

% Define a function 'rectify' that implements rectification (ReLU) for its input 'z'.
function [zr] = rectify(z)
    zr = z .* (z > 0);
end
