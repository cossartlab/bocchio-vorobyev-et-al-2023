import sys

import matplotlib.pyplot as plt
import numpy as np

sys.path.append('/Users/anaconda/Desktop/Code/EC')
import experiments_code_s2p as ec
import pandas as pd
import seaborn as sns
from sklearn.linear_model import LinearRegression
from scipy.stats import pearsonr, ttest_ind, mannwhitneyu, kstest
from cmath import isnan
from pathlib import Path
from itertools import chain
from datetime import datetime


data_path = '/Volumes/2TB_VIRUS/Data/'

## CONTROLS
#e_l = ec.load_experiments('controls')
#SAVE_TO = f'SCE_freq_controls/{datetime.now().strftime("%Y%m%d_%H%M%S")}'
#response_range_l = [[0, 10]]

exp_type = 'main'
response_range_l = [[0,0], [0,1], [0, 2], [3, 7], [6,10], [7,10], [8, 10], [0, 10], [5, 10], [2, 10], [3, 10], [4, 10], [1,10]]

#exp_type = 'controls'
#response_range_l = [[0, 10]]
e_l = ec.load_experiments(exp_type)

#e_l = [e for e in e_l if sum(e.iscell) >= 100]

SAVE_TO = f'SCE_ampl_full_analysis/{datetime.now().strftime("%Y%m%d_%H%M%S")}_{exp_type}'

MIN_SCE_PER_EPOCH = 5
SPEED_THR = 2
single_cell_stim_only = True

### Options
#SCE_thr = '5cells'  # should be 5cells or 5percent

SCE_thr_l = ['5percent', '5cells']
for SCE_thr in SCE_thr_l:
    for response_range in response_range_l:
        title = f"{SCE_thr},{response_range}"
        print(title)
        selected_exp = [i for i in e_l if sum(i.response) in range(response_range[0],response_range[1]+1)]
        if single_cell_stim_only:
            selected_exp = [i for i in selected_exp if i.stim_cell_number_l.size == 1]

        # Making the directories
        Path(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/single_exp/cell_proportions').mkdir(parents=True, exist_ok=True)
        Path(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/single_exp/cell_n').mkdir(parents=True, exist_ok=True)
        Path(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/single_exp/ecdf').mkdir(parents=True, exist_ok=True)

        filtered_exp, filtered_cell_n, filtered_cell_prop, filtered_frames, baseline_sce, stim_sce, post_stim_sce, \
            baseline_sce_fr, stim_sce_fr, post_stim_sce_fr, baseline_sce_amp, stim_sce_amp, post_stim_sce_amp = \
            ec.SCE_data_from_excel(
            e_l = selected_exp,
            #excel_path = "/Volumes/2TB_VIRUS/new_NWB_s2p/SCE_recruitment_table_all.xlsx",
            excel_path = '/Volumes/2TB_VIRUS/even_newer_NWB/SCEs_Cicada.xlsx',
            min_SCE_per_epoch = MIN_SCE_PER_EPOCH,
            cells_SCE_thr = SCE_thr,
            speed_thr = SPEED_THR
            )

        print(f'Selected experiments:{[i.exp_id for i in filtered_exp]}')
        if not filtered_exp:
            continue

        bl_mean_amp = [np.mean(i) for i in baseline_sce_amp]
        stim_mean_amp = [np.mean(i) for i in stim_sce_amp]
        ps_mean_amp = [np.mean(i) for i in post_stim_sce_amp]


        # Plot the linear graph, cell n
        for n, e in enumerate(filtered_exp):
            to_plot = np.zeros(15000)
            fig, axs = plt.subplots(1,1)
            fig.set_size_inches(14, 10)
            fig.suptitle(f'{e.exp_id}, responses number: {sum(e.response)}')

            for fr, cells_n in zip(filtered_frames[n], filtered_cell_n[n]):
                if fr <= e.stim_first_frames[0]:
                    to_plot[fr] = cells_n
            plt.plot(to_plot, c='black')
            to_plot = np.zeros(15000)
        
            for fr, cells_n in zip(filtered_frames[n], filtered_cell_n[n]):
                if fr > e.stim_first_frames[0] and fr <= e.stim_first_frames[-1] + 100:
                    to_plot[fr] = cells_n
            plt.plot(to_plot, c='red')
            to_plot = np.zeros(15000)

            for fr, cells_n in zip(filtered_frames[n], filtered_cell_n[n]):
                if fr > e.stim_first_frames[-1] + 100:
                    to_plot[fr] = cells_n
            plt.plot(to_plot, c='gray')

            #plt.vlines(e.stim_first_frames,  0, max(filtered_cell_n[n]) + 5)
            plt.scatter(x=e.stim_first_frames, y=[max(filtered_cell_n[n]) + 5]*len(e.stim_first_frames), marker='v', c='red')
            fig.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/single_exp/cell_n/{e.exp_id}.svg')
            plt.close()

        # Plot the linear graph, cell proportions
        for n, e in enumerate(filtered_exp):
            to_plot = np.zeros(15000)
            fig, axs = plt.subplots(1,1)
            fig.set_size_inches(14, 10)
            fig.suptitle(f'{e.exp_id}, responses number: {sum(e.response)}')

            for fr, cells_prop in zip(filtered_frames[n], filtered_cell_prop[n]):
                if fr <= e.stim_first_frames[0]:
                    to_plot[fr] = cells_prop
            plt.plot(to_plot, c='black')
            to_plot = np.zeros(15000)
        
            for fr, cells_prop in zip(filtered_frames[n], filtered_cell_prop[n]):
                if fr > e.stim_first_frames[0] and fr <= e.stim_first_frames[-1] + 100:
                    to_plot[fr] = cells_prop
            plt.plot(to_plot, c='red')
            to_plot = np.zeros(15000)

            for fr, cells_prop in zip(filtered_frames[n], filtered_cell_prop[n]):
                if fr > e.stim_first_frames[-1] + 100:
                    to_plot[fr] = cells_prop
            plt.plot(to_plot, c='gray')

            #plt.vlines(e.stim_first_frames,  0, max(filtered_cell_n[n]) + 5)
            plt.scatter(x=e.stim_first_frames, y=[max(filtered_cell_prop[n]) + 5]*len(e.stim_first_frames), marker='v', c='red')
            fig.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/single_exp/cell_proportions/{e.exp_id}.svg')
            plt.close()

        # Plot the edcf, single experiments
        for n, e in enumerate(filtered_exp):

            df = pd.DataFrame([baseline_sce_amp[n], stim_sce_amp[n], post_stim_sce_amp[n]]).T
            df.columns = ['Baseline', 'Stimulation', 'Post-stim']
            fig, axs = plt.subplots(1,1)
            #fig.set_size_inches(14, 10)
            sns.ecdfplot(df, legend=True, palette=['black', 'red', 'gray'])
            try:
                u_b_vs_s = round(mannwhitneyu(baseline_sce_amp[n],  stim_sce_amp[n])[1], 5)
                plt.text(0.95, 0.65, f'U-test: B vs S: {u_b_vs_s}', transform=plt.gca().transAxes, ha='right', fontsize=12)
                u_b_vs_p = round(mannwhitneyu(baseline_sce_amp[n],  post_stim_sce_amp[n])[1], 5)
                plt.text(0.95, 0.6, f'U-test: B vs P: {u_b_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
                u_s_vs_p = round(mannwhitneyu(stim_sce_amp[n],  post_stim_sce_amp[n])[1], 5)
                plt.text(0.95, 0.55, f'U-test: S vs P: {u_s_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)

                ks_b_vs_s = round(kstest(baseline_sce_amp[n],  stim_sce_amp[n])[1], 5)
                plt.text(0.95, 0.45, f'KS-test: B vs S: {ks_b_vs_s}', transform=plt.gca().transAxes, ha='right', fontsize=12)
                ks_b_vs_p = round(kstest(baseline_sce_amp[n],  post_stim_sce_amp[n])[1], 5)
                plt.text(0.95, 0.4, f'KS-test: B vs P: {ks_b_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
                ks_s_vs_p = round(kstest(stim_sce_amp[n],  post_stim_sce_amp[n])[1], 5)
                plt.text(0.95, 0.35, f'KS-test: S vs P: {ks_s_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)

            except ValueError as err:
                print(f'No text here {e.exp_id}, {err}')
            
            fig.suptitle(f'{e.exp_id}, responses number: {sum(e.response)}')
            fig.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/single_exp/ecdf/{e.exp_id}.svg')
            plt.close()


        # Plot the edcf, pulled data

        df = pd.DataFrame([
            list(chain.from_iterable(baseline_sce_amp)),
            list(chain.from_iterable(stim_sce_amp)),
            list(chain.from_iterable(post_stim_sce_amp))
            ]).T
        df.columns = ['Baseline', 'Stimulation', 'Post-stim']

        fig, axs = plt.subplots(1,1)

        sns.ecdfplot(df, legend=True, palette=['black', 'red', 'gray'])
        try:
            u_b_vs_s = round(mannwhitneyu(list(chain.from_iterable(baseline_sce_amp)),  list(chain.from_iterable(stim_sce_amp)))[1], 5)
            plt.text(0.95, 0.65, f'U-test: B vs S: {u_b_vs_s}', transform=plt.gca().transAxes, ha='right', fontsize=12)
            u_b_vs_p = round(mannwhitneyu(list(chain.from_iterable(baseline_sce_amp)),  list(chain.from_iterable(post_stim_sce_amp)))[1], 5)
            plt.text(0.95, 0.60, f'U-test: B vs P: {u_b_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
            u_s_vs_p = round(mannwhitneyu(list(chain.from_iterable(stim_sce_amp)),  list(chain.from_iterable(post_stim_sce_amp)))[1], 5)
            plt.text(0.95, 0.55, f'U-test: S vs P: {u_s_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)

            ks_b_vs_s = round(kstest(list(chain.from_iterable(baseline_sce_amp)),  list(chain.from_iterable(stim_sce_amp)))[1], 5)
            plt.text(0.95, 0.45, f'KS-test: B vs S: {ks_b_vs_s}', transform=plt.gca().transAxes, ha='right', fontsize=12)
            ks_b_vs_p = round(kstest(list(chain.from_iterable(baseline_sce_amp)),  list(chain.from_iterable(post_stim_sce_amp)))[1], 5)
            plt.text(0.95, 0.40, f'KS-test: B vs P: {ks_b_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
            ks_s_vs_p = round(kstest(list(chain.from_iterable(stim_sce_amp)),  list(chain.from_iterable(post_stim_sce_amp)))[1], 5)
            plt.text(0.95, 0.35, f'KS-test: S vs P: {ks_s_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        except ValueError as err:
            print(f'No text here {filtered_exp[n].exp_id}, {err}')

        fig.suptitle(f'{title}')
        fig.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/ecdf_comb.svg')
        plt.close()

        # Boxplots for inter SCEs, data pulled
        # Dirty version
        '''
        my_dict = {
            f'Baseline, n={len(list(chain.from_iterable(baseline_sce_amp)))}': list(chain.from_iterable(baseline_sce_amp)),
            f'Stimulation, n={len(list(chain.from_iterable(stim_sce_amp)))}': list(chain.from_iterable(stim_sce_amp)),
            f'Post-stim, n={len(list(chain.from_iterable(post_stim_sce_amp)))}': list(chain.from_iterable(post_stim_sce_amp)),
            }
        fig, ax = plt.subplots()
        fig.set_size_inches(8, 6)
        bp_dict = ax.boxplot(my_dict.values(), showfliers=False)

        for line, mean in zip(bp_dict['medians'], [np.mean(list(chain.from_iterable(baseline_sce_amp))), np.mean(list(chain.from_iterable(stim_sce_amp))), np.mean(list(chain.from_iterable(post_stim_sce_amp)))]):
            # get position data for median line
            x, y = line.get_xydata()[1] # top of median line
            # overlay median value
            ax.text(x+0.03, y, f'Median:{round(y, 2)}\nMean:{round(mean, 2)}',
                ha='left', va='center', fontsize=10) # draw above, centered

        ax.set_xlim(0.5, 3.7)
        ax.set_ylabel('Number of cells in SCE')
        ax.set_xticklabels(my_dict.keys())
        fig.suptitle(f'SCE amplitude pulled data\n{title}\nbaseVSstim U-test: {[round(i,8) for i in mannwhitneyu(list(chain.from_iterable(baseline_sce_amp)), list(chain.from_iterable(stim_sce_amp)))]}')
        plt.tight_layout()
        #plt.style.use(['dark_background'])
        fig.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/boxplot.svg')
        plt.close()
        '''

        # Clean version
        #plt.style.use(['dark_background'])
        baseline_sce_amp_prcnt = []
        stim_sce_amp_prcnt = []
        post_stim_sce_amp_prcnt = []
        for ei, b, s, p in zip(filtered_exp, baseline_sce_amp, stim_sce_amp, post_stim_sce_amp):
            baseline_sce_amp_prcnt.append(100*np.array(b) / sum(ei.iscell))
            stim_sce_amp_prcnt.append(100*np.array(s) / sum(ei.iscell))
            post_stim_sce_amp_prcnt.append(100*np.array(p) / sum(ei.iscell))

        my_dict = {
            f'Baseline': list(chain.from_iterable(baseline_sce_amp_prcnt)),
            f'Stimulation': list(chain.from_iterable(stim_sce_amp_prcnt)),
            f'Post-stim': list(chain.from_iterable(post_stim_sce_amp_prcnt)),
            }
        fig, ax = plt.subplots()
        fig.set_size_inches(5,8)
        bp_dict = ax.boxplot(my_dict.values(), showfliers=False)

        for line, mean in zip(bp_dict['medians'], [np.mean(list(chain.from_iterable(baseline_sce_amp_prcnt))), np.mean(list(chain.from_iterable(stim_sce_amp_prcnt))), np.mean(list(chain.from_iterable(post_stim_sce_amp_prcnt)))]):
            # get position data for median line
            x, y = line.get_xydata()[1] # top of median line
            # overlay median value
            ax.text(x+0.03, y, f'Median:{round(y, 2)}\nMean:{round(mean, 2)}',
                ha='left', va='center', fontsize=10) # draw above, centered

        ax.set_xlim(0.5, 3.7)
        ax.set_ylabel('Fraction of cells in SCE, %')
        ax.set_xticklabels(my_dict.keys())
        fig.suptitle(f'SCE amplitude pulled data\n{title}\nbaseVSstim U-test: {[round(i,8) for i in mannwhitneyu(list(chain.from_iterable(baseline_sce_amp_prcnt)), list(chain.from_iterable(stim_sce_amp_prcnt)))]}')
        plt.tight_layout()
        #plt.style.use(['dark_background'])
        fig.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/boxplot.svg')
        plt.close()

        # No post-stim version
        baseline_sce_amp_prcnt = []
        stim_sce_amp_prcnt = []
        for ei, b, s in zip(filtered_exp, baseline_sce_amp, stim_sce_amp):
            baseline_sce_amp_prcnt.append(100*np.array(b) / sum(ei.iscell))
            stim_sce_amp_prcnt.append(100*np.array(s) / sum(ei.iscell))

        my_dict = {
            f'Baseline': list(chain.from_iterable(baseline_sce_amp_prcnt)),
            f'Stimulation': list(chain.from_iterable(stim_sce_amp_prcnt)),
        }
        fig, ax = plt.subplots()
        fig.set_size_inches(2.55, 3.29)
        bp_dict = ax.boxplot(my_dict.values(),  widths = 0.3, showfliers=True)

        #ax.set_xlim(0.5, 3.7)
        ax.set_ylabel('SCE amplitude\n(% active)')
        plt.locator_params(axis='y', nbins=3)
        ax.set_xticklabels(my_dict.keys())
        fig.suptitle(f'SCE amplitude pulled data\n{title}\nbaseVSstim U-test: {[round(i,8) for i in mannwhitneyu(list(chain.from_iterable(baseline_sce_amp_prcnt)), list(chain.from_iterable(stim_sce_amp_prcnt)))]}')
        plt.tight_layout()
        #plt.style.use(['dark_background'])
        fig.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/boxplot.svg')
        plt.close()

        # LinReg, stim-baseline VS baseline
        change = np.array(stim_mean_amp) - np.array(bl_mean_amp)
        #filter out nan s
        baseline_l_filtered = []
        change_filtered = []
        response_n_filtered = []
        n = 0
        for a, b in zip(bl_mean_amp, change):
            if not isnan(a) and not isnan(b):
                baseline_l_filtered.append(a)
                change_filtered.append(b)
                response_n_filtered.append(sum(selected_exp[n].response))
                n+=1

        baseline_sceT_filtered = np.array(baseline_l_filtered).reshape((-1, 1))
        reg = LinearRegression().fit(baseline_sceT_filtered, change_filtered)
        r2 = reg.score(baseline_sceT_filtered, change_filtered)
        pearson_r, pearson_p = pearsonr(baseline_l_filtered, change_filtered)
        plt.axhline(y=0, xmin=0, xmax=1, c='red', ls='dashed', alpha=0.7)
        sns.regplot(x=baseline_l_filtered, y=change_filtered,  x_jitter=.1, y_jitter=.1).set(title=f'Stim SCE amp - Baseline SCE amp VS Baseline SCE amp\n{title}\nPearson r,p:{[round(i,5) for i in pearsonr(baseline_l_filtered, change_filtered)]}')
        plt.xlabel('Mean Baseline SCE amplitude')
        plt.ylabel('Mean Baseline SCE amplitude - \n Mean Stim SCE amplitude')
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/LinReg.svg')
        plt.close()

        # Violine plot version
        #plt.style.use(['dark_background'])
        baseline_sce_amp_prcnt = []
        stim_sce_amp_prcnt = []
        post_stim_sce_amp_prcnt = []
        for ei, b, s, p in zip(filtered_exp, baseline_sce_amp, stim_sce_amp, post_stim_sce_amp):
            baseline_sce_amp_prcnt.append(100*np.array(b) / sum(ei.iscell))
            stim_sce_amp_prcnt.append(100*np.array(s) / sum(ei.iscell))
            post_stim_sce_amp_prcnt.append(100*np.array(p) / sum(ei.iscell))

        my_dict = {
            f'Baseline': list(chain.from_iterable(baseline_sce_amp_prcnt)),
            f'Stimulation': list(chain.from_iterable(stim_sce_amp_prcnt)),
            f'Post-stim': list(chain.from_iterable(post_stim_sce_amp_prcnt)),
            }

        df = pd.DataFrame([my_dict['Baseline'], my_dict['Stimulation'], my_dict['Post-stim']]).transpose()
        # Label the columns of the DataFrame
        df = df.set_axis(['Baseline','Stimulation','Post-stim'], axis=1)
        df.to_csv('/Users/anaconda/Desktop/Analysis results/to_tune_the_plot.csv')
        fig, ax = plt.subplots()
        fig.set_size_inches(7,8)
        my_pal = {"Baseline": "dimgrey", "Stimulation": "r", "Post-stim": "gainsboro"}
        sns.violinplot(data=df,palette=my_pal, cut=2)
        #sns.swarmplot(data=df)
        
        # Create DataFrame where NaNs fill shorter arrays 
        #bp_dict = ax.boxplot(my_dict.values(), showfliers=False)
        '''
        for line, mean in zip(bp_dict['medians'], [np.mean(list(chain.from_iterable(baseline_sce_amp_prcnt))), np.mean(list(chain.from_iterable(stim_sce_amp_prcnt))), np.mean(list(chain.from_iterable(post_stim_sce_amp_prcnt)))]):
            # get position data for median line
            x, y = line.get_xydata()[1] # top of median line
            # overlay median value
            ax.text(x+0.03, y, f'Median:{round(y, 2)}\nMean:{round(mean, 2)}',
                ha='left', va='center', fontsize=10) # draw above, centered
        '''
        #ax.set_xlim(0.5, 3.7)
        ax.set_ylabel('Fraction of cells in SCE, %')
        ax.set_xticklabels(my_dict.keys())
        ax.set(ylim=(3, 15))
        fig.suptitle(f'SCE amplitude pulled data\n{title}\nbaseVSstim U-test: {[round(i,8) for i in mannwhitneyu(list(chain.from_iterable(baseline_sce_amp_prcnt)), list(chain.from_iterable(stim_sce_amp_prcnt)))]}')
        plt.tight_layout()
        #plt.style.use(['dark_background'])
        fig.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/violineplot.svg')
        plt.close()

        # LinReg, stim-baseline VS baseline
        change = np.array(stim_mean_amp) - np.array(bl_mean_amp)
        #filter out nan s
        baseline_l_filtered = []
        change_filtered = []
        response_n_filtered = []
        n = 0
        for a, b in zip(bl_mean_amp, change):
            if not isnan(a) and not isnan(b):
                baseline_l_filtered.append(a)
                change_filtered.append(b)
                response_n_filtered.append(sum(selected_exp[n].response))
                n+=1

        baseline_sceT_filtered = np.array(baseline_l_filtered).reshape((-1, 1))
        reg = LinearRegression().fit(baseline_sceT_filtered, change_filtered)
        r2 = reg.score(baseline_sceT_filtered, change_filtered)
        pearson_r, pearson_p = pearsonr(baseline_l_filtered, change_filtered)
        plt.axhline(y=0, xmin=0, xmax=1, c='red', ls='dashed', alpha=0.7)
        sns.regplot(x=baseline_l_filtered, y=change_filtered,  x_jitter=.1, y_jitter=.1).set(title=f'Stim SCE amp - Baseline SCE amp VS Baseline SCE amp\n{title}\nPearson r,p:{[round(i,5) for i in pearsonr(baseline_l_filtered, change_filtered)]}')
        plt.xlabel('Mean Baseline SCE amplitude')
        plt.ylabel('Mean Baseline SCE amplitude - \n Mean Stim SCE amplitude')
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/LinReg.svg')
        plt.close()

        # LinReg, stim-baseline VS response
        response_n_filteredT = np.array(response_n_filtered).reshape((-1, 1))
        plt.axhline(y=0, xmin=0, xmax=1, c='red', ls='dashed', alpha=0.7)
        sns.regplot(x=response_n_filteredT, y=change_filtered,  x_jitter=.1, y_jitter=.1).set(title=f'Stim SCE amp - Baseline SCE amp VS Stim response number\n{title}\nPearson r,p:{[round(i,5) for i in pearsonr(response_n_filtered, change_filtered)]}')
        plt.xlabel('Responses number')
        plt.ylabel('Mean Baseline SCE amplitude - \n Mean Stim SCE amplitude')
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/changeVSresponse.svg')
        plt.close()

        # Arrows graph
        sorted_sces = sorted(zip(bl_mean_amp, stim_mean_amp), key=lambda x:abs(x[1] - x[0]), reverse=True)
        for x,y,dx in zip(
            [i[0] for i in sorted_sces], 
            np.arange(len(sorted_sces)), 
            [i[1] for i in sorted_sces]):
            #plt.arrow(x,y,x-dx,0,color='white')
            plt.annotate("", xy=(x, y), xytext=(dx, y), arrowprops=dict(arrowstyle="->"))
        plt.xlim(np.min(sorted_sces), np.max(sorted_sces))
        plt.ylim(-1, y+1)
        plt.xlabel('SCEs amplitude')
        plt.ylabel('Experiment number')
        plt.axvline(x=np.mean(bl_mean_amp), ymin=0, ymax=y+5, c='red', ls='dashed', alpha=0.7)
        plt.title(f'Change of SCEs amplitude from Baseline to Stim\n{title}')
        plt.gcf().set_size_inches(8,6)
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/changeArrows.svg')
        plt.close()

        # Baseline to stim change with t-test
        stim_minus_bl_mean = np.array(stim_mean_amp) - np.median(bl_mean_amp)
        plt.hist(stim_minus_bl_mean, bins=100);
        t = ttest_ind(
            [i for i in stim_minus_bl_mean if i > 0],
            [i for i in stim_minus_bl_mean if i < 0], alternative='greater')
        plt.gcf().suptitle(f'Independent t-test between neg change and pos change {title}\nalt.hypothesis: mean distr of pos modulation is greater:\n{t}')
        plt.gcf().set_size_inches(8,6)
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/change_ttest.svg')
        plt.close()