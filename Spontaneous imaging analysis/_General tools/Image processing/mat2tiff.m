function mat2tiff(fileName, Y, frameStart,frameEnd)

tic;

%write movie to tiff

if nargin == 2
    frameStart = 1;
    frameEnd = size(Y,3);
end

%SS = fileName;
%prefix='MotC_';

milestones = 0.1:0.1:1;

disp('Saving as TIFF...')

for frameCounter=frameStart:frameEnd
    progress = frameCounter/frameEnd;
    if ismember(progress,milestones)
        disp([num2str(progress*100) '% of frames saved'])
    end
        %disp(num2str(frameCounter));
    imwrite(uint16(Y(:,:,frameCounter)), strcat(fileName,'.tif'),'TIFF','writemode', 'append');
       
end

toc;


end