% Tot pyr
figure
[data] = extract_variables ('pair_corr','tot',resultsPath, 'pyr');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(331)
arrange_boxplot(data1,data2)
title('Whole recording pyr')
xticklabels({'deep','sup'})
ylabel('Corr')
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

%Rest pyr
[data] = extract_variables ('pair_corr','rest',resultsPath, 'pyr');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(332)
arrange_boxplot(data1,data2)
title('Rest pyr')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

%Loc pyr
[data] = extract_variables ('pair_corr','mvm',resultsPath, 'pyr');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(333)
arrange_boxplot(data1,data2)
title('Loc pyr')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Tot int
[data] = extract_variables ('pair_corr','tot',resultsPath, 'int');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(334)
arrange_boxplot(data1,data2)
title('Whole recording int')
xticklabels({'deep','sup'})
ylabel('Corr')
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

%Rest int
[data] = extract_variables ('pair_corr','rest',resultsPath, 'int');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(335)
arrange_boxplot(data1,data2)
title('Rest int')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

%Loc int
[data] = extract_variables ('pair_corr','mvm',resultsPath, 'int');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(336)
arrange_boxplot(data1,data2)
title('Loc int')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Tot Pyr-int
[data] = extract_variables ('pair_corr','tot',resultsPath, 'pyr_int');
data_mdn = cell2mat(data);
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(337)
arrange_boxplot(data1,data2)
title('Whole recording pyr-int')
xticklabels({'Deep','Sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Rest Pyr-int
[data] = extract_variables ('pair_corr','rest',resultsPath, 'pyr_int');
data_mdn = cell2mat(data);
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(338)
arrange_boxplot(data1,data2)
title('Rest pyr-int')
xticklabels({'Deep','Sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Loc Pyr-int
[data] = extract_variables ('pair_corr','mvm',resultsPath, 'pyr_int');
data_mdn = cell2mat(data);
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(339)
arrange_boxplot(data1,data2)
title('Locomotion pyr-int')
xticklabels({'Deep','Sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')