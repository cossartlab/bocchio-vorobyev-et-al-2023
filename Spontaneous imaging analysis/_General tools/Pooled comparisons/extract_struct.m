function [outputStruct] = extract_struct (struct, path, conditionType, conditionValue)

files = dir(path);
fileNames = {files.name};
fileIsDir = {files.isdir};
fileNames = fileNames(cell2mat(fileIsDir)==0); %size of files in MB



for fileCounter = 1:length(fileNames)
    % extract only if recording belongs to certain condition
    if exist('conditionType')
        loadedCond = load([path fileNames{fileCounter}],conditionType);
        if ~strcmp(loadedCond.(conditionType),conditionValue)
            continue
        end
    end
    loadedStruct = load([path fileNames{fileCounter}],struct);
    structName = fieldnames(loadedStruct);
    structName = structName{1};
    outputStruct = loadedStruct.(structName);

    
end