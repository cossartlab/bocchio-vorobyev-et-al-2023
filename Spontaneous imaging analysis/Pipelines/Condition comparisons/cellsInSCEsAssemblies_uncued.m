
% SCEs
[pyrInSCE,intInSCE] = extract_variables ('SCE','cellsInSCE',resultsPath, 'pyr', 'int',conditionType,conditionValue);
[pyr_id,int_id] = extract_variables ('nCells',[],resultsPath, 'pyr', 'int',conditionType,conditionValue);
for recCounter = 1:size(pyrInSCE,2)
    data1_mdn(recCounter) = length(pyrInSCE{recCounter})/length(pyr_id{recCounter});
    data2_mdn(recCounter) = length(intInSCE{recCounter})/length(int_id{recCounter});
end
figure
%subplot(121)
arrange_boxplot(data1_mdn,data2_mdn)
p = ranksum(data1_mdn,data2_mdn);
title('Cells in SCEs')
xticklabels({'pyr','int'})
ylabel('Fraction of cells')
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

clearvars -except resultsPath conditionType conditionValue

% Assemblies
[CellCl] = extract_variables ('assemblies',[],resultsPath, 'CellCl',[],conditionType,conditionValue);
[pyr_id,int_id] = extract_variables ('nCells',[],resultsPath, 'pyr', 'int',conditionType,conditionValue);

% find recordings with no assemblies
noAssemblyRec = cell2mat(cellfun(@length,CellCl,'UniformOutput',false));
noAssemblyRec = noAssemblyRec==1;

% remove recordings without assemblies
CellCl(noAssemblyRec)=[];

for recCounter = 1:size(CellCl,2)
    cellsInAssemblies = find(sum(CellCl{recCounter},1));
    n_pyr_assemblies = length(cellsInAssemblies(cellsInAssemblies>max(int_id{recCounter})));
    n_int_assemblies = length(cellsInAssemblies(cellsInAssemblies<=max(int_id{recCounter})));
    data1_mdn(recCounter) = n_pyr_assemblies/length(pyr_id{recCounter});
    data2_mdn(recCounter) = n_int_assemblies/length(int_id{recCounter});
end
%subplot(122)
figure
arrange_boxplot(data1_mdn,data2_mdn)
p = ranksum(data1_mdn,data2_mdn);
title('Cells in assemblies')
xticklabels({'pyr','int'})
ylabel('Fraction of cells in assemblies')
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')