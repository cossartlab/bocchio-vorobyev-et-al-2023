## README

Codes from the manuscript [Bocchio, Vorobyev et al 2023](https://www.biorxiv.org/content/10.1101/2023.09.18.558335v1)

Here you will find:
- MATLAB Codes used to analyse spontaneous 2-photon calcium imaging data (Figs 1 and 4a)
- Python Codes used to analyse 2-photon imaging + holographic optogenetic stimulation data (Figs 2 and 4b-c)
- MATLAB codes used to generate the network model (Fig. 3)

Refer to readmes of individual sections for additional guidance.
