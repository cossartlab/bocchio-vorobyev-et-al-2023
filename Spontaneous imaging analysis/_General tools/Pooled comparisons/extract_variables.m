function [data1,data2] = extract_variables (variable, subVariable , path, group1, group2, conditionType, conditionValue)

% if nargin < 6
%     conditionType = [];
%     conditionValue = [];
% end

%sizeThresh = 1;

files = dir(path);
fileNames = {files.name};
fileIsDir = {files.isdir};
fileNames = fileNames(cell2mat(fileIsDir)==0);

% fileSizes = {files.bytes};
% fileSizes = (cell2mat(fileSizes))./1000000; %size of files in MB
% fileNames = (fileNames(fileSizes>sizeThresh));

% data1 = zeros(1,length(fileNames));
% data2 = zeros(1,length(fileNames));

for fileCounter = 1:length(fileNames)
    % extract only if recording belongs to certain condition
    if exist('conditionType')
        loadedCond = load([path fileNames{fileCounter}],conditionType);
        if ~strcmp(loadedCond.(conditionType),conditionValue)
            continue
        end
    end
    loadedVar = load([path fileNames{fileCounter}],variable);
    if isempty(subVariable) % if only one variable as input
        if isempty(group1) %and if no groups
            data1{fileCounter} = loadedVar.(variable); % extract variable and move to next iteration
            continue
        end
        data1{fileCounter} = loadedVar.(variable).(group1);
        if nargin > 4 && ~isempty(group2)
            data2{fileCounter} = loadedVar.(variable).(group2);  
        end
    else
        data1{fileCounter} = loadedVar.(variable).(subVariable).(group1);
        if nargin > 4 && ~isempty(group2)
            data2{fileCounter} = loadedVar.(variable).(subVariable).(group2);  
        end
    end
end


end