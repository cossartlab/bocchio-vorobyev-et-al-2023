%% Detect small calcium transients
Tr = traces.all;

disp('Detecting SCEs...')

[NCell,Nz] = size(Tr);

% Savitzky-Golay filter
 Tr = sgolayfilt(Tr',3,5)';

% figure
% for i = 1:NCell
%     plot(time,traces(i,:)+i-1)
%     hold on
% end

% Detect Calcium Transients using a sliding window
TrRest = Tr(:,mvm.restEpochsIndex);
SCE.Raster = zeros(NCell,Nz);
%WinSize = 50;
WinSize = 25;
parfor i=1:NCell
    Acttmp = zeros(1,Nz);
    Sigtmp = zeros(1,Nz);
    Trtmp = Tr(i,:);
    %Remove points with high baseline
    ThBurst = median(Trtmp) + iqr(Trtmp)/2;
    for j = WinSize+1:Nz-WinSize
        %Not active in 10 last frames and not within burst activity
        if ismember(j,mvm.mvmEpochsIndex) == 0
            Wintmp = j-WinSize:j+WinSize;
            Mediantmp = median(Trtmp(Wintmp));
            if sum(Acttmp(j-10:j-1)) == 0 && Mediantmp < ThBurst
                Acttmp(j) = Trtmp(j) - Mediantmp > 2*iqr(Trtmp(Wintmp)); %default: 2.5 or 3*iqr
                Sigtmp(j) = (Trtmp(j) - Mediantmp) / iqr(Trtmp(Wintmp));
            end
        end
    end
    
    Acttmp2{i} = find(Acttmp);
    Sigtmp2{i} = Sigtmp(Acttmp2{i});
end

for i = 1:NCell
    SCE.Raster(i,Acttmp2{i}) = 1;
%     plot(time(Acttmp2{i}),traces(i,Acttmp2{i})+i-1,'.r')
end

% Sum activity over two consecutive frames
MAct = zeros(1,Nz-1);
for i=1:Nz-1
    MAct(i) = sum(max(SCE.Raster(:,i:i+1),[],2));
end

% Select synchronies (RACE)
Th = 5;
[~,SCE.TRace] = findpeaks(MAct,'MinPeakHeight',Th,'MinPeakDistance',3);
SCE.NRace = length(SCE.TRace);

% Create RasterPlots
SCE.Race = zeros(NCell,SCE.NRace);
SCE.RasterRace = zeros(NCell,Nz);
for i = 1:SCE.NRace
    SCE.Race(:,i) = max(SCE.Raster(:,SCE.TRace(i)-1:SCE.TRace(i)+2),[],2);
    SCE.RasterRace(SCE.Race(:,i)==1,SCE.TRace(i)) = 1;
end

SCE.Racesum = sum(SCE.Race,2);

% Plot Traces and raster
figure
plotRasterTraces(SCE.Raster(1:30,:),Tr(1:30,1:length(SCE.Raster)),'noBin','norm')

% Display race
for i = 1:length(SCE.TRace)
    line(SCE.TRace(i)*[1 1],[0 NCell+1],'Color','g');
end

SCE.cellsInSCE.int = find(SCE.Racesum(nCells.int));
SCE.cellsInSCE.pyr = find(SCE.Racesum(nCells.pyr));
SCE.cellsInSCE.all = find(SCE.Racesum);

disp('End of SCE detection');
disp(['Number of SCEs: ' num2str(SCE.NRace)]);
disp(['SCE frequency: ' num2str(SCE.NRace/(length(mvm.restEpochsIndex)*si_img/1000))])
disp(['Fraction of interneurons in SCEs: ' num2str(length(SCE.cellsInSCE.int)/length(nCells.int))])
disp(['Fraction of pyramidal cells in SCEs: ' num2str(length(SCE.cellsInSCE.pyr)/length(nCells.pyr))])

%(length(find(Racesum(nCells.int)))/length(nCells.int))

% Save
fileName_SCE=strcat(fileName,'_SCEs_Arnaud.mat');
save(fileName_SCE,'fileName', 'Acttmp2', 'SCE','-v7.3');
clear fileName_SCE;






