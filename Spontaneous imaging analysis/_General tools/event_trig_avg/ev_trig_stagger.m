function ev_trig_stagger(event, signal, windowSize, plot_type)

if nargin < 4
    plot_type = 'cell'
end

event(1:windowSize/2)=0;

nEvs = sum(event);
evIdx = find(event);
avg = zeros(1,windowSize+1);
x = -windowSize/2:1:windowSize/2;

figure
hold on

switch plot_type
    % Staggered plot by cell
    case 'cell'
        for rowCounter = 1:size(signal,1)
            for evCounter = 1:nEvs
                % Find the indexes of the time window preceding the event
                wIdx = evIdx(evCounter)-windowSize/2 : evIdx(evCounter)+windowSize/2;
                % Add the signal from this window to the average
                avg = avg + signal(rowCounter,wIdx);
                
            end
            
            avg=avg./nEvs;
            
            plot(x,avg + rowCounter)
            ylabel('Cells')
        end
        
    % Staggered plot by trial
    case 'trial'
        for evCounter = 1:nEvs
            % Find the indexes of the time window preceding the event
            wIdx = evIdx(evCounter)-windowSize/2 : evIdx(evCounter)+windowSize/2;
            % Add the signal from this window to the average
            plot(x,signal(wIdx) + evCounter)
            ylabel ('Trials')
        end
end

title(['Average of ', num2str(nEvs), ' windows'])
xlabel('Time, pts')



end