fov = zeros(d1,d2);
figure
% Plot position of interneurons, pyramidal cells and assembly members
for assemblyCounter = 1:size(assemblies.CellCl,1)
    assemblies.cellsInAssembly = assemblies.C0{1,assemblyCounter};
    %assemblyN = ['assembly' num2str(assemblyCounter)];
    if size(assemblies.CellCl,1) > 1 && size(assemblies.CellCl,1) < 5
        subplot(2,2,assemblyCounter)
    elseif size(assemblies.CellCl,1) >= 5
        subplot(3,3,assemblyCounter)
    end
    imagesc(fov)
    colormap(white)
    hold on;
    for cellCounter = 1:max(nCells.all)
        if cellCounter <= max(nCells.int) %if cell is interneuron
            scatter(pos.all(cellCounter,1),pos.all(cellCounter,2),[], 'r')
        else
            scatter(pos.all(cellCounter,1),pos.all(cellCounter,2),[], 'b')
        end
    end
    
    for cellInAssemblyCounter = 1:length(assemblies.cellsInAssembly)
        if assemblies.cellsInAssembly(cellInAssemblyCounter) <= max(nCells.int) %if cell is interneuron
            scatter(pos.all(assemblies.cellsInAssembly(cellInAssemblyCounter),1),pos.all(assemblies.cellsInAssembly(cellInAssemblyCounter),2),[], 'r','filled')
        else
            scatter(pos.all(assemblies.cellsInAssembly(cellInAssemblyCounter),1),pos.all(assemblies.cellsInAssembly(cellInAssemblyCounter),2),[], 'b','filled')
        end
    end
title (['Assembly ' num2str(assemblyCounter)])
end

% Measure interneuron distances

pos.distance = pdist(pos.all); %Eucledian pos.distance between cells
pos.distance = squareform(pos.distance);

for assemblyCounter = 1:size(assemblies.CellCl,1)
    
    intInAssembly = intersect(nCells.int,assemblies.CellCl(assemblyCounter,:));
    
    if isempty(intInAssembly) %skip assembly if no interneurons are part of it
        continue
    end
    
    pyrInAssembly = intersect(nCells.pyr,find(assemblies.CellCl(assemblyCounter,:)));
    pyrOutAssembly = intersect(nCells.pyr,find(~assemblies.CellCl(assemblyCounter,:)));
    
    %pos.distanceInAssembly{1,assemblyCounter) = (zeros(length(pyrInAssembly),length(intInAssembly)));
    %distanceOutAssembly_tmp = (zeros(length(pyrOutAssembly),length(intInAssembly)));
    
    for internInAssemblyCounter = 1:length(intInAssembly)
        for pyrInAssemblyCounter = 1:length(pyrInAssembly)
            pos.distanceInAssembly{assemblyCounter}(internInAssemblyCounter,pyrInAssemblyCounter) = pos.distance(nCells.int(internInAssemblyCounter),nCells.pyr(pyrInAssemblyCounter));
        end
        
        
        for pyrOutAssemblyCounter = 1:length(pyrOutAssembly)
            pos.distanceOutAssembly{1,assemblyCounter}(internInAssemblyCounter,pyrOutAssemblyCounter) = pos.distance(nCells.int(internInAssemblyCounter),nCells.pyr(pyrOutAssemblyCounter));
        end   
    end
    
    %distanceInAssembly_tmp2(assemblyCounter) = mean(pos.distanceInAssembly);
    %distanceOutAssembly_tmp2(assemblyCounter) = mean(distanceOutAssembly);
    
end
        
pos.distIntPyrInAssembly =  mean(cell2mat(pos.distanceInAssembly));
pos.distIntPyrOutAssembly = mean(cell2mat(pos.distanceOutAssembly));

% Plot interneuron distances and run Mann Whitney test
figure
arrange_boxplot(cell2mat(pos.distanceInAssembly),cell2mat(pos.distanceOutAssembly))
xticklabels({'Pyr in assembly','Pyr out assembly'})
ylabel('Distance (um)')
disp('Dist int and pyr in assembly vs pyr out assembly')
p = ranksum(cell2mat(pos.distanceOutAssembly),cell2mat(pos.distanceInAssembly))

clear pyrInAssembly intInAssembly pyrOutAssembly distanceInAssembly_tmp distanceOutAssembly_tmp distanceOutAssembly_tmp2 assemblyCounter assemblyN cellCounter pyrInAssemblyCounter pyrOutAssemblyCounter cellInAssemblyCounter p
