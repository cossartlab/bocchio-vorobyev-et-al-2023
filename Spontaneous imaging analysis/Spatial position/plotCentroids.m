function plotContours(fov,cell_n,centroids,centroid_color,background_type,background_color)

%% >>> OPERATION >>>
% Plot contours of selected cells for defined field of view

% >>> INPUT VARIABLES >>>
% NAME             TYPE, DEFAULT        DESCRIPTION
% fov              double               field of view (image or movie)
% cell_n           double               1d vector with numbers of the cells
%                                       to plot
% contours         cell                 cell array with coordinates of each
%                                       cell in FOV
% contour_color    string, 'red'          color of contours to be plotted
% background_type  string, 'color'        type of background: FOV
%                                       (fluorescence signal) or single color.
%                                       ('white') or fluorescence signal ('image')
% background_color string, 'white'        color of background (black or
%                                       white)


%% 
% Marco Bocchio, 25/2/2020

if ~ismatrix(fov) %take mean across time to get rid of 3rd dimension if input is movie
    fov = mean(fov,3);
end

%set default parameters if inputs are missing
if nargin == 6
    if strcmp(background_color,'black')
        background_color = [0,0,0; 0,0,0; 0,0,0];
    elseif strcmp(background_color,'white')
        background_color = white;
    end
end


if nargin < 6
    if strcmp(background_type,'color')
        background_color = white;
    end
    if nargin < 5 
        background_type = 'color';
        background_color = white;  
        if nargin < 4
            centroid_color = 'blue';
        end            
    end
end

  
switch background_type
    case 'fov' %plot fluorescence signal
        figure;
        imagesc(fov)
    case 'color' %plot single color
        fov = zeros(size(fov,1),size(fov,2));
        figure;
        imagesc(fov)
        colormap(background_color)
    case 'keep' %keep background from previous figure
end

hold on;

% plot contours of selected cells
for cellCounter=1:length(cell_n)
scatter(centroids(cellCounter,1),centroids(cellCounter,2),50,centroid_color,'filled')
end

hold off;

end
 
    