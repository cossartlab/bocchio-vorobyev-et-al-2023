%% Corr vs distance pooled
% linear fit
% [r_all,distance_all] = extract_variables ('corr_vs_dist','tot',resultsPath, 'r_all', 'distance_all',conditionType,conditionValue);
% r_all = cell2mat(r_all);
% distance_all = cell2mat(distance_all);
% mdl = fitlm(distance_all,r_all);
% figure
% subplot(231)
% plot(mdl);
% xlabel('Distance (um)')
% ylabel('corr')
% title('Corr vs distance')
% text(0.45,0.99,['p=' num2str(coefTest(mdl))],'Units','normalized')


% distance of positive vs negative correlation cells (each sample is a
% cell pair)
[r_all,distance_all] = extract_variables ('corr_vs_dist','tot',resultsPath, 'r_all', 'distance_all',conditionType,conditionValue);
for recCounter = 1:size(r_all,2)
    negCorrPos_all{recCounter} = distance_all{recCounter}(r_all{recCounter}<prctile(r_all{recCounter},10));
    posCorrPos_all{recCounter} = distance_all{recCounter}(r_all{recCounter}>prctile(r_all{recCounter},90));
end
negCorrPos = cell2mat(negCorrPos_all);
posCorrPos = cell2mat(posCorrPos_all);
subplot(232)
arrange_boxplot(negCorrPos,posCorrPos);
xticklabels({'Pos corr','Neg corr'})
ylabel('Distance (um)')
title('Each sample is a cell pair')
p = ranksum(negCorrPos,posCorrPos);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

clear negCorrPos posCorrPos

% distance of positive vs negative correlation cells (each sample is a
% recording session)
for recCounter = 1:size(r_all,2)
    negCorrPos(recCounter) = median(negCorrPos_all{recCounter});
    posCorrPos(recCounter) = median(posCorrPos_all{recCounter});
end
subplot(233)
arrange_boxplot(negCorrPos,posCorrPos);
xticklabels({'Pos corr','Neg corr'})
ylabel('Distance (um)')
title('Each sample is a recording session')
p = ranksum(negCorrPos,posCorrPos);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

%% Corr vs distance movie by movie
[pvalue,x1] = extract_variables ('corr_vs_dist','tot',resultsPath, 'pvalue', 'x1',conditionType,conditionValue);
[Rsquared] = extract_variables ('corr_vs_dist','tot',resultsPath, 'Rsquared',[],conditionType,conditionValue);
%clearvars -except resultsPath
subplot(234)
histogram(cell2mat(pvalue),20,'Normalization','probability')
title('p value corr vs dist')
xlabel('p value')
%violin(cell2mat(pvalue)','facecolor','w','edgecolor','k','plotlegend',0)
subplot(235)
histogram(cell2mat(x1(cell2mat(pvalue)<0.05)),20,'Normalization','probability')
xlabel('slope')
title('significant slopes corr vs dist')
subplot(236)
histogram(cell2mat(Rsquared(cell2mat(pvalue)<0.05)),20,'Normalization','probability')
xlabel('R2')
title('significant R2 corr vs dist')
clear pvalue x1 Rsquared

