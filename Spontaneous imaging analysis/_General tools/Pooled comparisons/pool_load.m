function pool_load (parameter, path, group1, group2, group1_label, group2_label)

% path = 'E:\GAD67 results\Recordings\';
% parameter = 'pair_corr';
%group1 = 'dff.dfftot.pyr';
%group2 = 'dff.dfftot.int';
% group1_label = 'pyr';
% group2_label = 'int';

files = dir(path);
fileNames = {files.name};
fileIsDir = {files.isdir};
fileNames = fileNames(cell2mat(fileIsDir)==0); %size of files in MB

data1 = zeros(1,length(fileNames));
data2 = zeros(1,length(fileNames));

for fileCounter = 1:length(fileNames)
    load ([path fileNames{fileCounter}],parameter)
    data1(fileCounter) = median(group1);
    data2(fileCounter) = median(group2);
end

figure
arrange_boxplot(data1,data2)
xticklabels({group1_label,group2_label})
p = ranksum(data1,data2);
disp(['p = ' num2str(p)])

end