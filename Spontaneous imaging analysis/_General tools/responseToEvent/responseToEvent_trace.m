% Event-triggered average of signal

function [avg, nEvs] = responseToEvent_trace(event, signal, windowSize, varargin)

event(1:windowSize/2)=0;
event(end-windowSize/2:end)=0;


nEvs = sum(event); % n events
evIdx = find(event); %event times
avg = zeros(1,windowSize+1); %preallocation
x = -windowSize/2:1:windowSize/2; % x axis (time points)

responses = zeros(nEvs,length(x));

% For each event
for evCounter = 1:nEvs
    wIdx = evIdx(evCounter)-windowSize/2 : evIdx(evCounter)+windowSize/2;  % Find the indexes of the time window around the event
    responses(evCounter,:) = signal(wIdx); % Add the signal from this window to the average
end

baseMean = mean(responses(:,1:4),2);
baseStd = std(responses(:,1:4),[],2);

zScore = (responses - baseMean)./baseStd;

% avg=avg./nEvs; %divide sum by n events to take average
% 
% if ~isempty(varargin) % plot flag
%     if strcmp(varargin{1, 1},'plot') == true
%         figure
%         plot(x,avg)
%         title(['Average of ', num2str(nEvs), ' windows'])
%         xlabel('Time, pts')
%     end
% end

end