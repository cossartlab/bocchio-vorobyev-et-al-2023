% Tot pyr
figure
[data] = extract_variables ('dff','dfftot',resultsPath, 'pyr');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(341)
arrange_boxplot(data1,data2)
title('Whole recording pyr')
xticklabels({'deep','sup'})
ylabel('DF/F')
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

%Rest pyr
[data] = extract_variables ('dff','dffrest',resultsPath, 'pyr');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(342)
arrange_boxplot(data1,data2)
title('Rest pyr')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

%Loc pyr
[data] = extract_variables ('dff','dffmvm',resultsPath, 'pyr');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(343)
arrange_boxplot(data1,data2)
title('Loc pyr')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

%Loc score pyr
[data] = extract_variables ('dff','mvmDffScore',resultsPath, 'pyr');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(344)
arrange_boxplot(data1,data2)
title('Loc score pyr')
ylabel('Loc score')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Tot int
[data] = extract_variables ('dff','dfftot',resultsPath, 'int');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(345)
arrange_boxplot(data1,data2)
title('Whole recording int')
xticklabels({'deep','sup'})
ylabel('DF/F')
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

%Rest int
[data] = extract_variables ('dff','dffrest',resultsPath, 'int');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(346)
arrange_boxplot(data1,data2)
title('Rest int')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

%Loc int
[data] = extract_variables ('dff','dffmvm',resultsPath, 'int');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(347)
arrange_boxplot(data1,data2)
title('Loc int')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

%Loc score int
[data] = extract_variables ('dff','mvmDffScore',resultsPath, 'int');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = median(data{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(348)
arrange_boxplot(data1,data2)
title('Loc score int')
ylabel('Loc score')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

[data1,data2] = extract_variables ('dff','dffrest',resultsPath, 'pyr', 'int');
for recCounter = 1:size(data1,2)
    data1_mdn(recCounter) = median(data1{recCounter})./median(data2{recCounter});
end
data1 = data1_mdn(contains(layer,'deep'));
data2 = data1_mdn(contains(layer,'sup'));
subplot(349)
arrange_boxplot(data1,data2)
title('E/I ratio Rest')
ylabel('E/I ratio')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

[data1,data2] = extract_variables ('dff','dffmvm',resultsPath, 'pyr', 'int');
for recCounter = 1:size(data1,2)
    data1_mdn(recCounter) = median(data1{recCounter})./median(data2{recCounter});
end
data1 = data1_mdn(contains(layer,'deep'));
data2 = data1_mdn(contains(layer,'sup'));
subplot(3,4,10)
arrange_boxplot(data1,data2)
title('E/I ratio Locomotion')
ylabel('E/I ratio')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')