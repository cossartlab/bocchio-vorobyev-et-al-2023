%% Plot int DF/F vs cells in SCE
[traces_int] = extract_variables ('traces',[],resultsPath, 'int');
[TRace] = extract_variables ('SCE',[],resultsPath, 'TRace');
[Race] = extract_variables ('SCE',[],resultsPath, 'Race');
for recCounter = 1:size(traces_int,2)
    int_df_sce{recCounter} = mean(traces_int{recCounter}(:,TRace{recCounter}),1);
    cells_in_sce{recCounter} = sum(Race{recCounter},1);
end
int_df_sce = cell2mat(int_df_sce);
cells_in_sce = cell2mat(cells_in_sce);

figure
scatter(int_df_sce,cells_in_sce);
xlabel('Average int DF/F')
ylabel('Number of cells in SCEs')
