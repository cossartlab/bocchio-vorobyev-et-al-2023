for recCounter = 1:size(data1,2)
    data1_mdn(recCounter) = median(data1{recCounter})./median(data2{recCounter});
end

%colour = lines(length(int)); % colour code for different cells

r_all = (zeros(length(pyr),length(int)));
distance_all = (zeros(length(pyr),length(int)));
   
for internCounter = 1:length(int)
%     if separate_plots == true
%         figure;
%         subplot(3,4,internCounter)
%         hold on;
%     end
    
    for pyrCounter = 1:length(pyr)
        if int(internCounter) == pyr(pyrCounter) % skip correlation of a cell to itself
            continue
        end
        r_all(pyrCounter,internCounter) = r(int(internCounter),pyr(pyrCounter));
        distance_all(pyrCounter,internCounter) = distance(int(internCounter),pyr(pyrCounter));
%         if separate_plots == true
%             scatter(distance(int(internCounter),pyr(pyrCounter)),r(int(internCounter),pyr(pyrCounter)),30,'k')
%         %else
%          %   scatter(D(int(class2Counter),pyr(class1Counter)),r(int(class2Counter),pyr(class1Counter)),30,colour(class2Counter,:))
%         end
    end
end
    
xlabel('Distance (um)')
ylabel('corr')

cellsToRemove = find(r_all==1 & distance_all==0); %exclude autocorrelations

r_all(cellsToRemove) = [];
distance_all(cellsToRemove) = [];

r_all=reshape(r_all,[1,numel(r_all)]);
distance_all=reshape(distance_all,[1,numel(distance_all)]);

% Linear fit
mdl = fitlm(distance_all,r_all);
subplot(121)
plot(mdl);
xlabel('Distance (um)')
ylabel('corr')

% High corr vs low corr cells
posCorr = r_all>0.4;
negCorr = r_all<-0.2;
subplot(122)
arrange_boxplot(distance_all(posCorr),distance_all(negCorr))
xticklabels({'Pos corr','Neg corr'})
ylabel('Distance (um)')
p = ranksum(distance_all(posCorr),distance_all(negCorr));
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

%clear cellCounter cellsToRemove separate_plots pyrCounter intCounter

end