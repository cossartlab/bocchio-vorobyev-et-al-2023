function [mvmEpochsLogic, mvmEpochsIndex, mvmOnsetIndex, mvmOffsetIndex, restEpochsLogic, restEpochsIndex, time] = defMvmEpochs (mvmTrimmed,data,si_abf,si_img,spanThreshold_1,spanThreshold_2,plottingOptions,dataType)

% >>> OPERATION >>>
% Use wheel sensor data to define epochs with movement
% Only periods with flat signal > spanThreshold2 (usually 200 ms) are selected as
% 'rest' epochs. Shorter flat periods intermingled with movement periods
% are included in nearby movement periods
%
% % >>> INPUT VARIABLES >>>
% NAME             TYPE, DEFAULT        DESCRIPTION
% spikenums        double               spike raster matrix 
% si_img           scalar, 50          sampling interval for imaging data
% si_abf           scalar, 0.05          sampling interval of abf file         
% spanThreshold_1  scalar, 1000         initial threshold to find
%                                       consecutive periods of rest
% spanThreshold_2  scalar, 200          second threshold to label rest
%                                       periods as such only if their
%                                       duration is > spanThreshold_2
%%
% Marco Bocchio, 4/7/19

spanThreshold_1 = spanThreshold_1 / si_abf;
spanThreshold_2 = spanThreshold_2 / si_abf;


%% set consecutive ones between movement bouts as zero (set all rest periods as zero)
%mvmTrimmed = -mvmTrimmed; %old wheel: negative mvmTrimmed. comment out for new wheel
amplThreshold = 0.5; %(old wheel threshold: 0.02, new wheel threshold: 0.5)
[restIndex_1] = consecAboveThresh(mvmTrimmed,amplThreshold,spanThreshold_1); 
mvmTrimmed(restIndex_1) = 0; 
mvmTrimmed = mvmTrimmed > amplThreshold; 


%% set short epochs of zeros as ones (include in nearby movement epochs)
rest=abs(mvmTrimmed-1); % vector in which ones define rest periods
[restIndex_2] = consecAboveThresh(rest,0.05,spanThreshold_2); %find consecutive rest periods (ones) above a threshold
mvmTrimmed=ones(length(mvmTrimmed),1);
mvmTrimmed(restIndex_2)=0;

%% downsample movement vector to match spike raster data points
%mvmEpochsLogic = round(resample(mvmTrimmed,length(spikenums),length(mvmTrimmed)));
%mvmEpochsLogic = round(resample(mvmTrimmed,1,10));
%mvmEpochsLogic = round(resample(mvmEpochsLogic,length(spikenums),length(mvmEpochsLogic)));

%mvmEpochsLogic =
%round(resample(mvmTrimmed,round(length(data)/1000),round(length(mvmTrimmed)/1000)));
%%used for somatic data
mvmEpochsLogic = round(resample(mvmTrimmed,round(length(data)/100),round(length(mvmTrimmed)/100))); %used for axonal data

if length(mvmEpochsLogic)>length(data)
    mvmEpochsLogic = mvmEpochsLogic(1:end-(length(mvmEpochsLogic)-length(data)));
end
mvmEpochsIndex = find (mvmEpochsLogic == 1);

%% movement onset and offset
mvmOnsetIndex = (find(diff(mvmEpochsLogic)==1)+1);
mvmOffsetIndex = (find(diff(mvmEpochsLogic)==-1)+1);

%% rest epochs
restEpochsLogic = ~mvmEpochsLogic;
restEpochsIndex = find (restEpochsLogic == 1);

%% calculate time vector
time = 0:si_img*10^-3:(length(data)-1)*si_img*10^-3; % in s

%% plotting
if plottingOptions.epochs.epochs2 == 'mvm'
        plottingOptions.epochs.epochs2 = mvmEpochsIndex;
end

switch dataType
    case 'raster'
        figure
        plotRaster(data,plottingOptions);
        ylabel('Cell #');
        title('Spike raster and movement epochs');

    case 'traces'
        %plot_traces(data,plottingOptions,3);
        %plotTraces(data(1:30,:),1:length(data),'norm');
end






end