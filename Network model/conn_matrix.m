% Define a function that generates a connectivity matrix 'w' for a neural network.
% The matrix represents the synaptic connections between excitatory (E) and inhibitory (I) neurons.

function [w] = conn_matrix(NE, NI, g, pr_E, pr_I, pr_II, mEE, mEI, mIE, mII)
    
    % Calculate the total number of neurons in the network
    N = NE + NI;

    % Define synaptic coupling strengths (weights) between neuron types
    J = 1/NE * 2;
    JEE = J;
    JEI = J;
    JIE = -J * g;
    JII = -J * g;

    % Define angular positions for excitatory and inhibitory neurons on a ring
    th_exc = linspace(0, pi, NE);
    th_inh = linspace(0, pi, NI);

    % Generate random binary matrices to represent connectivity
    c_EE = binornd(1, pr_E, [NE, NE]);
    c_EI = binornd(1, pr_I, [NE, NI]);
    c_IE = binornd(1, pr_I, [NI, NE]);
    c_II = binornd(1, pr_II, [NI, NI]);

    % Ensure there are no self-connections (diagonal elements)
    c_EE(eye(NE) == 1) = 0;
    c_II(eye(NI) == 1) = 0;

    % Initialize weight matrices for E-E and E-I connections
    wEE = zeros(NE, NE);
    wEI = zeros(NE, NI);

    % Compute weights for E-E and E-I connections 
    % respective m values define the specificity of connections
    for i = 1:NE
        wEE(i, :) = (1 + mEE * cos(2 * (th_exc(i) - th_exc))) * JEE;
        wEI(i, :) = (1 + mEI * cos(2 * (th_exc(i) - th_inh))) * JEI;
    end

    % Initialize weight matrices for I-E and I-I connections
    wIE = zeros(NI, NE);
    wII = zeros(NI, NI);

    % Compute weights for I-E and I-I connections
    % respective m values define the specificity of connections
    for i = 1:NI
        wIE(i, :) = (1 + mIE * cos(2 * (th_inh(i) - th_exc))) * JIE;
        wII(i, :) = (1 + mII * cos(2 * (th_inh(i) - th_inh))) * JII;
    end

    % Make the total weight matrix by applying the connectivity matrices to the weight matrices
    wEE = c_EE .* wEE;
    wEI = c_EI .* wEI;
    wIE = c_IE .* wIE;
    wII = c_II .* wII;

    % Combine the connectivity and weight matrices into a single connectivity matrix 'w'
    c = [c_EE, c_EI; c_IE, c_II];
    w = [wEE, wEI; wIE, wII];

    % Ensure there are no self-connections in the final connectivity matrix
    w(eye(N) == 1) = 0;

end
