TRace_logic = zeros(1,size(traces.all,2));
TRace_logic(SCE.TRace) = 1;

% Racesum_rows = sum(Race,1);
% for i = length (Racesum_rows)
%     TRace_logic(SCE.TRace(i)) = Racesum_rows(i);
% end

r_int = zeros(size(traces.int,1),41);
for cellCounter = 1:length(nCells.int)
    if ismember (nCells.int(cellCounter),find(SCE.Racesum)) == 1
        continue
    else
        [r_tmp,lag] = xcorr(TRace_logic,traces.int(cellCounter,:),20,'coeff');
        r_int(cellCounter,:) = r_tmp;
    end
end
figure;
bar(lag*si_img,mean(r_int,1));
title('Interneurons')
xlabel('Time (ms)')

r_pyr = zeros(size(traces.pyr,1),41);
for cellCounter = 1:length(nCells.int)
    if ismember (nCells.pyr(cellCounter),find(SCE.Racesum)) == 1
        continue
    else
        [r_tmp,lag] = xcorr(TRace_logic,traces.pyr(cellCounter,:),20,'coeff');
        r_pyr(cellCounter,:) = r_tmp;
    end
end
figure;bar(lag*si_img,mean(r_pyr,1))
title('Pyramidal cells')
xlabel('Time (ms)')


