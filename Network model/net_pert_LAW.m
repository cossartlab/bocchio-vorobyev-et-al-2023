% - calculate the perturbation responses based on linear analysis

% Transpose the connectivity matrix 'w' to obtain 'W'
W = w';

% Calculate the matrix 'A' using the inverse of the identity matrix minus 'W'
A = inv(eye(N) - W);

% Initialize an empty array to store the differences in response rates
dr_all = [];

% Loop through inhibitory neurons (index range from NE+1 to NE+NI)
for i = NE+1:NE+NI
    
    % Create a vector 'ds' with a single non-zero value (a 1) at index 'i'
    ds = zeros(1, N);
    ds(i) = 1;

    % Calculate the response differences 'dr' by multiplying 'A' with 'ds'
    dr = A * ds';

    % Exclude the response difference of the selected inhibitory neuron
    dr(i) = nan;

    % Store the response difference in the 'dr_all' array
    dr_all = [dr_all, dr];

end
