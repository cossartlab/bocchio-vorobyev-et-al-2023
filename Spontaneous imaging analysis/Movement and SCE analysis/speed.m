%% Load and trim ABF

cmPerPeak = 0.0039;

imagingCh = 1; %imaging channel in abf file
mvmCh = 3;     %locomotion channel in abf file (old:4, current:2)

[imagingTrimmed, mvmTrimmed, fs_abf, si_abf] = loadMvm(Tracecellsdf,imagingCh,mvmCh,'traces');

if exist('fileName') == 0;
    fileName = input('Type filename:','s');
end

if exist('si_img') == 0;
    si_img = input('Type sampling interval for imaging:','s');
end

%si_img = si_img/2; %double resolution after MCMC

fileName_mvm=strcat(fileName,'_mvm.mat');
save(fileName_mvm,'fileName', 'mvmTrimmed','fs_abf','si_abf','si_img','-v7.3');
%clear fileName_mvm imagingTrimmed timeStart timeEnd;

%% Calculate speed

[pks,locs] = findpeaks(diff(mvmTrimmed),'MinPeakHeight',1); %find peaks of locomotion
%[pks,locs] = findpeaks(diff(mvmTrimmed)); %find peaks of locomotion
locs=locs-1; %location of peaks corrected for derivative

locsBinary = zeros(length(mvmTrimmed),1);
locsBinary(locs)=1;
%time=1:si_abf:length(mvmTrimmed)/(fs_abf*1000);

remainder=rem(length(mvmTrimmed),length(TraceClass1));
slidingWindow=round(length(mvmTrimmed)/length(TraceClass1));
mvmBins=sum(reshape(locsBinary(1:end-remainder),[slidingWindow,length(TraceClass1)]),1); %movement bins based on imaging sampling rate
subplot(211); plot(mvmTrimmed); xlim([0 length(mvmTrimmed)]);subplot(212); plot(mvmBins); xlim([0 length(mvmBins)])

distance_resampled=mvmBins*cmPerPeak;
speed_resampled=distance_resampled/si_img*1000;
subplot(311)
plot(TraceClass1(1,:))
xlim([0 length(TraceClass1)]);
subplot(312)
plot(mvmTrimmed)
xlim([0 length(mvmTrimmed)]);
subplot(313)
plot(speed_resampled)
xlim([0 length(mvmBins)]);

