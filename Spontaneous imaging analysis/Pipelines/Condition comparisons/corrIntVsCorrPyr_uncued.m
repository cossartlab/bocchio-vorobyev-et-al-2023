
[corr_pyr,corr_int] = extract_variables ('pair_corr','tot',resultsPath, 'pyr','int',conditionType,conditionValue);
[NRace] = extract_variables ('SCE',[],resultsPath, 'NRace',[],conditionType,conditionValue);
[time] = extract_variables ('time',[],resultsPath,[],[], conditionType,conditionValue);

maxTime = zeros(1,size(time,2));
for recCounter = 1:size(time,2)
    maxTime(recCounter) = max(time{1,recCounter});
end

SCEfreq = cell2mat(NRace)./maxTime;

data1=cell2mat(corr_pyr);
data2=cell2mat(corr_int);
scatter(data2,data1)

mdl = fitlm(data2,data1);
figure
plot(mdl);
xlabel('Corr int-int')
ylabel('Corr pyr-pyr')
title('Corr int vs corr pyr')
text(0.45,0.99,['p=' num2str(coefTest(mdl))],'Units','normalized')
