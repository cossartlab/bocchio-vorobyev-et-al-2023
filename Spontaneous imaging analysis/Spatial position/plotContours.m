function plotContours(fov,cellN,contours,background, color)

%% >>> OPERATION >>>
% Plot contours of selected cells for defined field of view

% >>> INPUT VARIABLES >>>
% NAME             TYPE, DEFAULT        DESCRIPTION
% fov              double               field of view (image or movie)
% cellN            double               1d vector with numbers of the cells
%                                       to plot
% contours         cell                 cell array with coordinates of each
%                                       cell in FOV
% background       char, 'image'        flag to plot white background
%                                       ('white') or fluorescence signal ('image')
% color            char, 'green'        color of contours


%% 
% Marco Bocchio, 25/2/2020

if ~ismatrix(fov) %take mean across time to get rid of 3rd dimension if input is movie
    fov = mean(fov,3);
end

if nargin < 5 %set default color and background
    color = 'g';
    if nargin < 4
        background = 'image';
    end
end

switch background
    case 'image'
        figure;
        imagesc(fov)
    case 'white'
        fov = zeros(size(fov,1),size(fov,2));
        figure;
        imagesc(fov)
        colormap(white)
end

hold on;

% plot contours of selected cells
for cellCounter=1:length(cellN)
    contour = contours{cellN(cellCounter)};
    if ~isa(contour,'double')
        contour = double(contour);
    end
    
    if size(contour,1)~=2
        contour = contour';
    end
    
cont = medfilt1(contour')';
    for j=1:size(cont,2)
        cellCounter
        j
     BW1(cont(2,j),cont(1,j))=1;
     BW2=bwperim(BW1);
    end
B = bwboundaries(BW2,'noholes');
visboundaries(B,'Color',color,'LineWidth', 2)
end

end
 
    