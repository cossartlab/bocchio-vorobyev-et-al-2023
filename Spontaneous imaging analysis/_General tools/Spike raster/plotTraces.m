function plotTraces (traces,time,norm,color)

% if isempty(color)
%     color = 'b';
% end

if nargin < 4
     color = 'b';
end


if isempty(norm)
    norm = 'no_norm';
end

N_cells = size(traces,1);

snr = mean(max(traces,[],2)./min(traces,[],2));

if snr < 0
    snr = -snr;
end

if snr < 1.8
    norm_fact = 10/snr;
else
    norm_fact = 1;
end

%% Plot the graph
switch norm
    case 'norm'
        %tracesToPlot = traces./max(traces,[],2);
        tracesToPlot = traces-min(traces,[],2);
        tracesToPlot = traces./max(traces,[],2);

    case 'no_norm'
        tracesToPlot = traces;      
end

hold on;

for cellCounter = 1:N_cells
    if strcmp(color,'rand') == 1
        c=rand(1,3);
    elseif numel(color)>3
        c=color(:,cellCounter);
    else
        c=color;
    end
     
    if isempty(time)
        plot(tracesToPlot(cellCounter,:) + cellCounter/norm_fact,'Color',c)
        xlim([0 size(traces,2)])   
    else
        plot(time,tracesToPlot(cellCounter,:) + cellCounter/norm_fact,'Color',c)
        xlim([0 max(time)])
    end 
end





set(gca,'children',flipud(get(gca,'children')))
ylim([1 N_cells/norm_fact+1])
%yticklabels(0:N_cells)
   
end