
M = M4_cropped;

T = size(Tracecellsdf,2);

%ContoursAll=ContoursAll';
for i = 1:length(ContoursAll)
    Contour = ContoursAll{1,i};
    Contour = Contour';
    ContoursAll{1,i}=Contour;
end


raw_traces= zeros(length(ContoursAll),T);

for i = 1:length(ContoursAll)
    BW1=zeros(size(M,1),size(M,2));
    cont=cell2mat(ContoursAll(i));
    for j=1:size(cont,2)
        BW1(cont(2,j),cont(1,j))=1;
        BW2=bwperim(BW1);
    end
    
    B=imfill(BW2,'holes');
    npixels=length(find(B));
    raw_traces(i)=mean(M(B));
    
    for k=1:size(M,1)
        for l=1:size(M,2)
            if B(k,l)==1
                tmp = M(k,l,:);
                tmp = squeeze(tmp)';
                raw_traces(i,:)= raw_traces(i,:) + tmp;
            end
        end
    end
    raw_traces(i,:)= raw_traces(i,:)./npixels;
end


% Arnaud normalization (median)
raw_traces_norm=raw_traces;
 for i=1:size(raw_traces_norm,1)
     raw_traces_norm(i,:)=raw_traces_norm(i,:)./median(raw_traces_norm(i,:));
 end

NCell=size(raw_traces_norm,1);
 figure
for i =1: NCell
    plot(raw_traces_norm (i,:) + i-1)
    hold on
    xlim([0 T+100]);
 ylim([0 NCell+5]);
end


%savefig('raw_traces_median_norm');
SaveName=strcat(fileName, '_raw_Traces');
save(SaveName,'raw_traces','raw_traces_norm');

%clear all
%clc
