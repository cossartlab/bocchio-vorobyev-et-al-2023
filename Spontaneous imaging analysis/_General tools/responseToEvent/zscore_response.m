function [negResp, negIndex, posResp, posIndex] = zscore_response(responses,stimPnt)

%% >>> OPERATION >>>
% Defines significant negative and positive responses based on Z-scores

% INPUTS
% NAME            TYPE              NOTES
% responses       m-by-n array      m is the number of trials, n are the
%                                   datapoints
% stimPnt         scalar            datapoint with stimulation

% OUTPUTS
% negResp         logical           significant neg resp
% posResp         logical           significant pos resp
% posIndex        double            datapoints above threshold for positive
                                    % modulation
% negIndex        double            datapoints above threshold for negative
                                    % modulation

% REQUIRED FUNCTIONS
% consecAboveThresh.m (to find consecutive points above threshold)

% Marco Bocchio 22/7/22

c = [1 0 0]; % colour of markers for significant response

baseline = 1:stimPnt-1;

baseAvg = mean(responses(:,baseline),2);
baseStd = std(responses(:,baseline),[],2);

x = -length(baseline):1:length(baseline); % x axis (time points)

zScore = (responses - baseAvg)./baseStd;

zScoreMedian = median(zScore,1);

% search for negative response
[negIndex, ~] = consecAboveThresh (-zScoreMedian(stimPnt:end), 1.65, 1); %1.65 as threshold (p=0.1)

if ~isempty(negIndex)
    negResp = true;
    disp('Negative modulation')
else
    negResp = false;
end

% search for positive response
[posIndex, ~] = consecAboveThresh (zScoreMedian(stimPnt:end), 1.96, 1); %1.96 as threshold (p=0.05)

if ~isempty(posIndex)
    posResp = true;
    disp('Positive modulation')
else
    posResp = false;
end

% Plot average response, single trials and significant points
figure
hold on;
plot(x,zScore','color',[0.8 0.8 0.8]) %single trials
plot(x,zScoreMedian,'k','LineWidth',1) %average
line([0,0],[-20 20],'Color','red','LineStyle','--')
ylim([min(zScore,[],'all') max(zScore,[],'all')])
xlabel ('Frames')
ylabel('Z-score')
if negResp == true %significant points
    scatter (negIndex+stimPnt-stimPnt-1,zScoreMedian(negIndex+stimPnt-1),[],c)
elseif posResp == true
    scatter (posIndex+stimPnt-stimPnt-1,zScoreMedian(posIndex+stimPnt-1),[],c)
end


end