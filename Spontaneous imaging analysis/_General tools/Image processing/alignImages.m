function [alignedImage] = alignImages(refImage,targetImage,nSelections)

%% >>> OPERATION >>>
% Align target image to reference image based on left click selection. If
% target image is a movie (3d array), the mean across the 3rd dimension
% (time) is taken.

% >>> INPUTS >>>
% NAME             TYPE, DEFAULT        DESCRIPTION
% refImage         double
% targetImage      double
% nSelections      scalar, 3            number of cells to select to
%                                       determine shift



% 
% Marco Bocchio, 26/2/2020

%%

if nargin < 3
    nSelections = 3;
end

if ~ismatrix(refImage)
    refImage = mean(refImage,3);
end



compositeImage = imfuse(imadjust(mat2gray(refImage)),imadjust(mat2gray(targetImage)),'ColorChannels','green-magenta'); %composite of red channel and calcium images
%compositeImage = imfuse(mat2gray(refImage),imadjust(mat2gray(targetImage)),'ColorChannels','green-magenta'); %composite of red channel and calcium images
%compositeImage = imfuse(imadjust(mat2gray(refImage)),mat2gray(targetImage),'ColorChannels','green-magenta'); %composite of red channel and calcium images

imagesc(compositeImage)

xRef = zeros(nSelections,1);
yRef = zeros(nSelections,1);
xTarg = zeros(nSelections,1);
yTarg = zeros(nSelections,1);


for selCounter = 1:nSelections
    disp(['Select centre of cell' num2str(selCounter) ' for reference image'])
    [xRef(selCounter),yRef(selCounter)]=ginput(1);
    disp(['Select centre of cell' num2str(selCounter) ' for target image'])
    [xTarg(selCounter),yTarg(selCounter)]=ginput(1);
end

xShift = floor(mean(xRef-xTarg));
yShift = floor(mean(yRef-yTarg));

alignedImage = noncircshift(targetImage,[yShift xShift]);

figure;
subplot(121)
imagesc(compositeImage)
title('Before alignment')
subplot(122)
compositeImage = imfuse(imadjust(mat2gray(refImage)),imadjust(mat2gray(alignedImage)),'ColorChannels','green-magenta'); %composite of red channel and calcium images
imagesc(compositeImage)
title('After alignment')

end
