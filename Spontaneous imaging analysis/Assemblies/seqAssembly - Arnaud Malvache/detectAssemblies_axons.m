%% Clustering
[NCell,NRace] = size(SCE.Race);
[IDX2,sCl,M,S] = kmeansopt(SCE.Race,100,'var');
% M = CovarM(SCE.Race);
% IDX2 = kmedoids(M,NCl);
NCl = max(IDX2);

[~,x2] = sort(IDX2);
MSort = M(x2,x2);

%SCE.Race clusters
R = cell(0);
CellScore = zeros(NCell,NCl);
CellScoreN = zeros(NCell,NCl);
for i = 1:NCl
    R{i} = find(IDX2==i);
    CellScore(:,i) = sum(SCE.Race(:,R{i}),2);
    CellScoreN(:,i) = CellScore(:,i)/length(R{i});
end
%Assign cells to cluster with which it most likely spikes
[~,CellCl] = max(CellScoreN,[],2);
%Remove cells with less than 2 spikes in a given cluster
CellCl(max(CellScore,[],2)<2) = 0;
[X1,x1] = sort(CellCl);

figure
subplot(1,2,1)
imagesc(MSort)
colormap jet
axis image
xlabel('RACE #')
ylabel('RACE #')

subplot(1,2,2)
imagesc(SCE.Race(x1,x2),[-1 1.2])
axis image
xlabel('RACE #')
ylabel('Cell #')



%% Remove cluster non-statistically significant

sClrnd = zeros(1,20);
for i = 1:20
    sClrnd(i) = kmeansoptrnd(SCE.Race,10,NCl);
end
NClOK = sum(sCl>max(sClrnd));
sClOK = sCl(1:NClOK)';

RaceOK = SCE.Race(:,IDX2<=NClOK);
NRaceOK = size(RaceOK,2);


%% New method for statistically finding assemblies and RACE recruitment
%with Bonferroni correction, from clustering output 

[NCell,NRace] = size(SCE.Race);
NCl = NClOK;
NShuf = 5000;
Nt = 14000; % For multiple movies

%% Statistiscal definition of cell assemblies

%Count number of participation to each cluster
CellP = zeros(NCell,NCl); CellR = zeros(NCell,NCl);
for i = 1:NCl
    CellP(:,i) = sum(SCE.Race(:,IDX2 == i),2);
    CellR(:,i) = CellP(:,i)/sum(IDX2 == i);
end

%Test for statistical significance
CellCl = zeros(NCl,NCell); %Binary matrix of cell associated to clusters
for j = 1:NCell
    %Random distribution among Clusters
    RClr = zeros(NCl,NShuf);
    Nrnd = sum(SCE.Race(j,:));
    parfor l = 1:NShuf
        Random = randperm(NRace);
        Random = Random(1:Nrnd);
        Racer = zeros(1,NRace);
        Racer(Random) = 1;
        for i = 1:NCl
            RClr(i,l) = sum(Racer(:,IDX2 == i),2);
        end
    end
    RClr = sort(RClr,2);
    %         ThMin = mean(Random) - 2*std(Random);
    %Proba above 95th percentile
    ThMax = RClr(:,round(NShuf*(1-0.05/NCl))); 
    for i = 1:NCl
        CellCl(i,j) = double(CellP(j,i)>ThMax(i));% - double(RCl(:,j)<ThMin);
    end
end

A0 = find(sum(CellCl) == 0); %Cells not in any cluster
A1 = find(sum(CellCl) == 1); %Cells in one cluster
A2 = find(sum(CellCl) >= 2); %Cells in several clusters

%Keep cluster where they participate the most
for i = A2
    [~,idx] = max(CellR(i,:));
    CellCl(:,i) = 0;
    CellCl(idx,i) = 1;
end
C0 = cell(0);
k = 0;
for i = 1:NCl
    if length(find(CellCl(i,:)))>5
        k = k+1;
        C0{k} = find(CellCl(i,:));
    end
end

%Participation rate to its own cluster
CellRCl = max(CellR([A1 A2],:),[],2);


%% Assign RACE to groups of cells

NCl = length(C0);
[NCell,NRace] = size(SCE.Race);
%Cell count in each cluster
RCl = zeros(NCl,NRace);
PCl = zeros(NCl,NRace);
for i = 1:NCl
    RCl(i,:) = sum(SCE.Race(C0{i},:));
end

RCln = zeros(NCl,NRace);
for j = 1:NRace
    %Random distribution among Clusters
    RClr = zeros(NCl,NShuf);
    Nrnd = sum(SCE.Race(:,j));
    parfor l = 1:NShuf
        Random = randperm(NCell);
        Random = Random(1:Nrnd);
        Racer = zeros(NCell,1);
        Racer(Random) = 1;
        for i = 1:NCl
            RClr(i,l) = sum(Racer(C0{i}));
        end
    end
    %         ThMin = mean(Random) - 2*std(Random);
    RClr = sort(RClr,2);
    %         ThMin = mean(Random) - 2*std(Random);
    %Proba above 95th percentile
    ThMax = RClr(:,round(NShuf*(1-0.05/NCl)));
    for i = 1:NCl
        PCl(i,j) = double(RCl(i,j)>ThMax(i));% - double(RCl(:,j)<ThMin);
    end
    %Normalize (probability)
    RCln(:,j) = RCl(:,j)/sum(SCE.Race(:,j));
end

%% Show Sorted Rasterplot

% load('RaceCl2')
% load('CellClList2.mat')
% load('SCE.Race.mat')
[NCell,NRace] = size(SCE.Race);
NCl = length(C0);

%Recreate CellCl (equivalent of RCl for the cells)
CellCl = zeros(NCl,NCell);
for i = 1:NCl
    CellCl(i,C0{i}) = 1;
end

NCl = length(C0);

Cl0 = find(sum(PCl,1) == 0);
Cl1 = find(sum(PCl,1) == 1);
Cl2 = find(sum(PCl,1) == 2);
Cl3 = find(sum(PCl,1) == 3);
Cl4 = find(sum(PCl,1) == 4);

Bin = 2.^(0:NCl-1);

%Sort Cl1
[~,x01] = sort(Bin*PCl(:,Cl1));
Cl1 = Cl1(x01);

%Sort Cl2
[~,x02] = sort(Bin*PCl(:,Cl2));
Cl2 = Cl2(x02);

%Sort Cl3
[~,x03] = sort(Bin*PCl(:,Cl3));
Cl3 = Cl3(x03);

RList = [Cl0 Cl1 Cl2 Cl3 Cl4];
%x1 from DetectRace

[~,x1] = sort(Bin*CellCl);

% Mov2 = find(TRace>Nt,1);
%Race2d = SCE.Race;
% Race2d(:, Mov2:end) = SCE.Race(:,Mov2:end)/2;

figure
assemblies.rasterPlot = imagesc(SCE.Race(x1,RList));
colormap hot
axis image

componentsInAssemblies = find(sum(CellCl,1));

disp(['Number of assemblies: ' num2str(NCl)])
disp(['Fraction of components in assemblies: ' num2str(length(componentsInAssemblies)./nComponents)])

assemblies.IDX2 = IDX2;
assemblies.C0 = C0;
assemblies.CellRCl = CellRCl;
assemblies.CellRCl = CellRCl;
assemblies.CellCl = CellCl;
assemblies.PCl = PCl;
assemblies.x1 = x1;
assemblies.RList = RList;
assemblies.cellsInAssemblies = cellsInAssemblies;

