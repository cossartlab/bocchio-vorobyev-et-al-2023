function [imagingTrimmed, mvmTrimmed, fs_abf, si_abf,timeStart,timeEnd] = loadMvm(data,imagingCh, mvmCh, dataType, timeStart,timeEnd);

% >>> OPERATION >>>
% Load abf file with imaging-on, treadmill (and LFP if applicable) data.
% Option: choose specific interval of abf to isolate (e.g. to analyse only
% one movie of the series)

% >>> INPUT VARIABLES >>>
% NAME             TYPE, DEFAULT        DESCRIPTION
% data        double                    spike raster or calcium traces
% imagingCh        scalar               imaging channel # on abf file
% mvmCh            scalar               locomotion channel # on abf file
% timeStart        scalar               start time (in sec) of temporal window to isolate a specific part of the abf (optional)         
% timeEnd          scalar               end time (in sec) of the temporal window (optional)

% >>> REQUIRED THIRD-PARTY CODES >>>
% - abfload (F. Collman)

% 
% Marco Bocchio, 5/7/19

%% load ABF
[fileName,pathName] = uigetfile('*.abf');
fullFileName = fullfile(pathName,fileName);
[abfTrace,h,si]=abfload (fullFileName);
si_abf = si.si/10^3; %sampling interval (in ms)
fs_abf = 1/si_abf; %sampling rate (in kHz)

%% isolate specific time window
if nargin > 4
    if timeStart == 0
        pntStart = 1;
    else
        pntStart = round(timeStart/si_abf*10^3); %start point    WRONG
    end
    pntEnd = round(timeEnd/si_abf*10^3); %end point          WRONG
    abfTrace = abfTrace(pntStart:pntEnd,:);
end

%% load channels
imaging=abfTrace(:,imagingCh);
mvm = abfTrace(:,mvmCh);

%% isolate 'imaging on' epochs
[imagingOffIndex] = consecAboveThresh(imaging,0.02,1000);
imagingTrimmed=imaging;
imagingTrimmed([imagingOffIndex])=[];
mvmTrimmed=mvm;
mvmTrimmed([imagingOffIndex])=[];

%% plot
close all;

% spike raster or traces
switch dataType
    case 'raster'
    subplot(3,1,1)
    plotSpikeRaster(logical(data),'PlotType','vertline');
    xlim([0 length(data)])
    case 'traces'
    subplot(3,1,1);
    plotTraces(data,1:length(data),'norm');
    xlim([0 length(data)])
end

%movement
subplot(3,1,2)
plot(mvmTrimmed);
xlim([0 length(mvmTrimmed)])

%imaging on
subplot(3,1,3)
plot(imagingTrimmed);
xlim([0 length(imagingTrimmed)])

end






