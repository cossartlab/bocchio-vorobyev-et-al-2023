
function [glob_avg] =  assembly_trig_avg_single (traces, nCells, si_img, SCE, assemblies)

%clear glob_avg1 glob_avg2 glob_avg3 glob_avg4 glob_avg5 glob_avg6


%event(find(SCE.RasterRace(8,:))) = 1;

windowSize = 200;
x = -windowSize/2:1:windowSize/2;

n_int_in_assembly = sum(sum(assemblies.CellCl(:,nCells.int)));
pyr_out_assemblies = intersect(nCells.pyr,find(sum(assemblies.CellCl,1)==0)); %find pyr cells not in assemblies

if n_int_in_assembly >= 1 %execute loops only if there are interneurons in assembly
    
    figure
    for assemblyCounter = 1:size(assemblies.C0,2) % for each assembly
        int_in_assembly = intersect(nCells.int,assemblies.C0{assemblyCounter});
        
        find(sum(assemblies.CellCl,1)==0);
        %     assemblyCounter
        %     int_in_assembly
        
        
        if isempty(int_in_assembly) % if there isn't an interneuron in this assembly
            continue % move to next assembly
        end
        
        for intCounter = 1:length(int_in_assembly) %for each interneuron within an assembly
            
            curr_int = int_in_assembly(intCounter);
            
            other_assemblies = find(assemblies.CellCl(:,curr_int)==0); %find assemblies without this interneuron
            
            assembly_active = SCE.TRace(find(assemblies.PCl(assemblyCounter,:))); %time points when assembly is active
            int_active = find(SCE.RasterRace(curr_int,:)); %time points when interneuron is active
            
            assembly_and_int = intersect(assembly_active,int_active); % interneuron and assembly active
            assembly_no_int = assembly_active;
            assembly_no_int(ismember(assembly_no_int,assembly_and_int)) = [];% assembly active but interneuron inactive
            
            
            % A) interneuron active and same assembly active: response of same
            %assembly
            event = zeros(1,length(traces.all));
            event(assembly_and_int) = 1;
            signal = traces.all(intersect(assemblies.C0{assemblyCounter},nCells.pyr),:); %get traces of pyr in current assembly
            %signal(find(assemblies.C0{assemblyCounter}==curr_int),:) = []; %remove trace with interneuron
            avg1 = zeros(size(signal,1),windowSize+1); %preallocation
            for cellCounter = 1:size(signal,1) % event triggered average for each cell in same assembly
                [avg1(cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
            end
            
            glob_avg.a(assemblyCounter,intCounter,:) = mean(avg1,1); % average across cells: gives cell responses in same assembly (n assemblies x n interneurons x time)
            
            % B) interneuron inactive and same assembly active: response of same
            %assembly
            event = zeros(1,length(traces.all));
            event(assembly_no_int) = 1;
            signal = traces.all(intersect(assemblies.C0{1},nCells.pyr),:); %get traces of pyr in current assembly
            %signal(find(assemblies.C0{assemblyCounter}==curr_int),:) = []; %remove trace with interneuron
            avg2 = zeros(size(signal,1),windowSize+1); %preallocation
            for cellCounter = 1:size(signal,1) % event triggered average for each cell in same assembly
                [avg2(cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
            end
            
            glob_avg.b(assemblyCounter,intCounter,:) = mean(avg2,1); % average across cells: gives cell responses in same assembly (n assemblies x n interneurons x time)
            
            % STEPS C AND D RUN ONLY IF THERE IS MORE THAN 1 ASSEMBLY
            if size(assemblies.C0,2) > 1
                % C) interneuron active and same assembly active: response of different
                %assemblies
                for otherAssemblyCounter = 1:length(other_assemblies)
                    
                    event = zeros(1,length(traces.all));
                    event(assembly_and_int) = 1;
                    signal = traces.all(intersect(assemblies.C0{other_assemblies(otherAssemblyCounter)},nCells.pyr),:); %get traces of cells current assembly
                    avg3 = zeros(length(other_assemblies),size(signal,1),windowSize+1); %preallocation
                    for cellCounter = 1:size(signal,1) % event triggered average for each cell in other assemblies
                        [avg3(otherAssemblyCounter,cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
                    end
                end
                
                if size(avg3,1) == 1 %if n other assemblies = 1
                    avg3 = squeeze(avg3);
                elseif size(avg3,1) > 1 %if n other assemblies > 1
                    avg3 = mean(squeeze(mean(avg3,2)),1); % average across all other assemblies
                end
                
                glob_avg.c(assemblyCounter,intCounter,:) = mean(avg3,1); % average across cells: gives cell responses in other assemblies (n assemblies x n interneurons x time)
                
                % D) interneuron inactive and same assembly active: response of different
                %assemblies
                for otherAssemblyCounter = 1:length(other_assemblies)
                    
                    event = zeros(1,length(traces.all));
                    event(assembly_no_int) = 1;
                    signal = traces.all(assemblies.C0{other_assemblies(otherAssemblyCounter)},:); %get traces of cells current assembly
                    avg4 = zeros(length(other_assemblies),size(signal,1),windowSize+1); %preallocation
                    for cellCounter = 1:size(signal,1) % event triggered average for each cell in other assemblies
                        [avg4(otherAssemblyCounter,cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
                    end
                end
                if size(avg4,1) == 1 %if n other assemblies = 1
                    avg4 = squeeze(avg4);
                elseif size(avg4,1) > 1 %if n other assemblies > 1
                    avg4 = mean(squeeze(mean(avg4,2)),1); % average across all other assemblies
                end
                
                glob_avg.d(assemblyCounter,intCounter,:) = mean(avg4,1); % average across cells: gives cell responses in other assemblies (n assemblies x n interneurons x time)
            else % If there is only one assembly
                disp('Only one assembly')
            end
            
            % E) interneuron active and same assembly active: response of
            % pyr not in assemblies
            event = zeros(1,length(traces.all));
            event(assembly_and_int) = 1;
            signal = traces.all(pyr_out_assemblies,:); %get traces of pyr in current assembly
            %signal(find(assemblies.C0{assemblyCounter}==curr_int),:) = []; %remove trace with interneuron
            avg1 = zeros(size(signal,1),windowSize+1); %preallocation
            for cellCounter = 1:size(signal,1) % event triggered average for each cell in same assembly
                [avg5(cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
            end
            
            glob_avg.e(assemblyCounter,intCounter,:) = mean(avg5,1); % average across cells: gives responses of pyr cells not in assemblies
            
            % F) interneuron inactive and same assembly active: response
            % of pyr cells not in assemblies
            event = zeros(1,length(traces.all));
            event(assembly_no_int) = 1;
            signal = traces.all(pyr_out_assemblies,:); %get traces of pyr in current assembly
            %signal(find(assemblies.C0{assemblyCounter}==curr_int),:) = []; %remove trace with interneuron
            avg6 = zeros(size(signal,1),windowSize+1); %preallocation
            for cellCounter = 1:size(signal,1) % event triggered average for each cell in same assembly
                [avg6(cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
            end
            
            glob_avg.f(assemblyCounter,intCounter,:) = mean(avg6,1); % average across cells: gives responses of pyr cells not in assemblies
            
        end
        
    end
    
    % average across interneurons and across assemblies
    glob_avg.a = squeeze(mean(mean(glob_avg.a,2),1));
    glob_avg.b = squeeze(mean(mean(glob_avg.b,2),1));
    if size(assemblies.C0,2) > 1
        glob_avg.c = squeeze(mean(mean(glob_avg.c,2),1));
        glob_avg.d = squeeze(mean(mean(glob_avg.d,2),1));
    end
    glob_avg.e = squeeze(mean(mean(glob_avg.e,2),1));
    glob_avg.f = squeeze(mean(mean(glob_avg.f,2),1));
    
    
    x = x*si_img/1000;
    subplot(321)
    plot(x,glob_avg.a)
    yl=ylim;
    xl=xlim;
    title ('Assembly and int ON: same assembly')
    ylabel('DF/F')
    subplot(322)
    plot(x,glob_avg.b)
    ylim(yl)
    xlim(xl)
    title ('Assembly ON, int OFF: same assembly')
    if size(assemblies.C0,2) > 1
        subplot(323)
        plot(x,glob_avg.c)
        ylim(yl)
        xlim(xl)
        ylabel('DF/F')
        title ('Assembly and int ON: diff assembly')
        subplot(324)
        plot(x,glob_avg.d)
        ylim(yl)
        xlim(xl)
        title ('Assembly ON, int OFF: diff assembly')
    end
    subplot(325)
    plot(x,glob_avg.e)
    title ('Assembly ON, int ON: pyr out assemblies')
    ylim(yl)
    xlim(xl)
    xlabel('Time (sec)')
    subplot(326)
    plot(x,glob_avg.f)
    ylim(yl)
    xlim(xl)
    title ('Assembly ON, int OFF: pyr out assemblies')
    xlabel('Time (sec)')
else
    disp('No interneurons in assemblies')
end

end



