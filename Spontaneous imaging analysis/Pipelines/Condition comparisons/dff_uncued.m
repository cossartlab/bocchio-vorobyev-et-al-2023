%% DF/F uncued

% Tot
figure
[data1,data2] = extract_variables ('dff','dfftot',resultsPath, 'pyr', 'int',conditionType,conditionValue);
for recCounter = 1:size(data1,2)
    data1_mdn(recCounter) = median(data1{recCounter});
    data2_mdn(recCounter) = median(data2{recCounter});
end
subplot(241)
arrange_boxplot(data1_mdn,data2_mdn)
title('Whole recording')
xticklabels({'pyr','int'})
ylabel('DF/F')
p = ranksum(data1_mdn,data2_mdn);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Rest
[data1,data2] = extract_variables ('dff','dffrest',resultsPath, 'pyr', 'int',conditionType,conditionValue);
for recCounter = 1:size(data1,2)
    data1_mdn(recCounter) = median(data1{recCounter});
    data2_mdn(recCounter) = median(data2{recCounter});
end
subplot(242)
arrange_boxplot(data1_mdn,data2_mdn)
title('Rest')
xticklabels({'pyr','int'})
p = ranksum(data1_mdn,data2_mdn);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Movement
[data1,data2] = extract_variables ('dff','dffmvm',resultsPath, 'pyr', 'int',conditionType,conditionValue);
for recCounter = 1:size(data1,2)
    data1_mdn(recCounter) = median(data1{recCounter});
    data2_mdn(recCounter) = median(data2{recCounter});
end
subplot(243)
arrange_boxplot(data1_mdn,data2_mdn)
title('Loc')
xticklabels({'pyr','int'})
p = ranksum(data1_mdn,data2_mdn);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Movement score
[data1,data2] = extract_variables ('dff','mvmDffScore',resultsPath, 'pyr', 'int',conditionType,conditionValue);
for recCounter = 1:size(data1,2)
    data1_mdn(recCounter) = median(data1{recCounter});
    data2_mdn(recCounter) = median(data2{recCounter});
end
subplot(244)
arrange_boxplot(data1_mdn,data2_mdn)
title('Loc score')
xticklabels({'pyr','int'})
p = ranksum(data1_mdn,data2_mdn);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')


% Pyr Loc vs Rest
[data1] = extract_variables ('dff','dffrest',resultsPath, 'pyr', [], conditionType,conditionValue);
[data2] = extract_variables ('dff','dffmvm',resultsPath, 'pyr', [], conditionType,conditionValue);
for recCounter = 1:size(data1,2)
    data1_mdn(recCounter) = median(data1{recCounter});
    data2_mdn(recCounter) = median(data2{recCounter});
end
subplot(245)
arrange_boxplot(data1_mdn,data2_mdn)
title('Pyr')
ylabel('DF/F')
xticklabels({'Rest','Loc'})
p = ranksum(data1_mdn,data2_mdn);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Pyr Loc vs Rest
[data1] = extract_variables ('dff','dffrest',resultsPath, 'int', [], conditionType,conditionValue);
[data2] = extract_variables ('dff','dffmvm',resultsPath, 'int', [], conditionType,conditionValue);
for recCounter = 1:size(data1,2)
    data1_mdn(recCounter) = median(data1{recCounter});
    data2_mdn(recCounter) = median(data2{recCounter});
end
subplot(246)
arrange_boxplot(data1_mdn,data2_mdn)
title('Int')
ylabel('DF/F')
xticklabels({'Rest','Loc'})
p = ranksum(data1_mdn,data2_mdn);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% E/I ratio
[data1,data2] = extract_variables ('dff','dffrest',resultsPath, 'pyr', 'int',conditionType,conditionValue);
for recCounter = 1:size(data1,2)
    data1_mdn(recCounter) = median(data1{recCounter})./median(data2{recCounter});
end
[data1,data2] = extract_variables ('dff','dffmvm',resultsPath, 'pyr', 'int',conditionType,conditionValue);
for recCounter = 1:size(data1,2)
    data2_mdn(recCounter) = median(data1{recCounter})./median(data2{recCounter});
end
subplot(247)
arrange_boxplot(data1_mdn,data2_mdn)
title('E/I ratio')
ylabel('E/I ratio')
xticklabels({'Rest','Loc'})
p = ranksum(data1_mdn,data2_mdn);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')