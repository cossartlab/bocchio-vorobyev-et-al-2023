%% Corr uncued

% Pyr vs int Tot
figure
[data1,data2] = extract_variables ('pair_corr','tot',resultsPath, 'pyr', 'int',conditionType,conditionValue);
data1_mdn = cell2mat(data1);
data2_mdn = cell2mat(data2);
subplot(341)
arrange_boxplot(data1_mdn,data2_mdn)
p = ranksum(data1_mdn,data2_mdn);
title('Whole recording')
ylabel('Corr')
xticklabels({'pyr-pyr','int-int'})
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Pyr vs int Rest
[data1,data2] = extract_variables ('pair_corr','rest',resultsPath, 'pyr', 'int',conditionType,conditionValue);
data1_mdn = cell2mat(data1);
data2_mdn = cell2mat(data2);
subplot(342)
arrange_boxplot(data1_mdn,data2_mdn)
p = ranksum(data1_mdn,data2_mdn);
title('Rest')
xticklabels({'pyr-pyr','int-int'})
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Pyr vs int Loc
[data1,data2] = extract_variables ('pair_corr','mvm',resultsPath, 'pyr', 'int',conditionType,conditionValue);
data1_mdn = cell2mat(data1);
data2_mdn = cell2mat(data2);
subplot(343)
arrange_boxplot(data1_mdn,data2_mdn)
p = ranksum(data1_mdn,data2_mdn);
title('Loc')
xticklabels({'pyr-pyr','int-int'})
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% E/I corr ratio
[data1,data2] = extract_variables ('pair_corr','rest',resultsPath, 'pyr', 'int',conditionType,conditionValue);
pyrByIntRest = cell2mat(data2)-cell2mat(data1);
[data1,data2] = extract_variables ('pair_corr','mvm',resultsPath, 'pyr', 'int',conditionType,conditionValue);
pyrByIntLoc = cell2mat(data2)-cell2mat(data1);
subplot(344)
arrange_boxplot(pyrByIntRest,pyrByIntLoc)
p = ranksum(data1_mdn,data2_mdn);
title('E/I corr ratio')
xticklabels({'Rest','Loc'})
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Int-int vs int-pyr Tot
[data1] = extract_variables ('pair_corr','tot',resultsPath, 'int', [], conditionType,conditionValue);
[data2] = extract_variables ('pair_corr','tot',resultsPath, 'pyr_int',[],conditionType,conditionValue);
data1_mdn = cell2mat(data1);
data2_mdn = cell2mat(data2);
subplot(345)
arrange_boxplot(data1_mdn,data2_mdn)
p = ranksum(data1_mdn,data2_mdn);
title('Tot')
xticklabels({'int-int','int-pyr'})
ylabel('Corr')
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Int-int vs int-pyr Loc
[data1] = extract_variables ('pair_corr','mvm',resultsPath, 'int', [], conditionType,conditionValue);
[data2] = extract_variables ('pair_corr','mvm',resultsPath, 'pyr_int',[],conditionType,conditionValue);
data1_mdn = cell2mat(data1);
data2_mdn = cell2mat(data2);
subplot(346)
arrange_boxplot(data1_mdn,data2_mdn)
p = ranksum(data1_mdn,data2_mdn);
title('Loc')
xticklabels({'int-int','int-pyr'})
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Int-int vs int-pyr Rest
[data1] = extract_variables ('pair_corr','rest',resultsPath, 'int', [], conditionType,conditionValue);
[data2] = extract_variables ('pair_corr','rest',resultsPath, 'pyr_int',[],conditionType,conditionValue);
data1_mdn = cell2mat(data1);
data2_mdn = cell2mat(data2);
subplot(347)
arrange_boxplot(data1_mdn,data2_mdn)
p = ranksum(data1_mdn,data2_mdn);
title('Rest')
xticklabels({'int-int','int-pyr'})
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')


% Pyr Loc vs Rest
[data1] = extract_variables ('pair_corr','rest',resultsPath, 'pyr', [], conditionType,conditionValue);
[data2] = extract_variables ('pair_corr','mvm',resultsPath, 'pyr', [], conditionType,conditionValue);
data1_mdn = cell2mat(data1);
data2_mdn = cell2mat(data2);
subplot(349)
arrange_boxplot(data1_mdn,data2_mdn)
title('Pyr-pyr corr')
ylabel('Corr')
xticklabels({'Rest','Loc'})
p = ranksum(data1_mdn,data2_mdn);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Int Loc vs Rest
[data1] = extract_variables ('pair_corr','rest',resultsPath, 'int', [], conditionType,conditionValue);
[data2] = extract_variables ('pair_corr','mvm',resultsPath, 'int', [], conditionType,conditionValue);
data1_mdn = cell2mat(data1);
data2_mdn = cell2mat(data2);
subplot(3,4,10)
arrange_boxplot(data1_mdn,data2_mdn)
title('Int-int corr')
xticklabels({'Rest','Loc'})
p = ranksum(data1_mdn,data2_mdn);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Pyr-int Loc vs Rest
[data1] = extract_variables ('pair_corr','rest',resultsPath, 'pyr_int', [], conditionType,conditionValue);
[data2] = extract_variables ('pair_corr','mvm',resultsPath, 'pyr_int', [], conditionType,conditionValue);
data1_mdn = cell2mat(data1);
data2_mdn = cell2mat(data2);
subplot(3,4,11)
arrange_boxplot(data1_mdn,data2_mdn)
title('Pyr-int corr')
xticklabels({'Rest','Loc'})
p = ranksum(data1_mdn,data2_mdn);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

