% SCE frequency
figure
[NRace] = extract_variables ('SCE',[],resultsPath,'NRace',[]);
[restEpochsIndex] = extract_variables ('mvm',[],resultsPath,'restEpochsIndex',[]);
[si_img] = extract_variables ('si_img',[],resultsPath,[],[]);
for recCounter = 1:size(NRace,2)
    SCEfreq(recCounter) = NRace{recCounter}/(length(restEpochsIndex{recCounter})*si_img{recCounter}/1000);
end
data1 = SCEfreq(contains(layer,'deep'));
data2 = SCEfreq(contains(layer,'sup'));

subplot(221)
arrange_boxplot(data1,data2)
title('SCE frequency')
ylabel('SCE frequency (Hz)')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% n assemblies / n cells
[CellCl] = extract_variables ('assemblies',[],resultsPath,'CellCl');
for recCounter = 1:size(CellCl,2)
    data_mdn(recCounter) = size(CellCl{recCounter},1)/size(CellCl{recCounter},2);
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(222)
arrange_boxplot(data1,data2)
title('Number of assemblies')
ylabel('n assemblies / n cells')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Cells per SCE
[data] = extract_variables ('SCE',[],resultsPath,'Race');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = mean(sum(data{recCounter},1));
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(223)
arrange_boxplot(data1,data2)
title('Average n cells in SCE')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Cells per assembly
[data] = extract_variables ('assemblies',[],resultsPath,'CellCl');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = mean(sum(data{recCounter},2));
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(224)
arrange_boxplot(data1,data2)
title('Average n cells in assembly')
xticklabels({'deep','sup'})
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')