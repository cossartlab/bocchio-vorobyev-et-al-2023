function arrange_boxplot(data1,data2)

error = false;

try
    data = [data1,data2];
catch
    error = true;
end

if error == true
    data = [data1',data2'];
else
    data = [data1,data2];
end
    
g = [zeros(1,length(data1)),ones(1,length(data2))];

boxplot(data,g,'Symbol','o')


end