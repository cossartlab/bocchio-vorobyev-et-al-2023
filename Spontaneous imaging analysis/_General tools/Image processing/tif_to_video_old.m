function [video,n_frames] = tif_to_video(filename,frame_rate)
%
% >>> OPERATION >>>
% Converts series of TIFF files into an AVI video. Uses MPEG-4 compression by default.

%%
% >>> USE >>>
%Select first TIFF file in the folder. The remaining files will be
%automatically found and concatenated

%If not provided, frame rate will be set to 20 Hz

%% 
% Marco Bocchio 30/9/20

% set default values
if nargin < 2
    frame_rate = 20;
    if nargin < 1
        prompt = 'Type filename: ';
        filename = input(prompt);
    end            
end

tic;

clear video

% select data to load
[~,pathname,~]=uigetfile('*.tiff','Select the first TIFF file'); %select first file in the folder
disp('Indexing folders...')
addpath(pathname);

% open folder where file will be saved (one level above TIFF files)
mydir  = fullfile(pathname);
mydir = mydir(1:end-1);
idcs   = strfind(mydir,'\');
newdir = mydir(1:idcs(end)-1);
cd (fullfile(newdir))

% fetch file parameters
disp('Fetching file parameters...')
files = dir(pathname);
filenames = {files.name};
file_size = {files.bytes};
file_size = (cell2mat(file_size))./1000; %size of files in KB
tif_files = (filenames(file_size>1))'; %restrict to files >1 KB in size
n_frames = length(tif_files);

%filename = [num2str(filename) '.avi'];

% if file is already existent, ask what to do
disp('Checking if avi file already exists...')
if isfile(filename)
    warning('File already exists!')
    overwrite = input('Overwrite? (Y/N)','s');Video
    if overwrite == 'Y'
        delete(filename)
    else
        warning('Video not saved. Function aborted. Restart and choose another file name')
        return
    end
end

disp('Loading TIFFs and saving video...')


% set up video file
video = VideoWriter(filename,'MPEG-4');
video.FrameRate = frame_rate;
open(video)

% show progression with waitbar
f = waitbar(0,'Saving video...','Name','Number of frames saved...');

% loop to write frames into video
for frame_counter = 1:1:n_frames
    waitbar(frame_counter/n_frames,f,sprintf('%d%s%d',frame_counter,' / ',n_frames)) %display progress in waitbar        
    tif_file=cell2mat(tif_files(frame_counter,:)); %name of tif image to load
    t = Tiff(tif_file,'r');
    image = read(t);
    close(t)
    writeVideo(video,image) %write tif frame to video
end

close(f)
   
close(video)

disp('Conversion finished')

toc;


end