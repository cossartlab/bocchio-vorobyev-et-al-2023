clear glob_avg1 glob_avg2 glob_avg3 glob_avg4 glob_avg5 glob_avg6

[traces_int_on] = extract_variables ('traces',[],resultsPath, 'all', [],conditionType,conditionValue);
[pyr_id,int_id] = extract_variables ('nCells',[],resultsPath, 'pyr', 'int',conditionType,conditionValue);
[si_img] = extract_variables ('si_img',[],resultsPath, [],[],conditionType,conditionValue);
[TRace,RasterRace] = extract_variables ('SCE',[],resultsPath,'TRace', 'RasterRace', conditionType,conditionValue);
[CellCl,PCl] = extract_variables ('assemblies',[],resultsPath,'CellCl', 'PCl', conditionType,conditionValue);
[C0] = extract_variables ('assemblies',[],resultsPath,'C0',[], conditionType,conditionValue);

% find recordings with no assemblies (C0 set on zero)
noAssemblyRec = cell2mat(cellfun(@iscell,C0,'UniformOutput',false));
noAssemblyRec = ~noAssemblyRec;

% remove variables from recordings with no assemblies
traces_int_on(noAssemblyRec)=[];
pyr_id(noAssemblyRec)=[];
int_id(noAssemblyRec)=[];
si_img(noAssemblyRec)=[];
TRace(noAssemblyRec)=[];
RasterRace(noAssemblyRec)=[];
C0(noAssemblyRec)=[];
CellCl(noAssemblyRec)=[];
PCl(noAssemblyRec)=[];

% calculate event triggered averages for each recording
for recCounter = 1:size(pyr_id,2)
[glob_avg1{recCounter},glob_avg2{recCounter},glob_avg3{recCounter},glob_avg4{recCounter},glob_avg5{recCounter},glob_avg6{recCounter},glob_avg7{recCounter},glob_avg8{recCounter}] =  assembly_trig_avg_pool (traces_int_on{recCounter}, pyr_id{recCounter}, int_id{recCounter}, si_img{recCounter}, TRace{recCounter}, RasterRace{recCounter}, CellCl{recCounter}, PCl{recCounter}, C0{recCounter});
end

figure
traces_int_on=cell2mat(glob_avg1);
traces_int_on=traces_int_on';
peaks_int_on=max(traces_int_on(:,90:110),[],2);
traces_int_off=cell2mat(glob_avg2);
traces_int_off=traces_int_off';
peaks_int_off=max(traces_int_off(:,90:110),[],2);
arrange_boxplot(peaks_int_on,peaks_int_off)
xticklabels({'Int ON','Int OFF'})
title('Same assembly')
p = ranksum(peaks_int_on,peaks_int_off);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')


x = -100:1:100;
x = x.*121/1000;

xlimit=[-10 10];
figure
subplot(421)
glob_std1 = std(cell2mat(glob_avg1),0,2,'omitnan');
glob_avg1 = mean(cell2mat(glob_avg1),2,'omitnan');
errorbar(x,glob_avg1,glob_std1)
yl=ylim;
xlim(xlimit)
title ('Assembly and int ON: same assembly')
ylabel('DF/F')

subplot(422)
glob_std2 = std(cell2mat(glob_avg2),0,2,'omitnan');
glob_avg2 = mean(cell2mat(glob_avg2),2,'omitnan');
errorbar(x,glob_avg2,glob_std2)
ylim(yl)
xlim(xlimit)
title ('Assembly ON, int OFF: same assembly')

subplot(423)
glob_std3 = std(cell2mat(glob_avg3),0,2,'omitnan');
glob_avg3 = mean(cell2mat(glob_avg3),2,'omitnan');
errorbar(x,glob_avg3,glob_std3)
ylim(yl-0.3);
xlim(xlimit)
ylabel('DF/F')
title ('Assembly and int ON: diff assembly')

subplot(424)
glob_std4 = std(cell2mat(glob_avg4),0,2,'omitnan');
glob_avg4 = mean(cell2mat(glob_avg4),2,'omitnan');
errorbar(x,glob_avg4,glob_std4)
ylim(yl-0.3);
xlim(xlimit)
title ('Assembly ON, int OFF: diff assembly')

subplot(425)
glob_std5 = std(cell2mat(glob_avg5),0,2,'omitnan');
glob_avg5 = mean(cell2mat(glob_avg5),2,'omitnan');
errorbar(x,glob_avg2,glob_std2)
ylim(yl)
xlim(xlimit)
ylabel('DF/F')
title ('Assembly ON, int ON: pyr out assemblies')

subplot(426)
glob_std6 = std(cell2mat(glob_avg6),0,2,'omitnan');
glob_avg6 = mean(cell2mat(glob_avg6),2,'omitnan');
errorbar(x,glob_avg6,glob_std6)
ylim(yl)
xlim(xlimit)
title ('Assembly ON, int OFF: pyr out assemblies')

subplot(427)
glob_std7 = std(cell2mat(glob_avg7),0,2,'omitnan');
glob_avg7 = mean(cell2mat(glob_avg7),2,'omitnan');
errorbar(x,glob_avg7,glob_std7)
ylim(yl)
xlim(xlimit)
title ('Assembly ON, int ON: int out assemblies')
xlabel('Time (sec)')

subplot(428)
glob_std8 = std(cell2mat(glob_avg8),0,2,'omitnan');
glob_avg8 = mean(cell2mat(glob_avg8),2,'omitnan');
errorbar(x,glob_avg8,glob_std8)
ylim(yl)
xlim(xlimit)
title ('Assembly ON, int OFF: int out assemblies')
xlabel('Time (sec)')






