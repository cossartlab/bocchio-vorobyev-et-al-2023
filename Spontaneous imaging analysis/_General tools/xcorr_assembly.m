Tassembly_logic = zeros(size(CellCl,1),size(Tracecellsdf,2));

for assemblyCounter = 1:size(CellCl,1)
    assemblyActiv{1,assemblyCounter} = TRace(find(PCl(assemblyCounter,:)));
    Tassembly_logic(assemblyActiv{1,assemblyCounter}) = 1;
end

r_class2 = zeros(size(TraceClass2df,1),41);
r_class1 = zeros(size(TraceClass1df,1),41);

for assemblyCounter = 1:size(CellCl,1)
    cellsInAssembly = C0{1,assemblyCounter};
    figure;
    subplot(121)
    for cellCounter = 1:length(nCells.class2)
        if ismember (nCells.class2(cellCounter),cellsInAssembly) == 1
            continue
        else
            [r_tmp,lag] = xcorr(Tassembly_logic(1,:),TraceClass2df(cellCounter,:),20,'coeff');
            r_class2(cellCounter,:) = r_tmp;
        end
    end
    bar(lag*si_img,mean(r_class2,1));
    xlabel('Time (ms)')
    title (['Interneurons - Assembly ' num2str(assemblyCounter)])
    
    for cellCounter = 1:length(nCells.class1)
    subplot(122)
        if ismember (nCells.class1(cellCounter),cellsInAssembly) == 1
            continue
        else
            [r_tmp,lag] = xcorr(Tassembly_logic(1,:),TraceClass1df(cellCounter,:),20,'coeff');
            r_class1(cellCounter,:) = r_tmp;
        end
    end
    bar(lag*si_img,mean(r_class1,1));
    xlabel('Time (ms)')
    title (['Pyramidal - Assembly ' num2str(assemblyCounter)]) 
end



