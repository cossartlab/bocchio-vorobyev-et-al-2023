function [data] = extract_cond (path, variable)

files = dir([path '*.mat']);
fileNames = {files.name};
%fileIsDir = {files.isdir};
%fileNames = fileNames(cell2mat(fileIsDir)==0); %size of files in MB

for fileCounter = 1:length(fileNames)  
    loadedVar = load([path fileNames{fileCounter}],variable);
    if ~isfield(loadedVar,variable)
        warning(['Variable missing in ' fileNames{fileCounter}])
        continue
    end
    loadedVar = loadedVar.(variable);
    data{1,fileCounter} = loadedVar;
end

end