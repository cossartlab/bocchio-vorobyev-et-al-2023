function [pairwise_corr_median,pairwise_corr_mat] = pairwise_corr (traces,source_cells,target_cells,plotting)

if nargin < 4
    plotting = false;
end

corr_matrix = corr(traces');

if find(size(source_cells)==1)~=find(size(target_cells)==1) % if source and target vectors don't have same orientation
    source_cells = source_cells';
end

if plotting == true
    figure;
    imagesc(corr_matrix([target_cells source_cells],[target_cells source_cells]))
end

% case in which correlation is within a population
if isequal(source_cells,target_cells)
    for i = 1:length(source_cells)
        for j = 1:length(source_cells)
            k = i+j;
            if k>length(source_cells)
                continue
            end
            pairwise_corr_mat{i}(j) = corr_matrix(source_cells(i),target_cells(k));
        end
    end 

pairwise_corr_median = median(cell2mat(pairwise_corr_mat));

% case in which correlation is between two separate populations
else
    pairwise_corr_mat = zeros(length(source_cells),length(target_cells));
    for i = 1:length(source_cells)
        for j = 1:length(target_cells)
            if source_cells(i) == target_cells(j) % skip correlation of a cell to itself
                pairwise_corr_mat(i,j) = NaN;
                continue
            end
            pairwise_corr_mat(i,j) = corr_matrix(source_cells(i),target_cells(j));
        end
    end
    
pairwise_corr_median = median(mean(pairwise_corr_mat,2,'omitnan'));
    
end



% for source_cell_n = source_cells
%     source_cell_counter = source_cells==source_cell_n;
%     for target_cell_n = target_cells
%         target_cell_counter = target_cells==target_cell_n;
%         pairwise_corr(target_cell_counter) = corr_matrix(source_cell_n,target_cell_n);
%     end
%     pairwise_corr_mean(source_cell_counter) = mean(pairwise_corr,'omitnan');
% end



end
    