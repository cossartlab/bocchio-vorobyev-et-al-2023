
clear corr_pyr_int Raceprob

[Racesum,NRace] = extract_variables ('SCE',[],resultsPath, 'Racesum','NRace',conditionType,conditionValue);
%[Racesum] = extract_variables ('SCE',[],resultsPath, 'Racesum',[],conditionType,conditionValue);
[pyr,int] = extract_variables ('nCells',[],resultsPath, 'pyr','int',conditionType,conditionValue);
%[corr_pyr_int_all] = extract_variables ('pair_corr','tot',resultsPath, 'pyr_int_all',[],conditionType,conditionValue);
[corr_pyr_int_all] = extract_variables ('pair_corr','tot',resultsPath, 'pyr_int_all',[],conditionType,conditionValue);

% pair_corr.tot.pyr_int_all

Racesum_int = Racesum;

for recCounter = 1:size(Racesum,2)
Racesum_int{1,recCounter}(pyr{1,recCounter})=[];
Raceprob_int{1,recCounter} = Racesum_int{1,recCounter}./NRace{1,recCounter}.*100;

Racesum{1,recCounter}(int{1,recCounter})=[];
Raceprob_tmp = Racesum{1,recCounter}./NRace{1,recCounter}.*100;
no_partic_index = find(Raceprob_tmp==0);
Raceprob_tmp(no_partic_index)=[];
Raceprob{1,recCounter} = Raceprob_tmp;

%Racesum_pyr{1,recCounter} = Racesum{1,recCounter}(pyr{1,recCounter});
corr_pyr_int_tmp = mean(corr_pyr_int_all{1,recCounter},2);
corr_pyr_int_tmp(no_partic_index)=[];
corr_pyr_int{1,recCounter} = corr_pyr_int_tmp;

end


Raceprob_int=cell2mat(Raceprob_int');
Raceprob=cell2mat(Raceprob');
corr_pyr_int=cell2mat(corr_pyr_int');

% Histogram SCE participation
figure
subplot(121)
histogram(Raceprob,'Normalization','probability')
ylabel('Probability')
xlabel('Participation to SCEs (%)')
title('Pyramidal cells')
Raceprob2=Raceprob;
Raceprob2(Raceprob2==0)=[];
h=histfit(Raceprob2,20,'lognormal');
h(1).FaceColor = [0 0 1];
h(2).Color = [.4 .4 .4];



subplot(122)
histogram(Raceprob_int,'Normalization','probability','EdgeColor',[0 0 0],'FaceColor',[1 0 0])
ylabel('Probability')
xlabel('Participation to SCEs (%)')
title('Interneurons')
Raceprob_int2=Raceprob_int;
Raceprob_int2(Raceprob_int2==0)=[];
h=histfit(Raceprob_int2,10,'lognormal');
h(1).FaceColor = [1 0 0];
h(2).Color = [.4 .4 .4];

% Linear fit SCE participation vs corr w/ int
mdl = fitlm(corr_pyr_int,Raceprob);
figure
plot(mdl)
disp(['p = ' num2str(coefTest(mdl))])
disp(['R2 = ' num2str(mdl.Rsquared.Adjusted)])
xlabel('Corr with int')
ylabel('Participation to SCEs (%)')
text(0.45,0.9,['p = ' num2str(coefTest(mdl)), ' R2 = ' num2str(mdl.Rsquared.Adjusted) ],'Units','normalized')

% 
highPartPyr = find(Raceprob>prctile(Raceprob,90));
lowPartPyr = find(Raceprob<=prctile(Raceprob,90));


% corr_pyr_int = mean(pair_corr.tot.pyr_int_all,2);
% 
figure
arrange_boxplot(corr_pyr_int(highPartPyr),corr_pyr_int(lowPartPyr))
xticklabels({'Highly active pyr in SCEs','Other pyr'})
title('By neuron')
p = ranksum(corr_pyr_int(highPartPyr),corr_pyr_int(lowPartPyr));
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')