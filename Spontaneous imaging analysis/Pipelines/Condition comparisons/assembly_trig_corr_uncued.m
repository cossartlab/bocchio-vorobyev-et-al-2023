clear r_other_asbl_int_on r_other_asbl_int_off r_out_asbl_int_on r_out_asbl_int_off


[traces] = extract_variables ('traces',[],resultsPath, 'all', [],conditionType,conditionValue);
[pyr_id,int_id] = extract_variables ('nCells',[],resultsPath, 'pyr', 'int',conditionType,conditionValue);
[si_img] = extract_variables ('si_img',[],resultsPath, [],[],conditionType,conditionValue);
[TRace,RasterRace] = extract_variables ('SCE',[],resultsPath,'TRace', 'RasterRace', conditionType,conditionValue);
[CellCl,PCl] = extract_variables ('assemblies',[],resultsPath,'CellCl', 'PCl', conditionType,conditionValue);
[C0] = extract_variables ('assemblies',[],resultsPath,'C0',[], conditionType,conditionValue);


% calculate correlations for each recording
for recCounter = 1:size(pyr_id,2)
    [r_other_asbl_int_on{recCounter},r_other_asbl_int_off{recCounter},r_out_asbl_int_on{recCounter},r_out_asbl_int_off{recCounter}] = assembly_trig_corr (traces{recCounter}, pyr_id{recCounter}, int_id{recCounter}, si_img{recCounter}, TRace{recCounter}, RasterRace{recCounter}, CellCl{recCounter}, PCl{recCounter}, C0{recCounter});
end

%[r_out_asbl_int_on,r_out_asbl_int_off] = assembly_trig_corr (traces.all, nCells.pyr, nCells.int, si_img, SCE.TRace, SCE.RasterRace, assemblies.CellCl, assemblies.PCl, assemblies.C0);

% Remove NaN entries
r_other_asbl_int_on(cellfun(@(r_other_asbl_int_on) any(isnan(r_other_asbl_int_on)),r_other_asbl_int_on)) = [];
r_other_asbl_int_off(cellfun(@(r_other_asbl_int_off) any(isnan(r_other_asbl_int_off)),r_other_asbl_int_off)) = [];
r_out_asbl_int_on(cellfun(@(r_out_asbl_int_on) any(isnan(r_out_asbl_int_on)),r_out_asbl_int_on)) = [];
r_out_asbl_int_off(cellfun(@(r_out_asbl_int_off) any(isnan(r_out_asbl_int_off)),r_out_asbl_int_off)) = [];

figure
subplot(121)
arrange_boxplot(cell2mat(r_other_asbl_int_on),cell2mat(r_other_asbl_int_off))
title('Pyr in diff assemblies')
xticklabels({'Int ON','Int OFF'})
ylabel('Corr')
p = ranksum(cell2mat(r_other_asbl_int_on),cell2mat(r_other_asbl_int_off));
text(0.4,0.9,['p = ' num2str(p) ', n = ' num2str(length(r_out_asbl_int_on))],'Units','normalized')

subplot(122)
arrange_boxplot(cell2mat(r_out_asbl_int_on),cell2mat(r_out_asbl_int_off))
title('Pyr out assemblies')
xticklabels({'Int ON','Int OFF'})
p = ranksum(cell2mat(r_out_asbl_int_on),cell2mat(r_out_asbl_int_off));
text(0.4,0.9,['p = ' num2str(p) ', n = ' num2str(length(r_out_asbl_int_on))],'Units','normalized')

