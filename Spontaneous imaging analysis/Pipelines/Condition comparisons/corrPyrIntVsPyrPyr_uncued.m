% Pooled
[corr_pyr_all,corr_pyr_int_all] = extract_variables ('pair_corr','tot',resultsPath, 'pyr_all', 'pyr_int_all',conditionType,conditionValue);
for recCounter = 1:size(corr_pyr_all,2)
    for pyrCounter = 1:length(corr_pyr_all{recCounter}) 
        corr_pyr{recCounter}(pyrCounter) = mean(corr_pyr_all{recCounter}{pyrCounter});
    end
corr_pyr_int{recCounter} = mean(corr_pyr_int_all{recCounter}(1:end-1,:),2)';
end
corr_pyr = cell2mat(corr_pyr);
corr_pyr_int = cell2mat(corr_pyr_int);
mdl = fitlm(corr_pyr,corr_pyr_int);
subplot(231)
plot(mdl);
xlabel('Corr pyr-int')
ylabel('Corr pyr-pyr')
title('Corr pyr-int vs corr pyr-pyr')
text(0.35,0.95,['p=' num2str(num2str(mdl.Coefficients{1,4})) ' R2=' num2str(mdl.Rsquared.Adjusted)],'Units','normalized')

% Movie by movie
[pvalue,x1] = extract_variables ('pair_corr','pyrIntVsPyr',resultsPath, 'pvalue', 'x1',conditionType,conditionValue);
[Rsquared] = extract_variables ('pair_corr','pyrIntVsPyr',resultsPath, 'Rsquared',[],conditionType,conditionValue);
subplot(234)
histogram(cell2mat(pvalue),20,'Normalization','probability')
title('p value corr pyr-pyr vs corr pyr-int')
xlabel('p value')
%violin(cell2mat(pvalue)','facecolor','w','edgecolor','k','plotlegend',0)
subplot(235)
histogram(cell2mat(x1(cell2mat(pvalue)<0.05)),20,'Normalization','probability')
xlabel('slope')
title('significant slopes corr pyr-pyr vs corr pyr-int')
subplot(236)
histogram(cell2mat(Rsquared(cell2mat(pvalue)<0.05)),20,'Normalization','probability')
xlabel('R2')
title('significant R2 corr vs dist')
clear pvalue x1 Rsquared

clearvars -except resultsPath conditionType conditionValue