resultsPath = 'E:\GAD67 results\Recordings\';
conditionType = 'belt';
conditionValue = 'uncued';

clear corr_pyr_int Raceprob

[Racesum,NRace] = extract_variables ('SCE',[],resultsPath, 'Racesum','NRace',conditionType,conditionValue);
%[Racesum] = extract_variables ('SCE',[],resultsPath, 'Racesum',[],conditionType,conditionValue);
[pyr,int] = extract_variables ('nCells',[],resultsPath, 'pyr','int',conditionType,conditionValue);
[corr_pyr_int_all] = extract_variables ('pair_corr','tot',resultsPath, 'pyr_int_all',[],conditionType,conditionValue);

% pair_corr.tot.pyr_int_all


for recCounter = 1:size(Racesum,2)
Racesum{1,recCounter}(int{1,recCounter})=[];
Raceprob{1,recCounter} = Racesum{1,recCounter}./NRace{1,recCounter}.*100;
%Racesum_pyr{1,recCounter} = Racesum{1,recCounter}(pyr{1,recCounter});
corr_pyr_int{1,recCounter} = mean(corr_pyr_int_all{1,recCounter},2);

end

Raceprob=cell2mat(Raceprob');
corr_pyr_int=cell2mat(corr_pyr_int');

% Histogram SCE participation
figure
subplot(131)
histogram(Raceprob,'Normalization','probability')
ylabel('Probability')
xlabel('Participation to SCEs (%)')

% Linear fit SCE participation vs corr w/ int
mdl = fitlm(corr_pyr_int,Raceprob);
subplot(132)
plot(mdl)
disp(['p = ' num2str(coefTest(mdl))])
disp(['R2 = ' num2str(mdl.Rsquared.Adjusted)])
xlabel('Corr with int')
ylabel('Participation to SCEs (%)')
text(0.45,0.9,['p = ' num2str(coefTest(mdl)), ' R2 = ' num2str(mdl.Rsquared.Adjusted) ],'Units','normalized')

% Linear fit SCE participation vs corr w/ int
highPartCells = find(Raceprob>prctile(Raceprob,90));
lowPartCells = find(Raceprob<=prctile(Raceprob,90));

% corr_pyr_int = mean(pair_corr.tot.pyr_int_all,2);
% 
subplot(133)
arrange_boxplot(corr_pyr_int(highPartCells),corr_pyr_int(lowPartCells))
xticklabels({'Highly active pyr in SCEs','Other pyr'})
p = ranksum(corr_pyr_int(highPartCells),corr_pyr_int(lowPartCells));
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')



