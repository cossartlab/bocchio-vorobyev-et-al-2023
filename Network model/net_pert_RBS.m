% - simulate a network of rate-based units 

dt = 1;        % Time step 
tau = 10;      % Time constant 

T_end = 150;   % Total simulation time 
T = 0:dt:T_end; % Time vector

t_trans = 50; % Time for transient responses
dp = 1;        % Perturbation magnitude

W = w;         % Connectivity matrix

% - baseline vs perturbation

dr_all = [];   % Initialize an empty array to store the differences in response rates

% Loop through inhibitory neurons (index range from NE+1 to NE+NI)
for i = NE+1:NE+NI

    % Generate baseline input current for all neurons
    Ib = ones(N, length(T));
    Ib = Ib + 4 * rand(size(Ib));

    % Simulate the network response under the baseline condition
    [rb] = sim_net_rate_based(Ib, W, tau, dt);

    % Create a perturbed input current by adding dp to the selected inhibitory neuron
    Ip = Ib;
    Ip(i, :) = Ip(i, :) + dp;

    % Simulate the network response under the perturbed condition
    [rp] = sim_net_rate_based(Ip, W, tau, dt);

    % Calculate the difference in response rates between perturbed and baseline
    rd = rp - rb;

    % Calculate the mean difference in response rates after the transient period
    rdm = nanmean(rd(:, t_trans/dt:end), 2);

    % Exclude the perturbed neuron's own response difference
    rdm(i) = nan;

    % Store the mean response difference in the dr_all array
    dr_all = [dr_all, rdm];

end
