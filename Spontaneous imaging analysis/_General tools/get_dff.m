% Calculates DF/F

% DF/F is defined with the following formula
% dF/F =( F(t) - F0)/F0

% where F0 is baseline, F(t) is each time point for a given cell
% F0 is defined as the median of a 10s window around F(t)

Fnorm = F-0.7.*Fneu;

dfftraces.all = Fnorm;

wdwSize = round(60/si_img*1000); % size of window for F0 (in datapoints)
if mod(wdwSize,2)==0 % if size of window is even
    wdwSize = wdwSize+1;
end

halfWdwSize = (wdwSize-1)/2; %size of half window

for cellCounter = 1:size(Fnorm,1)
    for datapntCounter = 1:size(Fnorm,2)
        if datapntCounter <= halfWdwSize
            wdw = 1:wdwSize;
        elseif datapntCounter >= size(Fnorm,2)-halfWdwSize
            wdw = size(Fnorm,2)-wdwSize+1:size(Fnorm,2);
        else
            wdw = datapntCounter-halfWdwSize:datapntCounter+halfWdwSize;
        end

        F0 = median(Fnorm(cellCounter,wdw));
        dfftraces.all(cellCounter,datapntCounter) = (dfftraces.all(cellCounter,datapntCounter)-F0)./F0;
            
    end
end

dfftraces.pyr = dfftraces.all(nCells.pyr,:);
dfftraces.int = dfftraces.all(nCells.int,:);


cellsToPlot = [1,100];
figure;
hold on;
subplot(221)
plot(Fnorm(cellsToPlot(1),:))
title('Raw trace')
subplot(222)
plot(dfftraces.all(cellsToPlot(1),:))
title('DF/F')
subplot(223)
plot(Fnorm(cellsToPlot(2),:))
title('Raw trace')
subplot(224)
plot(dfftraces.all(cellsToPlot(2),:))
title('DF/F')

clear halfWdwSize datapntCounter cellsToPlot cellCounter wdw p
