%% Settings

resultsPath = 'D:\GAD67 results\Recordings\';
conditionType = 'belt';
conditionValue = 'uncued';

%% DF/F
dff_uncued
clearvars -except resultsPath conditionType conditionValue

%% Corr
corr_uncued
clearvars -except resultsPath conditionType conditionValue

%% Corr vs distance
corrVsDistPooled_uncued
clearvars -except resultsPath conditionType conditionValue


%% Corr pyr-pyr vs corr pyr-int
corrPyrIntVsPyrPyr_uncued
clearvars -except resultsPath conditionType conditionValue


%% Cells in SCEs or assemblies
cellsInSCEsAssemblies_uncued
clearvars -except resultsPath conditionType conditionValue

%% SCE participation vs corr w/ interneurons
SCEpartic_uncued
clearvars -except resultsPath conditionType conditionValue

%% Corr pyr vs corr int
corrIntVsCorrPyr_uncued


%% Distance and correlations in relation to assemblies
distCorrAssemblies_uncued
clearvars -except resultsPath conditionType conditionValue

%% Assembly-triggered average in relation to int activation
assembly_trig_avg_uncued
clearvars -except resultsPath conditionType conditionValue

%% Assembly-triggered correlations in relation to int activation
assembly_trig_corr_uncued
clearvars -except resultsPath conditionType conditionValue

%% PCA/ICA assemblies
pca_ica_assembly_uncued






