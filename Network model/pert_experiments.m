%% default parameters
NE = 1000;              % Number of excitatory neurons
NI = 100;               % Number of inhibitory neurons
N = NE + NI;            % Total number of neurons

g = NE/NI;              % Excitatory to inhibitory neuron ratio

% Type of simulation 
simulation_type = 'rate_based';  % rate-based simulations
%simulation_type = 'LA_weights';  % linear analysis from weight matrix

% - Type of network structure to simulate
structure_type = 'default_params';
% structure_type = 'no_subnetworks';
% structure_type = 'less_global_II';
% structure_type = 'denser_specific_II';

%%

if strcmp(structure_type, 'default_params')
% single interneuron perturbations "with default connectivity"

    % Modify connectivity parameters to default parameters, with subnetwork structure, 
    % within E-E and E-I, but no specific connectivity within I-I (m_II=0)
    % E-E connections are sparse (pr_E), E-I connections are intermediate, and I-I dense

    pr_E = .01;             % Probability of connection from excitatory to excitatory neurons
    pr_I = 0.5;             % Probability of excitatory-inhibitory connections
    pr_II = 0.85;           % Probability of connection from inhibitory to inhibitory neurons
    
    mEE = 1;                % Connection specificity from excitatory to excitatory neurons
    mEI = 1;                % Connection specificity from excitatory to inhibitory neurons
    mIE = 1;                % Connection specificity from inhibitory to excitatory neurons
    mII = 0;                % Connection specificity from inhibitory to inhibitory neurons

elseif strcmp(structure_type, 'no_subnetworks')
% single interneuron perturbations "without subnetwork structure"

    % Modify connectivity parameters to default parameters, 
    % but without subnetwork structure in any connection type

    pr_E = .01;             % Probability of connection from excitatory to excitatory neurons
    pr_I = 0.5;             % Probability of excitatory-inhibitory connections
    pr_II = 0.85;           % Probability of connection from inhibitory to inhibitory neurons
    
    mEE = 0;                % Connection specificity from excitatory to excitatory neurons
    mEI = 0;                % Connection specificity from excitatory to inhibitory neurons
    mIE = 0;                % Connection specificity from inhibitory to excitatory neurons
    mII = 0;                % Connection specificity from inhibitory to inhibitory neurons

elseif strcmp(structure_type, 'less_global_II')
% single interneuron perturbations "less global II (less dense + more specific)"

    % Modify connectivity parameters to subnetwork structure, 
    % with specific I-to-I (mII = 1) and less dense I-to-I connections (pr_II = 0.5)
    
    pr_E = .01;             % Probability of connection from excitatory to excitatory neurons
    pr_I = 0.5;             % Probability of excitatory-inhibitory connections
    pr_II = 0.5;           % Probability of connection from inhibitory to inhibitory neurons
    
    mEE = 1;                % Connection specificity from excitatory to excitatory neurons
    mEI = 1;                % Connection specificity from excitatory to inhibitory neurons
    mIE = 1;                % Connection specificity from inhibitory to excitatory neurons
    mII = 1;                % Connection specificity from inhibitory to inhibitory neurons

elseif strcmp(structure_type, 'denser_specific_II')
% single interneuron perturbations "denser II but more specific"

    % Modify connectivity parameters to subnetwork structure, with specific
    % I-to-I (mII = 1) and same density of I-to-I connections as default (pr_II = 0.85)

    pr_E = .01;             % Probability of connection from excitatory to excitatory neurons
    pr_I = 0.5;             % Probability of excitatory-inhibitory connections
    pr_II = 0.85;           % Probability of connection from inhibitory to inhibitory neurons
    
    mEE = 1;                % Connection specificity from excitatory to excitatory neurons
    mEI = 1;                % Connection specificity from excitatory to inhibitory neurons
    mIE = 1;                % Connection specificity from inhibitory to excitatory neurons
    mII = 1;                % Connection specificity from inhibitory to inhibitory neurons

end

%% 

% Generate the connectivity matrix with the specified parameters
[w] = conn_matrix(NE, NI, g, pr_E, pr_I, pr_II,  mEE, mEI, mIE, mII);

% Run the network perturbation simulation
if strcmp(simulation_type, 'rate_based')
    net_pert_RBS;
elseif strcmp(simulation_type, 'LA_weights')
    net_pert_LAW;
end

% Figure name for this simulation
fig_name = [structure_type '__' simulation_type];   

plot_figure;

% Plot the figures and save them as PNG files
print(['./Figures/' fig_name '.png'], '-dpng', '-r300');

