function [r_other_asbl_int_on,r_other_asbl_int_off,r_out_asbl_int_on,r_out_asbl_int_off] = assembly_trig_corr (traces, pyr_id, int_id, si_img, TRace, RasterRace, CellCl, PCl, C0)

n_int_in_assembly = sum(sum(CellCl(:,int_id)));
pyr_out_assemblies = intersect(pyr_id,find(sum(CellCl,1)==0)); %find pyr cells not in assemblies
int_out_assemblies = intersect(int_id,find(sum(CellCl,1)==0)); %find interneurons not in assemblies

if n_int_in_assembly >= 1 %execute loops only if there are interneurons in assembly
    
    for assemblyCounter = 1:size(C0,2) % for each assembly
        int_in_assembly = intersect(int_id,C0{assemblyCounter});
        
        if isempty(int_in_assembly) % if there isn't an interneuron in this assembly
            continue % move to next assembly
        end
        
        for intCounter = 1:length(int_in_assembly) %for each interneuron within an assembly
            
            curr_int = int_in_assembly(intCounter);
            other_assemblies = find(CellCl(:,curr_int)==0); %find assemblies without this interneuron
            
            assembly_active = TRace(find(PCl(assemblyCounter,:))); %time points when assembly is active
            int_active = find(RasterRace(curr_int,:)); %time points when interneuron is active
            assembly_and_int = intersect(assembly_active,int_active); % interneuron and assembly active
            assembly_no_int = assembly_active;
            assembly_no_int(ismember(assembly_no_int,assembly_and_int)) = [];% assembly active but interneuron inactive
            
            
            
            
            if length(assembly_no_int)==3 || length(assembly_no_int)>3  % Run steps A-D only if there are at least 3 datapoints with assembly ON and interneuron OFF
                
                if size(C0,2) > 1 % Run steps A-B only if there is more than one assembly
                    
                    for otherAssemblyCounter = 1:length(other_assemblies)
                        % A) interneuron active and same assembly active: corr
                        % of pyr in other assemblies
                        signal = traces(intersect(C0{other_assemblies(otherAssemblyCounter)},pyr_id),assembly_and_int);
                        r = corr(signal');
                        r(r==1) = NaN;
                        r = mean(r, 'all', 'omitnan');
                        r_other_asbl_int_on(assemblyCounter,intCounter,otherAssemblyCounter) = r;%get traces of cells in current assembly
                        
                        % B) interneuron inactive and same assembly active: corr
                        % of pyr in other assemblies
                        signal = traces(intersect(C0{other_assemblies(otherAssemblyCounter)},pyr_id),assembly_no_int);
                        r = corr(signal');
                        r(r==1) = NaN;
                        r = mean(r, 'all', 'omitnan');
                        r_other_asbl_int_off(assemblyCounter,intCounter,otherAssemblyCounter) = r;%get traces of cells in current assembly
                        
                    end
                    
                    r_other_asbl_int_on(r_other_asbl_int_on==0)=NaN;
                    r_other_asbl_int_off(r_other_asbl_int_off==0)=NaN;
                    r_other_asbl_int_on = mean(r_other_asbl_int_on,3,'omitnan'); %average across other assemblies
                    r_other_asbl_int_off = mean(r_other_asbl_int_off,3,'omitnan'); %average across other assemblies

                    
                else
                    r_other_asbl_int_on = NaN;
                    r_other_asbl_int_off = NaN;
                    
                end
                
                
                
                % C) interneuron active and same assembly active: corr of
                % pyr not in assemblies
                signal = traces(pyr_out_assemblies,assembly_no_int); %get traces of pyr in current assembly
                r = corr(signal');
                r(r==1) = NaN;
                r = mean(r, 'all', 'omitnan');
                r_out_asbl_int_on(assemblyCounter,intCounter) = r;
                
                % D) interneuron inactive and same assembly active: corr of
                % pyr not in assemblies
                
                signal = traces(pyr_out_assemblies,assembly_and_int); %get traces of pyr in current assembly
                r = corr(signal');
                r(r==1) = NaN;
                r = mean(r, 'all', 'omitnan');
                r_out_asbl_int_off(assemblyCounter,intCounter) = r;
            else
                r_other_asbl_int_on(assemblyCounter,intCounter) = NaN;
                r_other_asbl_int_off(assemblyCounter,intCounter) = NaN;
                r_out_asbl_int_on(assemblyCounter,intCounter) = NaN;
                r_out_asbl_int_off(assemblyCounter,intCounter) = NaN;
            end
            
        end
    end
    
    r_other_asbl_int_on = mean(r_other_asbl_int_on,2,'omitnan'); %average across interneurons
    r_other_asbl_int_on = mean(r_other_asbl_int_on,'omitnan'); %average across assemblies
    r_other_asbl_int_off = mean(r_other_asbl_int_off,2,'omitnan'); %average across interneurons
    r_other_asbl_int_off = mean(r_other_asbl_int_off,'omitnan'); %average across assemblies
    
    r_out_asbl_int_on(r_out_asbl_int_on==0)=NaN; %remove assemblies without interneurons
    r_out_asbl_int_on = mean(r_out_asbl_int_on,2,'omitnan'); %average across interneurons
    %r_out_asbl_int_on(r_out_asbl_int_on==0)=[]; %remove assemblies without interneurons
    r_out_asbl_int_on = mean(r_out_asbl_int_on,'omitnan'); %average across assemblies
    
    r_out_asbl_int_off(r_out_asbl_int_off==0)=NaN; %remove assemblies without interneurons
    r_out_asbl_int_off = mean(r_out_asbl_int_off,2,'omitnan'); %average across interneurons
    %r_out_asbl_int_off(r_out_asbl_int_off==0)=[]; %remove assemblies without interneurons
    r_out_asbl_int_off = mean(r_out_asbl_int_off,'omitnan'); %average across assemblies
    
else
    r_other_asbl_int_on = NaN;
    r_other_asbl_int_off = NaN;
    r_out_asbl_int_on = NaN;
    r_out_asbl_int_off = NaN;
    
end

end



