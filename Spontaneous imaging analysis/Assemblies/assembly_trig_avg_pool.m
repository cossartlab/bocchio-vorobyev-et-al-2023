
function [glob_avg1,glob_avg2,glob_avg3,glob_avg4,glob_avg5,glob_avg6,glob_avg7,glob_avg8] =  assembly_trig_avg_pool (traces, pyr_id, int_id, si_img, TRace, RasterRace, CellCl, PCl, C0)

windowSize = 200; %in frames
x = -windowSize/2:1:windowSize/2;

n_int_in_assembly = sum(sum(CellCl(:,int_id)));
pyr_out_assemblies = intersect(pyr_id,find(sum(CellCl,1)==0)); %find pyr cells not in assemblies
int_out_assemblies = intersect(int_id,find(sum(CellCl,1)==0)); %find interneurons not in assemblies

if n_int_in_assembly >= 1 %execute loops only if there are interneurons in assembly
    
    for assemblyCounter = 1:size(C0,2) % for each assembly
        int_in_assembly = intersect(int_id,C0{assemblyCounter});
        
        find(sum(CellCl,1)==0);
        %     assemblyCounter
        %     int_in_assembly
        
        
        if isempty(int_in_assembly) % if there isn't an interneuron in this assembly
            continue % move to next assembly
        end
        
        for intCounter = 1:length(int_in_assembly) %for each interneuron within an assembly
            
            curr_int = int_in_assembly(intCounter);
            
            other_assemblies = find(CellCl(:,curr_int)==0); %find assemblies without this interneuron
            
            assembly_active = TRace(find(PCl(assemblyCounter,:))); %time points when assembly is active
            int_active = find(RasterRace(curr_int,:)); %time points when interneuron is active
            
            assembly_and_int = intersect(assembly_active,int_active); % interneuron and assembly active
            assembly_no_int = assembly_active;
            assembly_no_int(ismember(assembly_no_int,assembly_and_int)) = [];% assembly active but interneuron inactive
            
            
            % A) interneuron active and same assembly active: response of same
            %assembly
            event = zeros(1,length(traces));
            event(assembly_and_int) = 1;
            signal = traces(intersect(C0{assemblyCounter},pyr_id),:); %get traces of pyr in current assembly
            %signal(find(C0{assemblyCounter}==curr_int),:) = []; %remove trace with interneuron
            avg1 = zeros(size(signal,1),windowSize+1); %preallocation
            for cellCounter = 1:size(signal,1) % event triggered average for each cell in same assembly
                [avg1(cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
            end
            
            glob_avg1(assemblyCounter,intCounter,:) = mean(avg1,1); % average across cells: gives cell responses in same assembly (n assemblies x n interneurons x time)
            
            % B) interneuron inactive and same assembly active: response of same
            %assembly
            event = zeros(1,length(traces));
            event(assembly_no_int) = 1;
            signal = traces(intersect(C0{1},pyr_id),:); %get traces of pyr in current assembly
            %signal(find(C0{assemblyCounter}==curr_int),:) = []; %remove trace with interneuron
            avg2 = zeros(size(signal,1),windowSize+1); %preallocation
            for cellCounter = 1:size(signal,1) % event triggered average for each cell in same assembly
                [avg2(cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
            end
            
            glob_avg2(assemblyCounter,intCounter,:) = mean(avg2,1); % average across cells: gives cell responses in same assembly (n assemblies x n interneurons x time)
            
            % STEPS C AND D RUN ONLY IF THERE IS MORE THAN 1 ASSEMBLY
            if size(C0,2) > 1
                % C) interneuron active and same assembly active: response of different
                %assemblies
                for otherAssemblyCounter = 1:length(other_assemblies)
                    
                    event = zeros(1,length(traces));
                    event(assembly_and_int) = 1;
                    signal = traces(intersect(C0{other_assemblies(otherAssemblyCounter)},pyr_id),:); %get traces of cells current assembly
                    avg3 = zeros(length(other_assemblies),size(signal,1),windowSize+1); %preallocation
                    for cellCounter = 1:size(signal,1) % event triggered average for each cell in other assemblies
                        [avg3(otherAssemblyCounter,cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
                    end
                end
                
                if size(avg3,1) == 1 %if n other assemblies = 1
                    avg3 = squeeze(avg3);
                elseif size(avg3,1) > 1 %if n other assemblies > 1
                    avg3 = mean(squeeze(mean(avg3,2)),1); % average across all other assemblies
                end
                
                glob_avg3(assemblyCounter,intCounter,:) = mean(avg3,1); % average across cells: gives cell responses in other assemblies (n assemblies x n interneurons x time)
                
                % D) interneuron inactive and same assembly active: response of different
                %assemblies
                for otherAssemblyCounter = 1:length(other_assemblies)
                    
                    event = zeros(1,length(traces));
                    event(assembly_no_int) = 1;
                    signal = traces(C0{other_assemblies(otherAssemblyCounter)},:); %get traces of cells current assembly
                    avg4 = zeros(length(other_assemblies),size(signal,1),windowSize+1); %preallocation
                    for cellCounter = 1:size(signal,1) % event triggered average for each cell in other assemblies
                        [avg4(otherAssemblyCounter,cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
                    end
                end
                if size(avg4,1) == 1 %if n other assemblies = 1
                    avg4 = squeeze(avg4);
                elseif size(avg4,1) > 1 %if n other assemblies > 1
                    avg4 = mean(squeeze(mean(avg4,2)),1); % average across all other assemblies
                end
                
                glob_avg4(assemblyCounter,intCounter,:) = mean(avg4,1); % average across cells: gives cell responses in other assemblies (n assemblies x n interneurons x time)
            else % If there is only one assembly
                disp('Only one assembly')
                glob_avg3 = [];
                glob_avg4 = [];
            end
            
            % E) interneuron active and same assembly active: response of
            % pyr not in assemblies
            event = zeros(1,length(traces));
            event(assembly_and_int) = 1;
            signal = traces(pyr_out_assemblies,:); %get traces of pyr in current assembly
            %signal(find(C0{assemblyCounter}==curr_int),:) = []; %remove trace with interneuron
            avg5 = zeros(size(signal,1),windowSize+1); %preallocation
            for cellCounter = 1:size(signal,1) % event triggered average for each cell in same assembly
                [avg5(cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
            end
            
            glob_avg5(assemblyCounter,intCounter,:) = mean(avg5,1); % average across cells: gives responses of pyr cells not in assemblies
            
            % F) interneuron inactive and same assembly active: response
            % of pyr cells not in assemblies
            event = zeros(1,length(traces));
            event(assembly_no_int) = 1;
            signal = traces(pyr_out_assemblies,:); %get traces of pyr in current assembly
            %signal(find(C0{assemblyCounter}==curr_int),:) = []; %remove trace with interneuron
            avg6 = zeros(size(signal,1),windowSize+1); %preallocation
            for cellCounter = 1:size(signal,1) % event triggered average for each cell in same assembly
                [avg6(cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
            end
            
            glob_avg6(assemblyCounter,intCounter,:) = mean(avg6,1); % average across cells: gives responses of pyr cells not in assemblies
            
            % G) interneuron active and same assembly active: response of
            % int not in assemblies
            event = zeros(1,length(traces));
            event(assembly_and_int) = 1;
            signal = traces(int_out_assemblies,:); %get traces of pyr in current assembly
            %signal(find(C0{assemblyCounter}==curr_int),:) = []; %remove trace with interneuron
            avg7 = zeros(size(signal,1),windowSize+1); %preallocation
            for cellCounter = 1:size(signal,1) % event triggered average for each cell in same assembly
                [avg7(cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
            end
            
            glob_avg7(assemblyCounter,intCounter,:) = mean(avg7,1); % average across cells: gives responses of int cells not in assemblies
            
            % H) interneuron inactive and same assembly active: response
            % of interneurons not in assemblies
            event = zeros(1,length(traces));
            event(assembly_no_int) = 1;
            signal = traces(int_out_assemblies,:); %get traces of pyr in current assembly
            %signal(find(C0{assemblyCounter}==curr_int),:) = []; %remove trace with interneuron
            avg8 = zeros(size(signal,1),windowSize+1); %preallocation
            for cellCounter = 1:size(signal,1) % event triggered average for each cell in same assembly
                [avg8(cellCounter,:), nEvs] = ev_trig_avg(event, signal(cellCounter,:), windowSize);
            end
            
            glob_avg8(assemblyCounter,intCounter,:) = mean(avg8,1); % average across cells: gives responses of pyr cells not in assemblies
            
        end
        
    end
    
    % average across interneurons and across assemblies
    glob_avg1 = squeeze(mean(mean(glob_avg1,2),1));
    glob_avg2 = squeeze(mean(mean(glob_avg2,2),1));
    if size(C0,2) > 1
        glob_avg3 = squeeze(mean(mean(glob_avg3,2),1));
        glob_avg4 = squeeze(mean(mean(glob_avg4,2),1));
    end
    glob_avg5 = squeeze(mean(mean(glob_avg5,2),1));
    glob_avg6 = squeeze(mean(mean(glob_avg6,2),1));
    glob_avg7 = squeeze(mean(mean(glob_avg7,2),1));
    glob_avg8 = squeeze(mean(mean(glob_avg8,2),1));
    
    
    x = x*si_img*10^-3;
    
else
    disp('No interneurons in assemblies')
    glob_avg1 = [];
    glob_avg2 = [];
    glob_avg3 = [];
    glob_avg4 = [];
    glob_avg5 = [];
    glob_avg6 = [];
    glob_avg7 = [];
    glob_avg8 = [];
end

end



