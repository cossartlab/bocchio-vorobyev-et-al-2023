wheelCircumf = 11.2; %in cm

%% load ABF
[fileName,pathName] = uigetfile('*.abf');
fullFileName = fullfile(pathName,fileName);
[abfTrace,h,si]=abfload (fullFileName);
si_abf = si.si/10^3; %sampling interval (in ms)
fs_abf = 1/si_abf; %sampling rate (in kHz)

[pks,locs] = findpeaks(diff(abfTrace(1.5*10^6:2.7*10^6,2)),'MinPeakHeight',1);

msg=['Number of peaks: ', num2str(length(pks))];
disp(msg)

cmPerPeak = wheelCircumf/length(pks);

msg=['Cm per peak: ', num2str(cmPerPeak)];
disp(msg)
