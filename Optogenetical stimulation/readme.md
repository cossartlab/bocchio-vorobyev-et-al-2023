## README

# Overview

The experimental data is pre-processed with 1_prepro.ipynb, where an Excel spreadsheet 
with all the information about the experiments is created. In the the calcium imaging 
activity recordings are processed, and the artefacts from stimulation are removed. 

After that, the resulting movies are processed with the suit2p package (not included, 
is available from the official website https://github.com/MouseLand/suite2p).

Finally, the analysis's main part is done using the Experiment class from 
experiments_code_s2p.py file. This class combines the data from calcium imaging activity 
recordings processed by suite2p and the additional data about the stimulation and behaviour. 
Most of the results for the paper are done by running the Filling_data_in_the_paper.ipynb; 
it is explicitly stated in this file if some results are obtained using the other programs. 

Tested with Python 3.7.10 on a Mac OS 10.15.7.

Feel free to write an email to artem.vorobyev@inserm.fr if you are trying to use this code 
on your data or to reproduce some of the findings and you need further guidance.

# Dependencies
The file with the corresponding libraries env.yml is included in the folder.

# Dataset
The execution of the code requires the dataset presense; please ask me to provide it to run
the code.
