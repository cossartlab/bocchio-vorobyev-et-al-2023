function [video,n_frames] = mat_to_video(array,filename,frame_rate)
%
% >>> OPERATION >>>
% Converts series of TIFF files into an AVI video. Uses MPEG-4 compression by default.

%%
% >>> USE >>>
%Select first TIFF file in the folder. The remaining files will be
%automatically found and concatenated

%If not provided, frame rate will be set to 20 Hz

%% 
% Marco Bocchio 30/9/20

% set default values
if nargin < 3
    frame_rate = 20;
    if nargin < 2
        prompt = 'Type filename: ';
        filename = input(prompt);
    end            
end

tic;

clear video

n_frames = size(array,3);
array = mat2gray(array);

%filename = [num2str(filename) '.avi'];

% if file is already existent, ask what to do
disp('Checking if avi file already exists...')
if isfile(filename)
    warning('File already exists!')
    overwrite = input('Overwrite? (Y/N)','s');Video
    if overwrite == 'Y'
        delete(filename)
    else
        warning('Video not saved. Function aborted. Restart and choose another file name')
        return
    end
end

disp('Loading TIFFs and saving video...')


% set up video file
video = VideoWriter(filename,'Motion JPEG AVI');
video.FrameRate = frame_rate;
open(video)

% show progression with waitbar
f = waitbar(0,'Saving video...','Name','Number of frames saved...');

% loop to write frames into video
for frame_counter = 1:1:n_frames
    waitbar(frame_counter/n_frames,f,sprintf('%d%s%d',frame_counter,' / ',n_frames)) %display progress in waitbar        
    image=array(:,:,frame_counter); %frame to load
    writeVideo(video,image) %write tif frame to video
end

close(f)
   
close(video)

disp('Conversion finished')

toc;


end