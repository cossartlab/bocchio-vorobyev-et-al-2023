%--------------------------------------------------------------------------
% Pipeline to analyse DF/F activity for GAD67 project
%--------------------------------------------------------------------------
%
% >>> REQUIRED THIRD-PARTY CODES >>>
%   -   abfload (F. Collman). https://github.com/fcollman/abfload


%--------------------------------------------------------------------------
% Marco Bocchio, updated 2/2/22
%--------------------------------------------------------------------------

%% Load data

resultsPath = 'E:\GAD67 results\Recordings\';
fileName = 'gadcre1_uncued_deep1_160506_1';
load ([resultsPath fileName])

if ~exist('d1','var')
    d1 = 201;
    d2 = 201;
end



%% Load and trim ABF

imagingCh = 1; %imaging channel in abf file
mvmCh = 3;     %locomotion channel in abf file (old:4, current:3)

[~, mvmTrimmed, fs_abf, si_abf] = loadMvm(traces.all,imagingCh,mvmCh,'traces');

fileName_mvm=strcat(fileName,'_mvm.mat');
save(fileName_mvm,'fileName', 'mvmTrimmed','fs_abf','si_abf','si_img','-v7.3');
clear fileName_mvm


%% Define movement and rest epochs and align them to calcium traces
clear plottingOptions; % options for plotting the raster
plottingOptions.cellColour = nCells.int; % cells to highlight in binned spike raster
plottingOptions.epochs.epochs2 = 'mvm'; % highlight movement in raster

[mvmEpochsLogic, mvmEpochsIndex, mvmOnsetIndex, mvmOffsetIndex, restEpochsLogic, restEpochsIndex, time] = defMvmEpochs (mvmTrimmed,traces.all,si_abf,si_img,1000,200,plottingOptions,'traces'); % old analysis: spanthreshold_2 = 200 ms; new: 1000s

if exist('fileName') == 0
    fileName = input('Type filename:','s');
end

% remove pre-movement period (10 frames) from rest periods (avoid
% contamination for SCE and assembly analysis)
for mvmIndexCounter = 1:length(mvmOnsetIndex)
preMvmIndexTemp = mvmOnsetIndex(mvmIndexCounter)-10:mvmOnsetIndex(mvmIndexCounter)-1;
    if mvmIndexCounter == 1
        preMvmIndex = preMvmIndexTemp;
    else
        preMvmIndex = [preMvmIndex preMvmIndexTemp];
    end
end

clear preMvmIndexTemp

restEpochsOrigLogic = restEpochsLogic; % save a copy of original rest time points
restEpochsOrigIndex = restEpochsIndex;

restEpochsLogic(preMvmIndex)=[]; % remove pre-movement periods from rest
restEpochsIndex = find (restEpochsLogic == 1);

% Save in current experiment directory
fileName_mvmEpochs=strcat(fileName,'_mvmEpochs.mat');
save(fileName_mvmEpochs,'fileName', 'mvmEpochsLogic','mvmEpochsIndex','mvmOnsetIndex','mvmOffsetIndex', 'restEpochsLogic', 'restEpochsIndex','si_img','time','-v7.3');
%clear fileName_mvmEpochs mvmTrimmed fs_abf si_abf;

% Save in global results directory
mvm.mvmEpochsIndex = mvmEpochsIndex;
mvm.mvmEpochsLogic = mvmEpochsLogic;
mvm.mvmOffsetIndex = mvmOffsetIndex;
mvm.mvmOnsetIndex = mvmOnsetIndex;
mvm.restEpochsIndex = restEpochsIndex;
mvm.restEpochsLogic = restEpochsLogic;

clear mvmIndexCounter prMvmIndex mvmEpochsIndex mvmEpochsLogic mvmOffsetIndex mvmOnsetIndex restEpochsIndex restEpochsLogic fs_abf imagingCh ~imagingTrimmed mvmCh mvmTrimmed

save([resultsPath fileName])

%% DF/F during rest and movement

get_dff

% DF/F during rest and movement
dff.dfftot.all = mean(dfftraces.all,2);
dff.dfftot.pyr = mean(dfftraces.pyr,2);
dff.dfftot.int = mean(dfftraces.int,2);

dff.dffrest.all = mean(dfftraces.all(:,mvm.restEpochsIndex),2);
dff.dffrest.pyr = mean(dfftraces.pyr(:,mvm.restEpochsIndex),2);
dff.dffrest.int = mean(dfftraces.int(:,mvm.restEpochsIndex),2);

dff.dffmvm.all = mean(dfftraces.all(:,mvm.mvmEpochsIndex),2);
dff.dffmvm.pyr = mean(dfftraces.pyr(:,mvm.mvmEpochsIndex),2);
dff.dffmvm.int = mean(dfftraces.int(:,mvm.mvmEpochsIndex),2);

% movement rate score
dff.mvmDffScore.all = (dff.dffmvm.all-dff.dffrest.all)./(dff.dffmvm.all+dff.dffrest.all);
dff.mvmDffScore.pyr = (dff.dffmvm.int-dff.dffrest.int)./(dff.dffmvm.int+dff.dffrest.int);
dff.mvmDffScore.int = (dff.dffmvm.pyr-dff.dffrest.pyr)./(dff.dffmvm.pyr+dff.dffrest.pyr);

figure
subplot(221)
arrange_boxplot(dff.dfftot.pyr,dff.dfftot.int);
xticklabels({'Pyr','Int'})
title('Overall')
subplot(222)
arrange_boxplot(dff.dffrest.pyr,dff.dffmvm.pyr);
xticklabels({'Rest','Loc'})
title('Pyr')
subplot(223)
arrange_boxplot(dff.dffrest.int,dff.dffmvm.int);
xticklabels({'Rest','Loc'})
title('Int')
subplot(224)
arrange_boxplot(dff.mvmDffScore.pyr,dff.mvmDffScore.int);
xticklabels({'Pyr','Int'})
title('Loc DF/F score')

% Save results

% In local directory
%fileName_dffMvmRest=strcat(fileName,'_dffMvmRest.mat');
%save(fileName_dffMvmRest,'fileName', 'si_img','dff','nCells','-v7.3');
%clear fileName_dffMvmRest;

% In global directory
save([resultsPath fileName])

%% Pairwise correlations

% Whole recording pyramidal cells
% pair_corr.tot.pyr_all = corr(traces.all(nCells.pyr,:)');
% pair_corr.tot.pyr_all(pair_corr.tot.pyr_all==1)=NaN;
% pair_corr.tot.pyr = median(mean(pair_corr.tot.pyr_all,2,'omitnan'),'omitnan');
[pair_corr.tot.pyr, pair_corr.tot.pyr_all] = pairwise_corr (traces.all,nCells.pyr,nCells.pyr);

% Whole recording interneurons
[pair_corr.tot.int, pair_corr.tot.int_all] = pairwise_corr (traces.all,nCells.int,nCells.int);

% Whole recording pyr-int
[pair_corr.tot.pyr_int, pair_corr.tot.pyr_int_all] = pairwise_corr (traces.all,nCells.pyr,nCells.int);

% Run pyramidal cells
[pair_corr.mvm.pyr, pair_corr.mvm.pyr_all] = pairwise_corr (traces.all(:,mvm.mvmEpochsIndex),nCells.pyr,nCells.pyr);

% Run interneurons
[pair_corr.mvm.int, pair_corr.mvm.int_all] = pairwise_corr (traces.all(:,mvm.mvmEpochsIndex),nCells.int,nCells.int);

% Run pyr-int
[pair_corr.mvm.pyr_int, pair_corr.mvm.pyr_int_all] = pairwise_corr (traces.all(:,mvm.mvmEpochsIndex),nCells.pyr,nCells.int);

% Rest pyramidal cells
[pair_corr.rest.pyr, pair_corr.rest.pyr_all] = pairwise_corr (traces.all(:,mvm.restEpochsIndex),nCells.pyr,nCells.pyr);

% Rest interneurons
[pair_corr.rest.int, pair_corr.rest.int_all] = pairwise_corr (traces.all(:,mvm.restEpochsIndex),nCells.int,nCells.int);

% Rest pyr-int
[pair_corr.rest.pyr_int,pair_corr.rest.pyr_int_all] = pairwise_corr (traces.all(:,mvm.restEpochsIndex),nCells.pyr,nCells.int);

% Save variables

% In local directory
fileName_SCE=strcat(fileName,'_corr.mat');
save(fileName_SCE,'fileName', 'pair_corr','-v7.3');
clear fileName_corr r;

% In global results directory
save([resultsPath fileName])

%% Correlation figures
% Distance vs corr
figure
%Whole rec
[mdl,corr_vs_dist.tot.r_all,corr_vs_dist.tot.distance_all]=corrVsDist(traces.all,pos.all,nCells.pyr,nCells.int);
title('Whole recording')
corr_vs_dist.tot.Rsquared = mdl.Rsquared.Adjusted  ;
disp('Distance vs correlation')
corr_vs_dist.tot.pvalue = coefTest(mdl);
corr_vs_dist.tot.x1 = mdl.Coefficients.Estimate(2);
disp('Whole recording')
disp(['p=' num2str(corr_vs_dist.tot.pvalue)])
disp(['x1=' num2str(corr_vs_dist.tot.x1)])
disp(['R2=' num2str(corr_vs_dist.tot.Rsquared)])

% Corr pyr-pyr vs corr pyr-int
for i = 1:length(pair_corr.tot.pyr_all) 
    pair_corr_pyr(i)=mean(pair_corr.tot.pyr_all{1, i});
end
mdl = fitlm(mean(pair_corr.tot.pyr_int_all(1:end-1,:),2),pair_corr_pyr);
figure;
plot(mdl);
xlabel('Corr pyr-int')
ylabel('Corr between pyr')
pair_corr.pyrIntVsPyr.Rsquared = mdl.Rsquared.Adjusted  ;
disp('Corr pyr-pyr vs corr pyr-int')
pair_corr.pyrIntVsPyr.pvalue = coefTest(mdl);
pair_corr.pyrIntVsPyr.pvalue
pair_corr.pyrIntVsPyr.x1 = mdl.Coefficients.Estimate(2);
clear pair_corr_pyr i mdl h ans internCounter

save([resultsPath fileName])

%% Detect SCEs

clearvars -except resultsPath fileName
load ([resultsPath fileName])
detectSCEs5percent

% Save in global results directory
clear Acttmp2 MAct i Nz NCell Sigtmp2 Th Tr TrRest WinSize
save([resultsPath fileName])

%% Detect assemblies

detectAssemblies
clearvars -except resultsPath fileName
load ([resultsPath fileName])
load([fileName,'_assemblies_Arnaud.mat'])
save([resultsPath fileName])

%% Spatial position of interneurons and assemblies

posInternAssembly
%save([resultsPath fileName])

%% Correlation w/ int of pyr in assemblies vs pyr out assemblies
pyrInAssembly = intersect(nCells.pyr,find(mean(assemblies.CellCl,1)));
pyrOutAssembly = intersect(nCells.pyr,find(~mean(assemblies.CellCl,1)));

[~,assemblies.corrInt.PyrInAssembly] = pairwise_corr (traces.all,pyrInAssembly,nCells.int);
[~,assemblies.corrInt.PyrOutAssembly] = pairwise_corr (traces.all,pyrOutAssembly,nCells.int);
assemblies.corrInt.PyrInAssembly=mean(assemblies.corrInt.PyrInAssembly,2);
assemblies.corrInt.PyrOutAssembly=mean(assemblies.corrInt.PyrOutAssembly,2);
assemblies.corrInt.p = ranksum(assemblies.corrInt.PyrInAssembly,assemblies.corrInt.PyrOutAssembly); % Mann Whitney U test
figure
arrange_boxplot(assemblies.corrInt.PyrInAssembly,assemblies.corrInt.PyrOutAssembly); %boxplot
xticklabels({'Pyr in assembly','Pyr out assembly'})
ylabel('Corr w/ int')
disp('Corr pyr in assembly-int vs pyr out assembly-int')
p = ranksum(assemblies.corrInt.PyrInAssembly,assemblies.corrInt.PyrOutAssembly)

clear pyrInAssembly pyrOutAssembly p
save([resultsPath fileName])

%% Assembly triggered average and interneuron activation
assembly_trig_avg_single (traces, nCells, si_img, SCE, assemblies);
save([resultsPath fileName])


%% PCA/ICA assembly
ica_assembly

clear assemblyCounter
save([resultsPath fileName])

%% Detect sequences

Tr = traces.all(:,:);
Pcn = 7; % Principal component number
detect_sequence











