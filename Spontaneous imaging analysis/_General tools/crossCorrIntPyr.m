interneurons = nCells.class2(1:5);

randomPyr = randi(length(nCells.class1),13,1);
sampledPyr = nCells.class1(randomPyr);

close all;
for internCounter = 1:length(interneurons);
for pyrCounter = sampledPyr;
subplotCounter = find(sampledPyr==pyrCounter);
[r,lags] = xcorr(spikenums.tot.binned(interneurons(internCounter),:),spikenums.tot.binned(pyrCounter,:),50);
figure(internCounter);
subplot(4,4,subplotCounter);
bar(lags,r)
end
end