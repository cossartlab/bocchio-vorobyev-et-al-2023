import sys

sys.path.append('/Users/anaconda/Desktop/Code/EC')
import experiments_code_s2p as ec
from scipy.stats import pearsonr, mannwhitneyu, ttest_ind, kstest
from matplotlib import pyplot as plt
import numpy as np
import seaborn as sns
import pandas as pd
from pathlib import Path
from distutils.log import error
from itertools import chain
from datetime import datetime

TOTAL_LENGTH = 15000

data_path = '/Volumes/2TB_VIRUS/Data/'

exp_type = 'main'
response_range_l = [[0,0], [0,1], [0, 2], [3, 7], [6,10], [7,10], [8, 10], [0, 10], [5, 10], [2, 10], [3, 10], [4, 10], [1,10]]

#exp_type = 'controls'
#response_range_l = [[0, 10]]

e_l = ec.load_experiments(exp_type)

#j = pd.read_excel('/Volumes/2TB_VIRUS/Manual_SCE_check_with_TSNE_n1/SCEs_manual.xlsx')
#e_with_proper_sces = j.loc[np.logical_and(j['Unnamed: 2'] == 'Cicada', j['Unnamed: 1'] >= 2), 'Unnamed: 0'].values

#e_l = [e for e in e_l if int(e.exp_id) in e_with_proper_sces]
#e_l = [e for e in e_l if sum(e.iscell) >= 100]
SAVE_TO = f'SCE_freq_full_analysis/{datetime.now().strftime("%Y%m%d_%H%M%S")}_{exp_type}'


MIN_SCE_PER_EPOCH = 5
SPEED_THR = 2
#Create the SAVE_TO folder if it doesn't exist:
single_cell_stim_only = True

SCE_thr_l = ['5percent', '5cells']

for SCE_thr in SCE_thr_l:
    for response_range in response_range_l:
        title = f"{SCE_thr},{response_range}"
        print(title)
        selected_exp = [i for i in e_l if sum(i.response) in range(response_range[0], response_range[1]+1)]
        if single_cell_stim_only:
            selected_exp = [i for i in selected_exp if i.stim_cell_number_l.size == 1]

        #sessions, sce_frames, sce_cells = ec.sces_from_xlsx("/Volumes/2TB_VIRUS/new_NWB_s2p/SCE_recruitment_table_all.xlsx")

        # Making folders
        Path(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/single_exp/ecdf').mkdir(parents=True, exist_ok=True)

        filtered_exp, _, _, _, baseline_sce, stim_sce, post_stim_sce, baseline_sce_fr, stim_sce_fr, post_stim_sce_fr,_,_,_ = ec.SCE_data_from_excel(
            e_l=selected_exp,
            #excel_path = "/Volumes/2TB_VIRUS/new_NWB_s2p/SCE_recruitment_table_all.xlsx",
            excel_path='/Volumes/2TB_VIRUS/even_newer_NWB/SCEs_Cicada.xlsx',
            min_SCE_per_epoch=5,
            cells_SCE_thr=SCE_thr,
            speed_thr=SPEED_THR
            )

        print(f'Selected experiments:{[i.exp_id for i in filtered_exp]}')
        if not filtered_exp:
            continue

        baseline_inter = [np.diff(baseline_sce_i) for baseline_sce_i in baseline_sce]
        stim_inter = [np.diff(stim_sce_i) for stim_sce_i in stim_sce]
        post_stim_inter = [np.diff(post_stim_sce_i) for post_stim_sce_i in post_stim_sce]

        if len(baseline_inter) == 0:
            continue

        # To seconds from frames
        baseline_sce_fr = [np.array(baseline_sce_i) * 8.41 for baseline_sce_i in baseline_sce_fr]
        stim_sce_fr = [np.array(stim_sce_i) * 8.41 for stim_sce_i in stim_sce_fr]
        post_stim_sce_fr = [np.array(post_stim_sce_i) * 8.41 for post_stim_sce_i in post_stim_sce_fr]

        baseline_inter = [i/8.41 for i in baseline_inter]
        stim_inter = [i/8.41 for i in stim_inter]
        post_stim_inter = [i/8.41 for i in post_stim_inter]

        # Ecdf, single experiments

        b2s_up = []
        b2p_up = []
        s2p_up = []

        b2s_down = []
        b2p_down = []
        s2p_down = []

        for n, e in enumerate(filtered_exp):
            df = pd.DataFrame([
                baseline_inter[n],
                stim_inter[n],
                post_stim_inter[n]
                ]).T
            df.columns = ['Baseline', 'Stimulation', 'Post-stim']

            if not df.empty:
                sns.ecdfplot(df, legend=True, palette=['black', 'red', 'gray'])
                plt.xlabel('inter-SCE interval, frames')
                plt.gcf().suptitle(f'{e.exp_id}, baseline median: {round(np.median(baseline_inter[n])/8.41)}s')
                #plt.gcf().set_size_inches(14, 10)

                try:
                    ks_b_vs_s = round(kstest(baseline_inter[n],  stim_inter[n])[1], 4)
                    plt.text(0.95, 0.70, f'K-S: B vs S: {ks_b_vs_s}', transform=plt.gca().transAxes, ha='right', fontsize=12)
                    ks_b_vs_p = round(kstest(baseline_inter[n],  post_stim_inter[n])[1], 4)
                    plt.text(0.95, 0.65, f'K-S: B vs P: {ks_b_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
                    ks_s_vs_p = round(kstest(stim_inter[n],  post_stim_inter[n])[1], 4)
                    plt.text(0.95, 0.60, f'K-S: S vs P: {ks_s_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)

                    u_b_vs_s = round(mannwhitneyu(baseline_inter[n],  stim_inter[n])[1], 4)
                    plt.text(0.95, 0.55, f'U: B vs S: {u_b_vs_s}', transform=plt.gca().transAxes, ha='right', fontsize=12)
                    u_b_vs_p = round(mannwhitneyu(baseline_inter[n],  post_stim_inter[n])[1], 4)
                    plt.text(0.95, 0.5, f'U: B vs P: {u_b_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
                    u_s_vs_p = round(mannwhitneyu(stim_inter[n],  post_stim_inter[n])[1], 4)
                    plt.text(0.95, 0.45, f'U: S vs P: {u_s_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)

                except error as err:
                    print(f'No text here {e.exp_id}, {err}')
                #plt.style.use(['dark_background'])
                plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/single_exp/ecdf/{e.exp_id}.png')
                plt.close()
                #print(f'Saved {e.exp_id}')

            # Check for statistically significant changes
            for condition, b2s, b2p, s2p in zip(
                ['less', 'greater'],
                [b2s_up, b2s_down],
                [b2p_up, b2p_down],
                [s2p_up, s2p_down]):
                try:
                    if mannwhitneyu(baseline_inter[n],  stim_inter[n], alternative=condition)[1] < 0.05:
                        b2s.append(e)
                    if mannwhitneyu(baseline_inter[n],  post_stim_inter[n], alternative=condition)[1] < 0.05:
                        b2p.append(e)
                    if mannwhitneyu(stim_inter[n],  post_stim_inter[n], alternative=condition)[1] < 0.05:
                        s2p.append(e)
                except TypeError as err:
                    print(err)

        fig, ax = plt.subplots()
        labels = ['Baseline-\nStim', 'Stim-\nPost-Stim','Baseline-\nPost-Stim']

        lens_down = [len(b2s_down),len(b2p_down),len(s2p_down)]
        lens_up = [len(b2s_up),len(b2p_up),len(s2p_up)]

        p1=ax.bar(labels, lens_down, 0.35, label='Significant decrease')
        p2=ax.bar(labels, lens_up, 0.35, bottom=lens_down, label='Significant increase')

        #Matplotlib needs an update?
        #ax.bar_label(p1, label_type='center')
        #ax.bar_label(p2, label_type='center')

        ax.legend()
        ax.set_ylabel('Number of cases')
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/sign_changes_bar.svg')
        plt.close()

        # Ecdf, pulled data
        df = pd.DataFrame([
            list(chain.from_iterable(baseline_inter)),
            list(chain.from_iterable(stim_inter)),
            list(chain.from_iterable(post_stim_inter))
            ]).T
        df.columns = ['Baseline', 'Stimulation', 'Post-stim']

        sns.ecdfplot(df, legend=True, palette=['black', 'red', 'gray'])
        plt.title(f'{title}')
        plt.xlabel('inter-SCE interval, s')

        ks_b_vs_s = round(kstest(list(chain.from_iterable(baseline_inter)),  list(chain.from_iterable(stim_inter)))[1], 3)
        plt.text(0.95, 0.65, f'K-S: B vs S: {ks_b_vs_s}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        #plt.text(0.95, 0.55, f'K-S: B vs S: {ks_b_vs_s}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        ks_b_vs_p = round(kstest(list(chain.from_iterable(baseline_inter)),  list(chain.from_iterable(post_stim_inter)))[1], 3)
        plt.text(0.95, 0.60, f'K-S: B vs P: {ks_b_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        #plt.text(0.95, 0.5, f'K-S: B vs P: {ks_b_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        ks_s_vs_p = round(kstest(list(chain.from_iterable(stim_inter)),  list(chain.from_iterable(post_stim_inter)))[1], 3)
        plt.text(0.95, 0.55, f'K-S: S vs P: {ks_s_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        #plt.text(0.95, 0.45, f'K-S: S vs P: {ks_s_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)


        u_b_vs_s = round(mannwhitneyu(list(chain.from_iterable(baseline_inter)),  list(chain.from_iterable(stim_inter)))[1], 4)
        plt.text(0.95, 0.45, f'U: B vs S: {u_b_vs_s}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        u_b_vs_p = round(mannwhitneyu(list(chain.from_iterable(baseline_inter)),  list(chain.from_iterable(post_stim_inter)))[1], 4)
        plt.text(0.95, 0.4, f'U: B vs P: {u_b_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        u_s_vs_p = round(mannwhitneyu(list(chain.from_iterable(stim_inter)),  list(chain.from_iterable(post_stim_inter)))[1], 4)
        plt.text(0.95, 0.35, f'U: S vs P: {u_s_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)

        plt.xlim(-1, 222)
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/Ecdf_big.svg')
        plt.title(f'{title}, x:[0, 100]')
        plt.xlim(0, 100)
        plt.ylim(0.6, 1)
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/Ecdf_zoomed.svg')
        plt.close()

        # Ecdf, pulled data, inter-SCEs with excluded run

        baseline_inter_no_speed = []
        stim_inter_no_speed = []
        post_stim_inter_no_speed = []

        for baseline_sce_i, stim_sce_i, post_stim_sce_i, e in \
            zip(baseline_sce, stim_sce, post_stim_sce, filtered_exp):
            print(f'Code:Stim_start {e.stim_start}, stim_end {e.stim_end}')
            baseline_sce_binarized = np.zeros(e.stim_start); baseline_sce_binarized[np.array(baseline_sce_i) - 1] = 1
            stim_sce_binarized = np.zeros(e.stim_end-e.stim_start); stim_sce_binarized[np.array(stim_sce_i) - e.stim_start- 1] = 1
            post_stim_sce_binarized = np.zeros(TOTAL_LENGTH-e.stim_end); post_stim_sce_binarized[np.array(post_stim_sce_i) - e.stim_end - 1] = 1

            baseline_sce_binarized = baseline_sce_binarized[e.speed_array[:e.stim_start] <= 2]
            stim_sce_binarized = stim_sce_binarized[e.speed_array[e.stim_start:e.stim_end] <= 2]
            post_stim_sce_binarized = post_stim_sce_binarized[e.speed_array[e.stim_end:] <= 2]

            baseline_inter_no_speed.append(np.diff(np.where(baseline_sce_binarized))[0])
            stim_inter_no_speed.append(np.diff(np.where(stim_sce_binarized))[0])
            post_stim_inter_no_speed.append(np.diff(np.where(post_stim_sce_binarized))[0])

        #print(baseline_inter_no_speed)

        baseline_inter_no_speed = [i/8.41 for i in baseline_inter_no_speed]
        stim_inter_no_speed = [i/8.41 for i in stim_inter_no_speed]
        post_stim_inter_no_speed = [i/8.41 for i in post_stim_inter_no_speed]

        #print(baseline_inter_no_speed)

        df = pd.DataFrame([
            list(chain.from_iterable(baseline_inter_no_speed)),
            list(chain.from_iterable(stim_inter_no_speed)),
            list(chain.from_iterable(post_stim_inter_no_speed))
        ]).T
        df.columns = ['Baseline', 'Stimulation', 'Post-stim']

        sns.ecdfplot(df, legend=True, palette=['black', 'red', 'gray'])
        plt.title(f'{title}')
        plt.xlabel('inter-SCE interval, s')

        ks_b_vs_s = round(kstest(list(chain.from_iterable(baseline_inter_no_speed)),  list(chain.from_iterable(stim_inter_no_speed)))[1], 3)
        plt.text(0.95, 0.65, f'K-S: B vs S: {ks_b_vs_s}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        #plt.text(0.95, 0.55, f'K-S: B vs S: {ks_b_vs_s}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        ks_b_vs_p = round(kstest(list(chain.from_iterable(baseline_inter_no_speed)),  list(chain.from_iterable(post_stim_inter_no_speed)))[1], 3)
        plt.text(0.95, 0.60, f'K-S: B vs P: {ks_b_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        #plt.text(0.95, 0.5, f'K-S: B vs P: {ks_b_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        ks_s_vs_p = round(kstest(list(chain.from_iterable(stim_inter_no_speed)),  list(chain.from_iterable(post_stim_inter_no_speed)))[1], 3)
        plt.text(0.95, 0.55, f'K-S: S vs P: {ks_s_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        #plt.text(0.95, 0.45, f'K-S: S vs P: {ks_s_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)


        u_b_vs_s = round(mannwhitneyu(list(chain.from_iterable(baseline_inter_no_speed)),  list(chain.from_iterable(stim_inter)))[1], 4)
        plt.text(0.95, 0.45, f'U: B vs S: {u_b_vs_s}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        u_b_vs_p = round(mannwhitneyu(list(chain.from_iterable(baseline_inter_no_speed)),  list(chain.from_iterable(post_stim_inter_no_speed)))[1], 4)
        plt.text(0.95, 0.4, f'U: B vs P: {u_b_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        u_s_vs_p = round(mannwhitneyu(list(chain.from_iterable(stim_inter_no_speed)),  list(chain.from_iterable(post_stim_inter_no_speed)))[1], 4)
        plt.text(0.95, 0.35, f'U: S vs P: {u_s_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)

        plt.xlim(-1, 222)
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/Ecdf_big_nospeed.svg')
        plt.title(f'{title}, x:[0, 100]')
        plt.xlim(0, 100)
        plt.ylim(0.6, 1)
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/Ecdf_zoomed_nospeed.svg')
        plt.close()


        # Boxenplot, pulled data

        my_pal = {"Baseline": "dimgrey", "Stimulation": "r"}
        sns.boxenplot(data=df[df.columns[:-1]], palette=my_pal, linewidth=0)
        plt.title(f'{title}')
        plt.ylabel('inter-SCE interval, s')
        plt.ylim(0, 200)
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/Boxenplot.svg')


        # Boxplots for inter SCEs, data pulled

        my_dict = {
            f'Baseline, n={len(list(chain.from_iterable(baseline_inter)))}': list(chain.from_iterable(baseline_inter)),
            f'Stimulation, n={len(list(chain.from_iterable(stim_inter)))}': list(chain.from_iterable(stim_inter)),
            f'Post-stim, n={len(list(chain.from_iterable(post_stim_inter)))}': list(chain.from_iterable(post_stim_inter))}
        fig, ax = plt.subplots()
        fig.set_size_inches(6, 8)

        # No bootstrap
        bp_dict = ax.boxplot(my_dict.values(), showfliers=True)

        # With bootstrap
        #bp_dict = ax.boxplot(my_dict.values(), bootstrap=10000, conf_intervals = [None,None,None], notch=True, showfliers=False)

        for line, mean in zip(
            bp_dict['medians'],
                [
                np.mean(list(chain.from_iterable(baseline_inter))),
                np.mean(list(chain.from_iterable(stim_inter))),
                np.mean(list(chain.from_iterable(post_stim_inter)))
                ]
            ):
            # get position data for median line
            x, y = line.get_xydata()[1] # top of median line
            # overlay median value
            ax.text(x+0.1, y, f'Median:{round(y, 2)}\nMean:{round(mean, 2)}',
                ha='left', va='center', fontsize=8) # draw above, centered

        ax.set_xlim(0.5, 3.7)
        ax.set_xticklabels(my_dict.keys())
        fig.suptitle(f'{title}, inter-SCE pulled data\n{mannwhitneyu(list(chain.from_iterable(baseline_inter)), list(chain.from_iterable(stim_inter)))}')
        plt.ylabel('Inter-SCE interval, s')
        plt.tight_layout()
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/Pulled_interSCE.svg')
        plt.close()


        # Boxplots for inter SCEs, data pulled, no post-stim

        my_dict = {
            f'Baseline': list(chain.from_iterable(baseline_inter)),
            f'Stimulation': list(chain.from_iterable(stim_inter))}
        fig, ax = plt.subplots()
        fig.set_size_inches(2.55, 3.29)


        # No bootstrap
        bp_dict = ax.boxplot(my_dict.values(), widths = 0.3, showfliers=True)

        #ax.set_xlim(0.5, 3.7)
        ax.set_xticklabels(my_dict.keys())
        fig.suptitle(f'{title}, inter-SCE pulled data\n{mannwhitneyu(list(chain.from_iterable(baseline_inter)), list(chain.from_iterable(stim_inter)))}')
        plt.ylabel('Inter-SCE interval (s)')
        plt.locator_params(axis='y', nbins=3)
        plt.tight_layout()
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/Pulled_interSCE_2.svg')
        plt.close()

        change = np.array(stim_sce_fr) - np.array(baseline_sce_fr)
        responses = [sum(i.response) for i in filtered_exp]
        # Linear regression/pearson for baseline fr
        if len(baseline_sce_fr) > 1 and len(change) > 1:
            change = np.array(stim_sce_fr) - np.array(baseline_sce_fr)
            sns.regplot(x=baseline_sce_fr, y=change).set(title=f'Stim SCE freq - Baseline SCE freq VS Baseline SCE freq\n{title}\nPearson r,p:{[round(i,5) for i in pearsonr(baseline_sce_fr, change)]}')
            plt.axhline(y=0, xmin=0, xmax=1, c='red', ls='dashed', alpha=0.7)
            plt.xlabel('Baseline SCEs freq, Hz')
            plt.ylabel('Stim SCEs freq - Baseline SCEs freq, Hz')
            plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/LinReg_change2base.svg')
            plt.close()

        # Linear regression/pearson for change - response
        if len(responses) > 1 and len(change) > 1:
            sns.regplot(x=responses, y=change).set(title=f'Stim SCE freq - Baseline SCE freq VS Stim cell response\n{title}\nPearson r,p:{[round(i,5) for i in pearsonr(baseline_sce_fr, responses)]}')
            #plt.axhline(y=0, xmin=0, xmax=1, c='red', ls='dashed', alpha=0.7)
            plt.xlabel('Responses number')
            plt.ylabel('Stim SCEs freq - Baseline SCEs freq, Hz')
            plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/LinReg_chang2resp.svg')
            plt.close()

        # Linear regression/pearson for poststim - response
        if len(responses) > 1 and len(post_stim_sce_fr) > 1:
            responses = [sum(i.response) for i in filtered_exp]
            sns.regplot(x=responses, y=post_stim_sce_fr).set(title=f'Poststim SCE freq - Stim cell response\n{title}\nPearson r,p:{[round(i,5) for i in pearsonr(baseline_sce_fr, responses)]}')
            #plt.axhline(y=0, xmin=0, xmax=1, c='red', ls='dashed', alpha=0.7)
            plt.xlabel('Responses number')
            plt.ylabel('Poststim SCEs freq, Hz')
            plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/LinReg_post2resp.svg')
            plt.close()

        # Arrows
        if len(baseline_sce_fr) > 1 and len(stim_sce_fr) > 1:
            sorted_sces = sorted(zip(baseline_sce_fr, stim_sce_fr), key=lambda x:abs(x[1] - x[0]), reverse=True)
            for x,y,dx in zip(
                [i[0] for i in sorted_sces],
                np.arange(len(sorted_sces)),
                [i[1] for i in sorted_sces]):
                #plt.arrow(x,y,x-dx,0,color='white')
                plt.annotate("", xy=(x, y), xytext=(dx, y), arrowprops=dict(arrowstyle="->"))
            plt.xlim(np.min(sorted_sces), np.max(sorted_sces))
            plt.ylim(-1, y+1)
            plt.xlabel('SCEs freq, Hz')
            plt.ylabel('Experiment number')
            plt.axvline(x=np.mean(baseline_sce_fr), ymin=0, ymax=y+5, c='red', ls='dashed', alpha=0.7)
            plt.title('Change of SCEs freq from Baseline to Stim')
            plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/changeArrows.svg')
            plt.close()

        # Baseline to stim change with t-test
        stim_minus_bl_mean = np.array(stim_sce_fr) - np.median(baseline_sce_fr)
        plt.hist(stim_minus_bl_mean, bins=100);
        t = ttest_ind(
            [i for i in stim_minus_bl_mean if i > 0],
            [i for i in stim_minus_bl_mean if i < 0], alternative='greater')
        plt.gcf().suptitle(f'Independent t-test between neg change and pos change,\nalt.hypothesis: mean distr of pos modulation is greater:\n{t}')
        plt.gcf().set_size_inches(8,6)
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/change_ttest.svg')
        plt.close()

        my_dict = {
            'Baseline': baseline_sce_fr,
            'Stimulation': stim_sce_fr,
            'Post-stim': post_stim_sce_fr
            }
        fig, ax = plt.subplots()
        fig.set_size_inches(6, 8)
        bp_dict = ax.boxplot(my_dict.values())

        for line, mean in zip(
            bp_dict['medians'],
                [
                np.mean(baseline_sce_fr),
                np.mean(stim_sce_fr),
                np.mean(post_stim_sce_fr)
                ]
            ):
            # get position data for median line
            x, y = line.get_xydata()[1] # top of median line
            # overlay median value
            ax.text(x+0.03, y, f'Median:{round(y, 4)}\nMean:{round(mean, 4)}',
                ha='left', va='center', fontsize=8) # draw above, centered

        u_b_vs_s = round(mannwhitneyu(baseline_sce_fr,  stim_sce_fr)[1], 4)
        plt.text(0.95, 0.55, f'U: B vs S: {u_b_vs_s}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        u_b_vs_p = round(mannwhitneyu(baseline_sce_fr,  post_stim_sce_fr)[1], 4)
        plt.text(0.95, 0.5, f'U: B vs P: {u_b_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)
        u_s_vs_p = round(mannwhitneyu(post_stim_sce_fr,  stim_sce_fr)[1], 4)
        plt.text(0.95, 0.45, f'U: S vs P: {u_s_vs_p}', transform=plt.gca().transAxes, ha='right', fontsize=12)

        ax.set_xlim(0.5, 3.7)
        ax.set_xticklabels(my_dict.keys())
        fig.suptitle(f'{title}, SCE frequency, pulled data')
        plt.ylabel('Frequency, Hz')
        plt.tight_layout()
        plt.ylim(0, 0.1)
        plt.savefig(f'/Users/anaconda/Desktop/Analysis results/{SAVE_TO}/{SCE_thr}/{response_range}/Boxplot_freq.svg')
        plt.close()

print('Done')
#%%
