% separate_plots = true;

r = corr(traces.all(:,mvm.restEpochsIndex)');
% r = corr(traces.all(:,mvm.mvmEpochsIndex)');
% r = corr(traces.all');

distance = pdist(pos.all); %Eucledian distance between cells
distance = squareform(distance);

%colour = lines(length(nCells.int)); % colour code for different cells

r_all = (zeros(length(nCells.pyr),length(nCells.int)));
distance_all = (zeros(length(nCells.pyr),length(nCells.int)));
   
for internCounter = 1:length(nCells.int)
%     if separate_plots == true
%         figure;
%         subplot(3,4,internCounter)
%         hold on;
%     end
    
    for pyrCounter = 1:length(nCells.pyr)
        if nCells.int(internCounter) == nCells.pyr(pyrCounter) % skip correlation of a cell to itself
            continue
        end
        r_all(pyrCounter,internCounter) = r(nCells.int(internCounter),nCells.pyr(pyrCounter));
        distance_all(pyrCounter,internCounter) = distance(nCells.int(internCounter),nCells.pyr(pyrCounter));
        if separate_plots == true
            scatter(distance(nCells.int(internCounter),nCells.pyr(pyrCounter)),r(nCells.int(internCounter),nCells.pyr(pyrCounter)),30,'k')
        %else
         %   scatter(D(nCells.int(class2Counter),nCells.pyr(class1Counter)),r(nCells.int(class2Counter),nCells.pyr(class1Counter)),30,colour(class2Counter,:))
        end
    end
end
    
xlabel('Distance (um)')
ylabel('corr')

cellsToRemove = find(r_all==1 & distance_all==0); %exclude autocorrelations

r_all(cellsToRemove) = [];
distance_all(cellsToRemove) = [];

r_all=reshape(r_all,[1,numel(r_all)]);
distance_all=reshape(distance_all,[1,numel(distance_all)]);

% Linear fit
mdl = fitlm(distance_all,r_all);
h=plot(mdl);
xlabel('Distance (um)')
ylabel('corr')

clear cellCounter cellsToRemove separate_plots pyrCounter intCounter

