% Distance int-pyr in assembly vs int-pyr out assembly
figure
[data1,data2] = extract_variables ('pos',[],resultsPath, 'distIntPyrInAssembly', 'distIntPyrOutAssembly',conditionType,conditionValue);

% find recordings with no assemblies (distIntPyrInAssembly set on zero)
noAssemblyRec = find(cell2mat(data1)==0);

data1 = cell2mat(data1);
data2 = cell2mat(data2);

% remove variables from recordings with no assemblies
data1(noAssemblyRec)=[];
data2(noAssemblyRec)=[];

arrange_boxplot(data1,data2)
p = ranksum(data1,data2);
title('Distance int-pyr in assembly vs int-pyr out assembly')
ylabel('Distance with int (um)')
xticklabels({'pyr in assembly','pyr out assembly'})
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

%% Correlation w/ int of pyr in assemblies vs pyr out assemblies
[pyr_id,int_id] = extract_variables ('nCells',[],resultsPath, 'pyr', 'int',conditionType,conditionValue);
[CellCl] = extract_variables ('assemblies',[],resultsPath, 'CellCl', [],conditionType,conditionValue);
[traces] = extract_variables ('traces',[],resultsPath, 'all', [],conditionType,conditionValue);
[restEpochsIndex] = extract_variables ('mvm',[],resultsPath, 'restEpochsIndex', [],conditionType,conditionValue);

% remove variables from recordings with no assemblies
pyr_id(noAssemblyRec)=[];
int_id(noAssemblyRec)=[];
CellCl(noAssemblyRec)=[];
traces(noAssemblyRec)=[];
restEpochsIndex(noAssemblyRec)=[];

for recCounter = 1:size(pyr_id,2)
    pyrInAssembly = intersect(pyr_id{recCounter},find(mean(CellCl{recCounter},1)));
    pyrOutAssembly = intersect(pyr_id{recCounter},find(~mean(CellCl{recCounter},1)));
%     [~,corrIntPyrInAssembly_tmp] = pairwise_corr (traces{recCounter}(:,restEpochsIndex{recCounter}),pyrInAssembly,int_id{recCounter});
%     [~,corrOutPyrInAssembly_tmp] = pairwise_corr (traces{recCounter}(:,restEpochsIndex{recCounter}),pyrOutAssembly,int_id{recCounter});
    [~,corrIntPyrInAssembly_tmp] = pairwise_corr (traces{recCounter},pyrInAssembly,int_id{recCounter});
    [~,corrOutPyrInAssembly_tmp] = pairwise_corr (traces{recCounter},pyrOutAssembly,int_id{recCounter});
    corrIntPyrInAssembly_tmp2{recCounter} = mean(corrIntPyrInAssembly_tmp,2);
    corrIntPyrOutAssembly_tmp2{recCounter} = mean(corrOutPyrInAssembly_tmp,2);
    corrIntPyrInAssembly(recCounter) = mean(corrIntPyrInAssembly_tmp2{recCounter});
    corrIntPyrOutAssembly(recCounter) = mean(corrIntPyrOutAssembly_tmp2{recCounter});
end

corrIntPyrInAssembly_all = cell2mat(corrIntPyrInAssembly_tmp2');
corrIntPyrOutAssembly_all = cell2mat(corrIntPyrOutAssembly_tmp2');


figure
arrange_boxplot(corrIntPyrInAssembly,corrIntPyrOutAssembly); %boxplot
title('By recording')
ylabel('Corr with int')
xticklabels({'pyr in assembly','pyr out assembly'})
p = ranksum(corrIntPyrInAssembly,corrIntPyrOutAssembly); 
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

figure
arrange_boxplot(corrIntPyrInAssembly_all,corrIntPyrOutAssembly_all); %boxplot
title('By neuron')
ylabel('Corr with int')
xticklabels({'pyr in assembly','pyr out assembly'})
p = ranksum(corrIntPyrInAssembly_all,corrIntPyrOutAssembly_all); 
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Correlation w/ int of pyr in assemblies vs pyr out assemblies
% subplot(122)
% [data1,data2] = extract_variables ('assemblies','corrInt',resultsPath, 'PyrInAssembly', 'PyrOutAssembly',conditionType,conditionValue);
% for recCounter = 1:size(data1,2)
%     data1_mdn(recCounter) = median(data1{recCounter});
%     data2_mdn(recCounter) = median(data2{recCounter});
% end
% arrange_boxplot(data1_mdn,data2_mdn)
% p = ranksum(data1_mdn,data2_mdn);
% title('Corr int-pyr in assembly vs int-pyr out assembly')
% ylabel('Corr to int')
% xticklabels({'pyr in assembly','pyr out assembly'})
% text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')
