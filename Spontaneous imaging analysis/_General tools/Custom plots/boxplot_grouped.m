function [h] = boxplot_grouped(data,group,group1_label,group2_label,color,legend_flag)
%Boxplot for 2 independent variables

% INPUTS        TYPE           DESCRIPTION
%
% group         double         categorical array describing conditions (4 in total) for data
%                              array
% group1_label  cell           Cell array with name of conditions for
%                              independent variable 1
% group1_label  cell           Cell array with 2 conditions for independent variable 2
% color         char           1x4 char array with color for each condition

if nargin < 6
    legend_flag = true;
end

positions = [1 1.25 1.75 2];
boxplot(data,group, 'positions', positions);
%set(gca,'xtick',[mean(positions(1:2)) mean(positions(3:4)) ])
set(gca,'xtick',positions)
set(gca,'xticklabel',{group1_label{1},group1_label{2},group1_label{1},group1_label{2}})
h = findobj(gca,'Tag','Box');
for j=1:length(h)
   patch(get(h(j),'XData'),get(h(j),'YData'),color(j),'FaceAlpha',.5);
end
c = get(gca, 'Children');

if legend_flag == true
    hleg1 = legend(c(2:3), group2_label{1}, group2_label{2} ); 
end

end

