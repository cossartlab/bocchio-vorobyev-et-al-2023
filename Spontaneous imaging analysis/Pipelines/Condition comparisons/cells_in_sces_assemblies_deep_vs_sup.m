% Pyr in SCEs 
[data] = extract_variables ('SCE','cellsInSCE',resultsPath, 'pyr');
[n_cells] = extract_variables ('nCells',[],resultsPath, 'pyr');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = length(data{recCounter})/length(n_cells{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
figure
subplot(221)
arrange_boxplot(data1,data2)
title('Pyr in SCEs')
xticklabels({'deep','sup'})
ylabel('Fraction of cells')
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Int in SCEs 
[data] = extract_variables ('SCE','cellsInSCE',resultsPath, 'int');
[n_cells] = extract_variables ('nCells',[],resultsPath, 'int');
for recCounter = 1:size(data,2)
    data_mdn(recCounter) = length(data{recCounter})/length(n_cells{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(222)
arrange_boxplot(data1,data2)
title('Int in SCEs')
xticklabels({'deep','sup'})
ylabel('Fraction of cells')
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

clearvars -except resultsPath layer

% Pyr in assemblies 
[CellCl] = extract_variables ('assemblies',[],resultsPath, 'CellCl',[]);
[pyr_id,int_id] = extract_variables ('nCells',[],resultsPath, 'pyr', 'int');
for recCounter = 1:size(CellCl,2)
    cellsInAssemblies = find(sum(CellCl{recCounter},1));
    n_pyr_assemblies = length(intersect(cellsInAssemblies,pyr_id{recCounter}));
    data_mdn(recCounter) = n_pyr_assemblies/length(pyr_id{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(223)
arrange_boxplot(data1,data2)
title('Pyr in assemblies')
xticklabels({'deep','sup'})
ylabel('Fraction of cells')
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')

% Int in assemblies 
for recCounter = 1:size(CellCl,2)
    cellsInAssemblies = find(sum(CellCl{recCounter},1));
    n_int_assemblies = length(intersect(cellsInAssemblies,int_id{recCounter}));
    data_mdn(recCounter) = n_int_assemblies/length(int_id{recCounter});
end
data1 = data_mdn(contains(layer,'deep'));
data2 = data_mdn(contains(layer,'sup'));
subplot(224)
arrange_boxplot(data1,data2)
title('Int in assemblies')
xticklabels({'deep','sup'})
ylabel('Fraction of cells')
p = ranksum(data1,data2);
text(0.45,0.9,['p = ' num2str(p)],'Units','normalized')