function pairwise_corrs = pairwise_corr_all (inputMatrix,source_cells,target_cells)

corr_matrix = corr(inputMatrix');

if find(size(source_cells)==1)~=find(size(target_cells)==1) % if source and target vectors don't have same orientation
    source_cells = source_cells';
end

%figure;
imagesc(corr_matrix([target_cells source_cells],[target_cells source_cells]))

%corr_matrix_2 = corr(spike_count([target_cells source_cells],:)');
%figure;imagesc(corr_matrix_2)

for source_cell_n = source_cells
    source_cell_counter = find(source_cells==source_cell_n);
    for target_cell_n = target_cells
        target_cell_counter = find(target_cells==target_cell_n);
        pairwise_corr(target_cell_counter) = corr_matrix(source_cell_n,target_cell_n);
    end
    pairwise_corrs(source_cell_counter) = mean(pairwise_corr,'omitnan');
end



end
    