'''
import sys
sys.path.append('/Users/anaconda/Desktop/Code/EC')
import experiments_code_s2p as ec
'''

f_data_path = '/Volumes/2TB_VIRUS/Data' # Path to data
n_stimulations = 10
STIM_END_CONSTANT = 100

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import ttest_rel, pearsonr
from tqdm import notebook


class Experiment:

    def __init__(self, exp_path, data_table):
        #print(exp_path)
        self.path = exp_path
        self.data_table_path = data_table
        
        self.exp_id = exp_path.split('/')[-2]

        df = pd.read_csv(data_table)
        self.order = df.loc[df['path'] == f"/Users/anaconda/Desktop/Data/{self.exp_id}/", 'Order'].to_numpy()[0]
        self.baseline_id = df.loc[df['id'] == int(self.exp_id), 'baseline_id'].to_numpy()[0]
        if not self.exp_id.isnumeric():
            print(self.exp_id)
            raise TypeError('The folder has an incorrect name. Perhaps, NWB?')
        
        self.iscell = [i[0] for i in np.load(f'{f_data_path}/s2p_processed/{self.exp_id}/suite2p/plane0/iscell.npy')]
        self.raw_traces = np.load(f'{f_data_path}/s2p_processed/{self.exp_id}/suite2p/plane0/F.npy')
        try:
            self.spks = np.load(f'{f_data_path}/s2p_processed/{self.exp_id}/suite2p/plane0/spks_new.npy')
        except FileNotFoundError:
            self.spks = np.load(f'{f_data_path}/s2p_processed/{self.exp_id}/suite2p/plane0/spks.npy')
        self.cell_n = sum(self.iscell)
        self.stat = np.load(f'{f_data_path}/s2p_processed/{self.exp_id}/suite2p/plane0/stat.npy',  allow_pickle=True)
        self.ops = np.load(f'{f_data_path}/s2p_processed/{self.exp_id}/suite2p/plane0/ops.npy', allow_pickle=True).item()
        
        '''
        try:
            self.stim_cell_number_l = np.loadtxt(f'{exp_path}/suite2p_stim.txt', dtype = int, delimiter='\n')
        except OSError as err:
            print(f"Couldnt load stim cell number, {self.exp_id}. Error: {err}")
            self.stim_cell_number_l = None

        try:
            self.int_cell_number_l = np.loadtxt(f'{exp_path}/suite2p_int_rois.txt', dtype = int, delimiter='\n')
        except OSError as err:
            print(f"Couldnt load int cells, {self.exp_id}. Error: {err}")
            self.int_cell_number_l = None
        '''

        self.stim_cell_number_l = self._list_from_str_csv(df.loc[df['id']== int(self.exp_id), 'stim_cell_number'].to_numpy()[0])
        self.int_cell_number_l = self._list_from_str_csv(df.loc[df['id']== int(self.exp_id), 'int_cell_number'].to_numpy()[0])
        self.order = df.loc[df['path'] == f"/Users/anaconda/Desktop/Data/{self.exp_id}/", 'Order'].to_numpy()[0]
        self.genotype = df.loc[df['path'] == f"/Users/anaconda/Desktop/Data/{self.exp_id}/", 'genotype'].values[0]
        self.inj = df.loc[df['path'] == f"/Users/anaconda/Desktop/Data/{self.exp_id}/", 'inj'].values[0]
        self.controls = df.loc[df['path'] == f"/Users/anaconda/Desktop/Data/{self.exp_id}/", 'controls'].values[0]
        self.update_from_dt()

        additional_data = self.path + '/additional_data.csv'
        with open(additional_data, 'r') as additional_data_file:
            ad = pd.read_csv(additional_data_file)
            self.speed_array = ad['speed'].to_numpy()
            if self.stripes:         
                self.stim_array = ad['true_stim'].to_numpy()
                self.stim_epoch = [np.where(self.stim_array)[0][0], np.where(self.stim_array)[0][-1] + 100]
                self.stim_first_frames = stim_starts(self.stim_array)
                self.stim_start = self.stim_first_frames[0]
                self.stim_end = self.stim_first_frames[-1] + STIM_END_CONSTANT
            
        if self.stim_cell_number_l is not None and self.stim_cell_number_l.size == 1:
            self.distance_to_stim = self.calc_dist_from_stim_cell()
            #pass

    @staticmethod
    def _str_to_num(string):
        if not isinstance(string, float) and string != '[]' and string != 0:
            num = [int(i) for i in str(string).replace('[', '').replace(']','').replace('.','').split(',')]
        else:
            num = None
        return num
    
    @staticmethod
    def _iscell_idx(total_cells_number, iscell, idx_l, exp_id, message):
        all_cells_idx_l = np.arange(total_cells_number)
        cleaned_idx_l = all_cells_idx_l[np.array(iscell).astype(bool)]
        new_idx_l = []
        try:
            if idx_l.size == 1:
                new_idx_l.append(np.where(cleaned_idx_l == idx_l)[0][0])
            else:
                for idx in idx_l:
                    new_idx_l.append(np.where(cleaned_idx_l == idx)[0][0])
        except IndexError as ie:
                    print(f'{ie}, {idx_l}, {exp_id}, {message}')
        return np.array(new_idx_l)
    
    @staticmethod
    def _distance(coord1, coord2):
        return ((coord1[0] - coord2[0])**2 + (coord1[1] - coord2[1])**2)**0.5
    
    @staticmethod
    def _get_response_arr(data_table, directory, exp_id):
        if data_table.loc[data_table['path'] == directory, 'response'].values[0] == 0:
            response_arr = None
            print(f'No response data: {exp_id}. Consider .full_response_evaluation')
        else:
            stim_cell_response = data_table.loc[data_table['path'] == directory, 'response'].values[0].split('], [')
            stim_cell_response = [[int(i) for i in i.replace(']', '').replace('[', '').split(',')] for i in stim_cell_response]
            stim_cell_response = np.sum(stim_cell_response, axis=0)
            response_arr = np.where(stim_cell_response > 0, 1, 0).tolist()
        return response_arr

    @staticmethod
    def _list_from_str_csv(string) -> np.array:
        if string == '[]' or string == "['']" or isinstance(string, float):
            return np.array([])
        else:
            list = string.replace('[', '').replace(']', '')
            list = np.array([int(i) for i in list.split(',') if i != ''])
            return list


    # Evaluate every cell starting from the stim one(s) and add the data to the cell
    #def full_response_evaluation(cell_trace_l, iscell, stim_cell_number_l, stim_first_frames, dt_path, exp_id):
    def full_response_evaluation(self):
    
       # T-test for stim cells
        def stim_t_response_evaluation(cell_trace, pos_or_neg, stim_cell_response = np.ones(n_stimulations)):
            if pos_or_neg == 'pos':
                diapazone = 10
                alternative = 'greater'
            if pos_or_neg == 'neg':
                diapazone = 50
                alternative = 'less'
            stim_responses= np.zeros(n_stimulations)
            for n, stim in enumerate(self.stim_first_frames):
                if stim_cell_response[n] == 1:
                    if ttest_rel(
                        cell_trace[stim-diapazone:stim], 
                        cell_trace[stim+1:stim+diapazone+1], 
                        alternative=alternative)[1] > 0.95:
                            stim_responses[n] = 1
            return stim_responses
       
       #Z-score median for all the others
        def zscore_eval(self, method = np.median, diapazone=10):

            def cons_above_tr(array, threshold):
                above = False
                if threshold > 0:
                    for i in range(len(array)-1):
                        if array[i] >= threshold and array[i+1] >= threshold:
                            above = True
                else:
                    for i in range(len(array)-1):
                        if array[i] <= threshold and array[i+1] <= threshold:
                            above = True
                return above

            def zscore_single_eval(stim_first_frames, raw_traces, cell_n, diapazone=10):
                z_score_l = []
                for frame in stim_first_frames:
                    single_stim_line = raw_traces[cell_n][frame-diapazone:frame+diapazone+1]
                    baseAvg = np.mean(single_stim_line[:diapazone])
                    baseStd = np.std(single_stim_line[:diapazone])
                    zScore = (single_stim_line - baseAvg) / baseStd
                    z_score_l.append(zScore)
                return z_score_l

            pos_cells = []
            neg_cells = []

            for cell_n in range(self.raw_traces.shape[0]):
                if self.iscell[cell_n] and cell_n not in self.stim_cell_number_l and np.sum(self.raw_traces[cell_n]) != 0:
                    if self.stim_cell_number_l.size > 1:
                        correlations = []
                        for stim_cell_number in self.stim_cell_number_l:
                            correlations.append(pearsonr(self.raw_traces[cell_n], self.raw_traces[stim_cell_number])[1])
                        if any([i>0.95 for i in correlations]):
                            #print(f'{self.exp_id}, the {cell_n} is too corellated with the stim cell {stim_cell_number}')
                            pass
                        else:
                            z_score = method(zscore_single_eval(self.stim_first_frames, self.raw_traces, cell_n), axis=0)[diapazone:]
                            if cons_above_tr(z_score, -1.65):
                                neg_cells.append(cell_n)
                            if cons_above_tr(z_score, 1.96):
                                pos_cells.append(cell_n)
                            if cons_above_tr(z_score, -1.65) and cons_above_tr(z_score, 1.96):
                                print(f'Exp {self.exp_id} cell {cell_n} is both neg and pos')
                                    
                    else:
                        #if pearsonr(e.raw_traces[cell_n], e.raw_traces[e.stim_cell_number_l])[1] < 0.95: #Allow the high correlations for now
                        if True:
                            z_score = method(zscore_single_eval(self.stim_first_frames, self.raw_traces, cell_n), axis=0)[diapazone:]
                            if cons_above_tr(z_score, -1.65):
                                neg_cells.append(cell_n)
                            if cons_above_tr(z_score, 1.96):
                                pos_cells.append(cell_n)
                            if cons_above_tr(z_score, -1.65) and cons_above_tr(z_score, 1.96):
                                print(f'Exp {self.exp_id} cell {cell_n} is both neg and pos')
                        else:
                            print(f'the {cell_n} is too corellated with the stim cell {e.stim_cell_number_l}')
                            
            return np.array(pos_cells), np.array(neg_cells)
        
        # Start with the stim cell(s):

        stim_cell_response = np.zeros(n_stimulations)
        if self.stim_cell_number_l.size == 1:
            stim_cell_response += stim_t_response_evaluation(self.raw_traces[self.stim_cell_number_l[0]], 'pos')    
        else:
            for stim_cell_number in self.stim_cell_number_l:
                stim_cell_response += stim_t_response_evaluation(self.raw_traces[stim_cell_number], 'pos')
        stim_cell_response = np.where(stim_cell_response>0, 1, 0)

        # For all cells:

        pos_mod, neg_mod = zscore_eval(self)

        '''
        pos_cells_numbers = []
        pos_cell_responses = []
        neg_cells_numbers = []
        neg_cell_responses = []

        for cell_n, cell_trace in enumerate(cell_trace_l):
            if cell_n in stim_cell_number_l:
                continue
            if not iscell[cell_n]:
                continue
            pos_stim_resp = response_evaluation(cell_trace, 'pos', stim_cell_response)
            if sum(pos_stim_resp) >= sum(stim_cell_response)//2:
                pos_cells_numbers.append(cell_n)
                pos_cell_responses.append(pos_cell_responses)
            neg_stim_resp = response_evaluation(cell_trace, 'neg', stim_cell_response)
            if sum(neg_stim_resp) >= sum(stim_cell_response)//2:
                neg_cells_numbers.append(cell_n)
                neg_cell_responses.append(neg_cell_responses)

        '''
        
            #s2p_csv.loc[s2p_csv['path'] == f'/Users/anaconda/Desktop/Data/{e.exp_id}/', 'stim_cell_number'] = np.array2string(stim_cell_number, separator=',').replace(' ', '')
            #s2p_csv.loc[s2p_csv['path'] == f'/Users/anaconda/Desktop/Data/{e.exp_id}/', 'int_cell_number'] = np.array2string(int_cell_number, separator=',').replace(' ', '')
        #s2p_csv.loc[s2p_csv['id'] == self.exp_id, 'pos_mod_cells'] = np.array2string(pos_cells_numbers, separator=',').replace(' ', '')
        #s2p_csv.loc[s2p_csv['id'] == self.exp_id, 'neg_mod_cells'] = np.array2string(neg_cell_responses, separator=',').replace(' ', '')
        
        s2p_csv = pd.read_csv(self.data_table_path)
        s2p_csv.loc[s2p_csv['id'] == int(self.exp_id), 'response'] = np.array2string(stim_cell_response, separator=',').replace(' ', '')
        s2p_csv.loc[s2p_csv['id'] == int(self.exp_id), 'pos_mod_cells'] = np.array2string(pos_mod, separator=',').replace(' ', '')
        s2p_csv.loc[s2p_csv['id'] == int(self.exp_id), 'neg_mod_cells'] = np.array2string(neg_mod, separator=',').replace(' ', '')
        s2p_csv.to_csv(f'{self.data_table_path}', index=False)


    def full_response_evaluation_2(self):

        DIAPAZONE = 10

        # Leave only psth part of the traces
        def extract_psth() -> np.array:
            psth_all = np.empty((self.raw_traces.shape[0], 10, 21))
            for cell_i in range(self.raw_traces.shape[0]):
                for n, stim in enumerate(self.stim_first_frames):
                    psth_all[cell_i][n] = self.raw_traces[cell_i][stim-DIAPAZONE:stim+DIAPAZONE+1]
            return psth_all

        def to_list(cell_n) -> list:
            if isinstance(cell_n, np.ndarray):
                return cell_n.tolist()
            elif isinstance(cell_n, list):
                return cell_n
            elif isinstance(cell_n, int) or isinstance(cell_n, np.int64) or isinstance(cell_n, float):
                return [cell_n]
            else:
                print(f'ERROR, cell_n is wierd: {cell_n}')

        # single psth to z_score
        def psth_to_z_score(psth:np.array) -> np.array:
            before_stim = psth[:DIAPAZONE]
            baseAvg = np.mean(before_stim)
            baseStd = np.std(before_stim)
            z_score = (psth - baseAvg) / baseStd
            return z_score

        def conseq_over_thr(arr, thr) -> int:
            if thr > 0:
                binarized = np.where(arr >= thr, 1, 0)
            else:
                binarized = np.where(arr <= thr, 1, 0)
            max_c_n = 0
            c_n = 0
            for i in binarized:
                if i == 1:
                    c_n += 1
                    if c_n > max_c_n:
                        max_c_n = c_n
                else:
                    c_n = 0
            return max_c_n

        def z_score_av(psth:np.array, method, thr, conseq = 2, cell_n=None) -> np.array:
            
            if cell_n is not None:
                cell_l = to_list(cell_n)
            else:
                cell_l = np.arange(psth.shape[0])

            z_score_l = np.empty((len(cell_l), 21))
            binary_result = np.zeros(len(cell_l))

            for n, cell in enumerate(cell_l):
                z_score = method([psth_to_z_score(i) for i in psth[cell]], axis=0)
                z_score_l[n] = z_score
                if conseq_over_thr(z_score[DIAPAZONE:], thr) >= conseq:
                    #print(f'{conseq_over_thr(z_score[DIAPAZONE:], thr)}, {cell}, {z_score[DIAPAZONE:]}')
                    binary_result[n] = 1
            return binary_result, z_score_l

        def t_test_sixplus(psth, alternative, n_thr, cell_n=None):

            if cell_n is not None:
                cell_l = to_list(cell_n)
            else:
                cell_l = np.arange(psth.shape[0])

            t_test_l = np.empty((len(cell_l), 10))
            binary_result = np.zeros(len(cell_l))

            for n1, cell in enumerate(cell_l):
                for n2, psth_i in enumerate(psth[cell]):
                    before_stim = psth_i[:DIAPAZONE]
                    #print(len(before_stim))
                    after_stim = psth_i[DIAPAZONE+1:]
                    #print(len(after_stim))
                    if ttest_rel(before_stim, after_stim, alternative=alternative)[1] >= 0.95:
                        t_test_l[n1][n2] = 1
                    else:
                        t_test_l[n1][n2] = 0
                if sum(t_test_l[n1]) >= n_thr:
                    binary_result[n1] = 1
            return binary_result, t_test_l

        # Stim cells:
        stim_cell_response = np.zeros((1, 10))
        for stim_cell in self.stim_cell_number_l:
            stim_cell_response += t_test_sixplus(extract_psth(), 'greater', 6, stim_cell)[1]
        stim_cell_response = stim_cell_response.astype(int)[0].tolist()

        pos_mod = []
        neg_mod = []
        for cell_i in range(self.raw_traces.shape[0]):
            if self.iscell[cell_i] and cell_i not in self.stim_cell_number_l:
                if z_score_av(extract_psth(), np.median, 1.96, cell_n=cell_i)[0]:
                    pos_mod.append(cell_i)
                if z_score_av(extract_psth(), np.median, -1.65, cell_n=cell_i)[0]:
                    neg_mod.append(cell_i)
        
        s2p_csv = pd.read_csv(self.data_table_path)
        s2p_csv.loc[s2p_csv['id'] == int(self.exp_id), 'response'] = np.array2string(np.array(stim_cell_response), separator=',').replace(' ', '')
        s2p_csv.loc[s2p_csv['id'] == int(self.exp_id), 'pos_mod_cells'] = np.array2string(np.array(pos_mod), separator=',').replace(' ', '')
        s2p_csv.loc[s2p_csv['id'] == int(self.exp_id), 'neg_mod_cells'] = np.array2string(np.array(neg_mod), separator=',').replace(' ', '')
        s2p_csv.to_csv(f'{self.data_table_path}', index=False)

        print(f'Experiment:{self.exp_id}, stim: {stim_cell_response}, pos_mod: {pos_mod}, neg_mod: {neg_mod}')
        return pos_mod, neg_mod 

    def update_from_dt(self):
        name_for_table = f'/Users/anaconda/Desktop/Data/{self.exp_id}/'
        with open(self.data_table_path , 'r') as data_table_file:
            dt = pd.read_csv(data_table_file)
            self.mouse_id = dt.loc[dt['path'] == name_for_table, 'mouse_id'].to_numpy()[0]
            self.zoom = dt.loc[dt['path'] == name_for_table, 'zoom'].to_numpy()[0]
            self.plain_depth = dt.loc[dt['path'] == name_for_table, 'plain_depth'].to_numpy()[0]
            self.response = self._get_response_arr(dt, name_for_table, self.exp_id)
            self.pos_mod_cells = self._str_to_num(dt.loc[dt['path'] == name_for_table, 'pos_mod_cells'].to_numpy()[0])
            self.neg_mod_cells = self._str_to_num(dt.loc[dt['path'] == name_for_table, 'neg_mod_cells'].to_numpy()[0])
            indirectly_mod_cells = np.concatenate([self.pos_mod_cells, self.neg_mod_cells]) if \
                self.pos_mod_cells and self.neg_mod_cells else self.pos_mod_cells or self.neg_mod_cells or []
            unmod_cell_index_l = np.array(self.iscell); unmod_cell_index_l[indirectly_mod_cells] = 0; unmod_cell_index_l[self.stim_cell_number_l[0]] = 0
            self.unmod_cells = np.where(unmod_cell_index_l > 0)[0]
            #if dt.loc[dt['path'] == name_for_table, 'stim_done'].to_numpy()[0] == 1:
            #    self.stim_done = True
            #else:
            #    self.stim_done = False

            if dt.loc[dt['path'] == name_for_table, 'stripes'].to_numpy()[0] == 1:
                self.stripes = True
            else:
                self.stripes = False
            if not pd.isna(dt.loc[dt['path'] == name_for_table, 'stim_cell_number'].values[0]) and \
                np.array_equal(
                    dt.loc[dt['path'] == name_for_table, 'stim_cell_number'].to_numpy()[0], 
                    self.stim_cell_number_l):
                #str(dt.loc[dt['path'] == name_for_table, 'stim_cell_number'].to_numpy()[0]) != \
                #np.array2string(self.stim_cell_number_l, separator=',').replace(' ', ''):
                wrong = str(dt.loc[dt['path'] == name_for_table, 'stim_cell_number'].to_numpy()[0])
                print(f'{self.exp_id}: wrong stim cell: {wrong} Consider replacing with {self.stim_cell_number_l}...')
                #s2p_csv = pd.read_csv(self.data_table_path)
                #s2p_csv.loc[s2p_csv['path'] == f'/Users/anaconda/Desktop/Data/{self.exp_id}/', 'stim_cell_number'] = \
                #np.array2string(self.stim_cell_number_l, separator=',').replace(' ', '')
                #s2p_csv.loc[s2p_csv['path'] == f'/Users/anaconda/Desktop/Data/{self.exp_id}/', 'int_cell_number'] = \
                #np.array2string(self.int_cell_number_l, separator=',').replace(' ', '')
                #s2p_csv.to_csv(f'{self.data_table_path}', index=False)
            
            if self.stim_cell_number_l is not None:
                self.stim_cell_number_l_iscell = self._iscell_idx(self.raw_traces.shape[0], self.iscell, self.stim_cell_number_l, self.exp_id, 'stim')
            if self.int_cell_number_l is not None:
                self.int_cell_number_l_iscell = self._iscell_idx(self.raw_traces.shape[0], self.iscell, self.int_cell_number_l, self.exp_id, 'int')

    def calc_dist_from_stim_cell(self):
        distance_to_stim = []
        for cell_n in range(self.raw_traces.shape[0]):
            if self.iscell[cell_n] == 0:
                distance_to_stim.append(None)
            else:
                if cell_n == self.stim_cell_number_l[0]:
                    distance_to_stim.append(0)
                else:
                    distance_to_stim.append(self._distance(
                        (np.mean(self.stat[cell_n]['xpix']), np.mean(self.stat[cell_n]['ypix'])),
                        (np.mean(self.stat[self.stim_cell_number_l[0]]['xpix']), np.mean(self.stat[self.stim_cell_number_l[0]]['ypix']))))
        return distance_to_stim

def boxplot_with_median(dict, title, save_to = False, show=True, precision=2):
    fig, ax = plt.subplots()

    ax.boxplot(dict.values())
    bp_dict = ax.boxplot(dict.values(), )

    for line in bp_dict['medians']:
        # get position data for median line
        x, y = line.get_xydata()[1] # top of median line
        # overlay median value
        ax.text(x+0.02, y, f'%.{precision}f' % y,
            horizontalalignment='left') # draw above, centered

    ax.set_xticks(range(1, 1 + len(dict.values()))) # that's a wierd trick but it works
    ax.set_xticklabels(dict.keys())
    fig.suptitle(title)
    plt.tight_layout()
    if save_to:
        plt.savefig(save_to)
    if not show:
        plt.clf()
    return(fig)

def stim_starts(arr):
    stim_starts = []
    for i in range(len(arr)):
        if arr[i] == 1 and arr[i-1] == 0:
            stim_starts.append(i)
    return stim_starts

def sces_from_xlsx(path):
    sce_rec_pd = pd.read_excel(path)
    # Extract session id and corresponding SCE frames
    sessions = []
    sce_frames = []
    sce_cells = []
    for value in np.unique(sce_rec_pd['Session'].values):
        sessions.append(value)
        sce_frames_n = []
        sce_cells_n = []
        for index in sce_rec_pd.index[sce_rec_pd['Session']== value].tolist():
            sce_frames_n.append(sce_rec_pd['SCE_frame'].iloc[index])
            sce_cells_n.append(sce_rec_pd['AllCells_Recruitment_Count'].iloc[index])
        sce_frames.append(sce_frames_n)
        sce_cells.append(sce_cells_n)
    return sessions, sce_frames, sce_cells

def sces_by_epoch(experiments, sessions, sce_frames, condition, minimal_SCEn = 3, ignore_speed=False):

    def movement_prop(experiment, thr=100):
        baseline = 1 - sum(experiment.speed_array[:experiment.stim_epoch[0]] > thr) / len(experiment.speed_array[:experiment.stim_epoch[0]])
        stim = 1 - sum(experiment.speed_array[experiment.stim_epoch[0]:experiment.stim_epoch[1]] > thr) /len(experiment.speed_array[experiment.stim_epoch[0]:experiment.stim_epoch[1]])
        poststim = 1 - sum(experiment.speed_array[experiment.stim_epoch[1]:] > thr) / len(experiment.speed_array[experiment.stim_epoch[1]:])
        return [baseline, stim, poststim]

    def exclude_run(experiment, baseline_sce_step, stim_sce_step, post_stim_sce_step):
        baseline = np.zeros(experiment.stim_epoch[0])
        baseline[baseline_sce_step] = 1

        stim = np.zeros(experiment.stim_epoch[1] - experiment.stim_epoch[0])
        stim[np.array(stim_sce_step) - experiment.stim_epoch[0] - 1] = 1

        poststim = np.zeros(experiment.raw_traces.shape[1] - experiment.stim_epoch[1])
        poststim[np.array(post_stim_sce_step) - experiment.stim_epoch[1] - 1] = 1
        
        return [
            baseline[experiment.speed_array[:experiment.stim_epoch[0]] < 100].nonzero()[0],
            stim[experiment.speed_array[experiment.stim_epoch[0]:experiment.stim_epoch[1]] < 100].nonzero()[0],
            poststim[experiment.speed_array[experiment.stim_epoch[1]:] < 100].nonzero()[0]
        ]

    # Selecting only the experiments that had SCEn > value in each epoch

    baseline_sce = []
    stim_sce = []
    post_stim_sce = []

    baseline_inter = []
    stim_inter = []
    post_stim_inter = []

    move_epochs = []

    selected_exp = []

    for experiment in experiments:
        if condition == 'all':
            baseline_sce_n = 0
            stim_sce_n = 0
            post_stim_sce_n = 0

            baseline_sce_step = []
            stim_sce_step = []
            post_stim_sce_step = []

            for i in range(len(sessions)):
                if str(sessions[i]) == experiment.exp_id:
                    for sce in sce_frames[i]:
                        #if sce > 5000 and sce <= experiment.stim_epoch[0]:
                        if sce <= experiment.stim_epoch[0]:
                            baseline_sce_n += 1
                            baseline_sce_step.append(sce)
                        elif sce > experiment.stim_epoch[0] and sce <= experiment.stim_epoch[1]:
                            stim_sce_n += 1
                            stim_sce_step.append(sce)
                        elif sce > experiment.stim_epoch[1]:
                            post_stim_sce_n += 1
                            post_stim_sce_step.append(sce)

            if (
                baseline_sce_n >= minimal_SCEn and
                stim_sce_n >= minimal_SCEn and
                post_stim_sce_n >= minimal_SCEn):

                move_epochs.append(movement_prop(experiment))

                if ignore_speed:
                    baseline_sce.append(np.array(baseline_sce_n) / experiment.stim_epoch[0])
                    stim_sce.append(np.array(stim_sce_n) / (experiment.stim_epoch[1] - experiment.stim_epoch[0]))   
                    post_stim_sce.append(np.array(post_stim_sce_n) / (experiment.raw_traces.shape[1] - experiment.stim_epoch[1]))

                    baseline_inter.append(np.diff(baseline_sce_step))
                    stim_inter.append(np.diff(stim_sce_step))
                    post_stim_inter.append(np.diff(post_stim_sce_step))

                else:
                    baseline_sce.append(np.array(baseline_sce_n) / experiment.stim_epoch[0] / movement_prop(experiment)[0])
                    stim_sce.append(np.array(stim_sce_n) / (experiment.stim_epoch[1] - experiment.stim_epoch[0]) / movement_prop(experiment)[1])   
                    post_stim_sce.append(np.array(post_stim_sce_n) / (experiment.raw_traces.shape[1] - experiment.stim_epoch[1])/ movement_prop(experiment)[2])
                    
                    # why is this here
                    [x.append(np.diff(y)) for x,y in zip( 
                    [baseline_inter, stim_inter, post_stim_inter],
                    exclude_run(experiment, baseline_sce_step,stim_sce_step,post_stim_sce_step)
                    )]

                selected_exp.append(experiment.exp_id)

                move_epochs.append(movement_prop(experiment))

            else:
                print(f'Experiment {experiment.exp_id} skipped, not enough SCEs:\n {baseline_sce_n}, {stim_sce_n}, {post_stim_sce_n}')

        elif condition == 'single':
            if experiment.stim_cell_number_l.size == 1:
                baseline_sce_n = 0
                stim_sce_n = 0
                post_stim_sce_n = 0

                baseline_sce_step = []
                stim_sce_step = []
                post_stim_sce_step = []

                for i in range(len(sessions)):
                    if str(sessions[i]) == experiment.exp_id:
                        for sce in sce_frames[i]:
                            if sce <= experiment.stim_epoch[0]:
                                baseline_sce_n += 1
                                baseline_sce_step.append(sce)
                            elif sce > experiment.stim_epoch[0] and sce <= experiment.stim_epoch[1]:
                                stim_sce_n += 1
                                stim_sce_step.append(sce)
                            elif sce > experiment.stim_epoch[1]:
                                post_stim_sce_n += 1
                                post_stim_sce_step.append(sce)

                if (
                    baseline_sce_n >= minimal_SCEn and
                    stim_sce_n >= minimal_SCEn and
                    post_stim_sce_n >= minimal_SCEn):

                    move_epochs.append(movement_prop(experiment))

                    if ignore_speed:
                        baseline_sce.append(np.array(baseline_sce_n) / experiment.stim_epoch[0])
                        stim_sce.append(np.array(stim_sce_n) / (experiment.stim_epoch[1] - experiment.stim_epoch[0]))   
                        post_stim_sce.append(np.array(post_stim_sce_n) / (experiment.raw_traces.shape[1] - experiment.stim_epoch[1]))

                        baseline_inter.append(np.diff(baseline_sce_step))
                        stim_inter.append(np.diff(stim_sce_step))
                        post_stim_inter.append(np.diff(post_stim_sce_step))

                    else:
                        baseline_sce.append(np.array(baseline_sce_n) / experiment.stim_epoch[0] / movement_prop(experiment)[0])
                        stim_sce.append(np.array(stim_sce_n) / (experiment.stim_epoch[1] - experiment.stim_epoch[0]) / movement_prop(experiment)[1])   
                        post_stim_sce.append(np.array(post_stim_sce_n) / (experiment.raw_traces.shape[1] - experiment.stim_epoch[1])/ movement_prop(experiment)[2])
                        [x.append(np.diff(y)) for x,y in zip(
                            [baseline_inter, stim_inter, post_stim_inter],
                            exclude_run(experiment, baseline_sce_step,stim_sce_step,post_stim_sce_step)
                            )
                        ]

                    selected_exp.append(experiment.exp_id)

                    move_epochs.append(movement_prop(experiment))

                else:
                    print(f'Experiment {experiment.exp_id} skipped, not enough SCEs:\n {baseline_sce_n}, {stim_sce_n}, {post_stim_sce_n}')
            else:
                    print(f'Experiment {experiment.exp_id} skipped, not correct number of stimulated cells')
        
        elif condition == 'multiple':
            if experiment.stim_cell_number_l.size > 1:
                baseline_sce_n = 0
                stim_sce_n = 0
                post_stim_sce_n = 0

                baseline_sce_step = []
                stim_sce_step = []
                post_stim_sce_step = []

                for i in range(len(sessions)):
                    if str(sessions[i]) == experiment.exp_id:
                        for sce in sce_frames[i]:
                            if sce <= experiment.stim_epoch[0]:
                                baseline_sce_n += 1
                                baseline_sce_step.append(sce)
                            elif sce > experiment.stim_epoch[0] and sce <= experiment.stim_epoch[1]:
                                stim_sce_n += 1
                                stim_sce_step.append(sce)
                            elif sce > experiment.stim_epoch[1]:
                                post_stim_sce_n += 1
                                post_stim_sce_step.append(sce)

                if (
                    baseline_sce_n >= minimal_SCEn and
                    stim_sce_n >= minimal_SCEn and
                    post_stim_sce_n >= minimal_SCEn):

                    move_epochs.append(movement_prop(experiment))

                    if ignore_speed:
                        baseline_sce.append(np.array(baseline_sce_n) / experiment.stim_epoch[0])
                        stim_sce.append(np.array(stim_sce_n) / (experiment.stim_epoch[1] - experiment.stim_epoch[0]))   
                        post_stim_sce.append(np.array(post_stim_sce_n) / (experiment.raw_traces.shape[1] - experiment.stim_epoch[1]))

                        baseline_inter.append(np.diff(baseline_sce_step))
                        stim_inter.append(np.diff(stim_sce_step))
                        post_stim_inter.append(np.diff(post_stim_sce_step))

                    else:
                        baseline_sce.append(np.array(baseline_sce_n) / experiment.stim_epoch[0] / movement_prop(experiment)[0])
                        stim_sce.append(np.array(stim_sce_n) / (experiment.stim_epoch[1] - experiment.stim_epoch[0]) / movement_prop(experiment)[1])   
                        post_stim_sce.append(np.array(post_stim_sce_n) / (experiment.raw_traces.shape[1] - experiment.stim_epoch[1])/ movement_prop(experiment)[2])
                        
                        [x.append(np.diff(y)) for x,y in zip( 
                        [baseline_inter, stim_inter, post_stim_inter],
                        exclude_run(experiment, baseline_sce_step,stim_sce_step,post_stim_sce_step)
                        )]

                    selected_exp.append(experiment.exp_id)

                    move_epochs.append(movement_prop(experiment))

                else:
                    print(f'Experiment {experiment.exp_id} skipped, not enough SCEs:\n {baseline_sce_n}, {stim_sce_n}, {post_stim_sce_n}')
            else:
                    print(f'Experiment {experiment.exp_id} skipped, not correct number of stimulated cells')
    
    print(f'Selected experiments: {len(selected_exp)}')
    return baseline_sce, stim_sce, post_stim_sce, baseline_inter, stim_inter, post_stim_inter, selected_exp, move_epochs


def grouped_by_uniq(experiments, property:str) -> list:
    unique_values = list(set([getattr(e, property) for e in experiments]))
    grouped_experiments = [[] for _ in range(len(unique_values))]
    for n, value in enumerate(unique_values):
        for e in experiments:
            if getattr(e, property) == value:
                grouped_experiments[n].append(e)
    return grouped_experiments

def get_psth(e, diapazone=10) -> np.array:
    # Skip non-cells
    psth_l = []
    for cell_trace in e.raw_traces:
        to_append = []
        for stim_first_frame in e.stim_first_frames:
            to_append.append(cell_trace[stim_first_frame-diapazone:stim_first_frame+diapazone+1])
        psth_l.append(to_append)
    return np.asarray(psth_l)

def iscell_idx(e, idx) -> list:
    result = []
    if isinstance(idx, list):
        idx_l = idx
    else:
        idx_l = [idx]
    all_cells_idx_l = np.arange(e.raw_traces.shape[0])
    cleaned_idx_l = all_cells_idx_l[np.array(e.iscell).astype(bool)]

    for i in idx_l:
        result.append(np.where(cleaned_idx_l == i)[0][0])
    return result

def to_list(cell_n) -> list:

    if isinstance(cell_n, np.ndarray):
        return cell_n.tolist()
    elif isinstance(cell_n, list):
        return cell_n
    elif isinstance(cell_n, int) or isinstance(cell_n, np.int64) or isinstance(cell_n, float):
        return [cell_n]
    else:
        print(f'ERROR, cell_n is wierd: {cell_n}')


def SCE_data_from_excel(e_l, excel_path, min_SCE_per_epoch, cells_SCE_thr, speed_thr=0):

    baseline_sce = []
    stim_sce = []
    post_stim_sce = []

    baseline_sce_amp = []
    stim_sce_amp = []
    post_stim_sce_amp = []

    baseline_sce_fr = []
    stim_sce_fr = []
    post_stim_sce_fr = []

    filtered_exp = []
    filtered_frames = []
    filtered_cell_n = []
    filtered_cell_prop = []
    excel_sces = pd.read_excel(excel_path)

    sce_thr = 5

    for e in e_l:

        frames = []
        cell_n = []
        cell_prop = []
        speed = []

        try:
            # Only cell proportion mask
            if cells_SCE_thr == '5percent':
                cell_n_bin_mask = excel_sces.loc[excel_sces['Session'] == int(e.exp_id), 'AllCells_Recruitment_Prop'].to_numpy() >= sce_thr
            else:
                cell_n_bin_mask = excel_sces.loc[excel_sces['Session'] == int(e.exp_id), 'AllCells_Recruitment_Count'].to_numpy() >= sce_thr
            speed_bin_mask = excel_sces.loc[excel_sces['Session'] == int(e.exp_id), 'Corresponding_speed'].to_numpy() <= speed_thr
            bin_mask = cell_n_bin_mask*speed_bin_mask
            if excel_sces.loc[excel_sces['Session'] == int(e.exp_id), 'AllCells_Recruitment_Count'].to_numpy()[bin_mask].size > 0:
                cell_n = excel_sces.loc[excel_sces['Session'] == int(e.exp_id), 'AllCells_Recruitment_Count'].to_numpy()[bin_mask]
                cell_prop = excel_sces.loc[excel_sces['Session'] == int(e.exp_id), 'AllCells_Recruitment_Prop'].to_numpy()[bin_mask]
                frames = excel_sces.loc[excel_sces['Session'] == int(e.exp_id), 'SCE_frame'].to_numpy()[bin_mask]
                speed = excel_sces.loc[excel_sces['Session'] == int(e.exp_id), 'Corresponding_speed'].to_numpy()[bin_mask]
        except IndexError as err:
            print(f'{e.exp_id}, {err}')

        if len(frames) == 0:
            continue

        '''
        if speed_thr:
            #print(f'Frames:{len(frames)}, speed:{len(speed)}')
            frames = np.array(frames)[np.array(speed) <= speed_thr]
            frames = frames.tolist()
        '''

        stim_start = e.stim_first_frames[0]
        stim_end = e.stim_first_frames[-1] + 100

        baseline_sce_i = [i for i in frames if i <= stim_start]
        stim_sce_i = [i for i in frames if np.logical_and(i > stim_start, i <= stim_end)]
        post_stim_sce_i = [i for i in frames if i > stim_end]

        baseline_sce_amp_i = [i for i, j in zip(cell_n, frames) if j <= stim_start]
        stim_sce_amp_i = [i for i, j in zip(cell_n, frames) if np.logical_and(j > stim_start, j <= stim_end)]
        post_stim_sce_amp_i = [i for i, j in zip(cell_n, frames) if j > stim_end]

        if \
            len(baseline_sce_i) >= min_SCE_per_epoch and\
            len(stim_sce_i) >= min_SCE_per_epoch and\
            len(post_stim_sce_i) >= min_SCE_per_epoch:

            filtered_exp.append(e)
            filtered_cell_n.append(cell_n)
            filtered_cell_prop.append(cell_prop)
            filtered_frames.append(frames)

            baseline_sce.append(baseline_sce_i)
            stim_sce.append(stim_sce_i)
            post_stim_sce.append(post_stim_sce_i)

            sum(e.speed_array[stim_start:stim_end] <= speed_thr)

            baseline_sce_fr.append(len(baseline_sce_i) / sum(e.speed_array[:stim_start] <= speed_thr))
            stim_sce_fr.append(len(stim_sce_i) / sum(e.speed_array[stim_start:stim_end] <= speed_thr))
            post_stim_sce_fr.append(len(post_stim_sce_i) / sum(e.speed_array[stim_end:] <= speed_thr))

            baseline_sce_amp.append(baseline_sce_amp_i)
            stim_sce_amp.append(stim_sce_amp_i)
            post_stim_sce_amp.append(post_stim_sce_amp_i)

    return \
        filtered_exp, filtered_cell_n, filtered_cell_prop, filtered_frames, baseline_sce, stim_sce, post_stim_sce, baseline_sce_fr, stim_sce_fr, post_stim_sce_fr, baseline_sce_amp, stim_sce_amp, post_stim_sce_amp


def load_experiments(e_type, data_path='/Volumes/2TB_VIRUS/Data/', data_csv_name='data_table.csv', show_errors=False) \
        -> list:

    if e_type not in ['main', 'all', 'multistim', 'controls']:
        print(f'Wrong e_type string: {e_type}')
        return ValueError
    e_l = []
    dublicates = [394, 291, 292, 175, 369, 341, 200, 230, 246, 261, 297, 309, 201, 248, 212, 273, 211, 245, 274, 222, 260, 298, 308, 259, 307, 249, 272]
    interference = [379, 407, 408, 409, 410, 411, 441, 446, 447, 448, 450, 451, 453, 454,460, 461, 475, 483, 484,486]
    # potentially 397-400, 462
    # 447, 448 - might keep in fact
    # 475  - increase in signal
    # 486 almost no signal at all
    z_drift = [190, 191, 192, 193, 467, 468, 469, 477,478,505,506,507,509,510,511]
    # potentially 471,472,473, 513,514,515
    # might keep 477,478,
    all_experiments_str = [i.split('/')[-2] for i in pd.read_csv(f'{data_path}data_table.csv')['path'] if not \
        ((int(i.split('/')[-2]) in dublicates) or \
         (int(i.split('/')[-2]) in interference) or \
         (int(i.split('/')[-2]) in z_drift))]

    for e in notebook.tqdm(all_experiments_str):
        try:
            e_l.append(Experiment(f'{data_path}{e}/', f'{data_path}{data_csv_name}'))
        except OSError as err:
            if show_errors:
                print(err)
            else:
                pass
    e_l.sort(key=lambda x: int(x.exp_id))

    if e_type == 'main':
        # Remove controls
        e_l = [e for e in e_l if e.controls == 0]
        # Remove multistim
        e_l = [e for e in e_l if e.stim_cell_number_l.size == 1]

    elif e_type == 'all':
        pass

    elif e_type == 'multistim':
        # Remove controls
        e_l = [e for e in e_l if e.controls == 0]
        # Remove single-stim
        e_l = [e for e in e_l if e.stim_cell_number_l.size > 1]

    elif e_type == 'controls':
        e_l = [e for e in e_l if e.controls == 1]

    print(f'{len(e_l)} {e_type} experiments loaded')
    return e_l
