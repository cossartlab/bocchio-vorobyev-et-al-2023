% %assembly1on = SCE.TRace(find(assemblies.PCl(1,:)));
% %raster(assembly1on) = 1;
% raster(find(SCE.RasterRace(8,:))) = 1;
event = zeros(1,length(traces.all));
%event(find(SCE.RasterRace(8,:))) = 1;

assembly_active = SCE.TRace(find(assemblies.PCl(1,:)));
int_active = find(SCE.RasterRace(8,:));

assembly_and_int = intersect(assembly_active,int_active); % interneuron in assembly active
assembly_no_int = assembly_active;
assembly_no_int(ismember(assembly_no_int,assembly_and_int)) = [];% interneuron in assembly not active

event(assembly_no_int) = 1;
%event(assembly_and_int) = 1;


%sameAssemblyActiv = SCE.TRace(find(assemblies.PCl(1,:)));
%otherAssemblyActiv = SCE.TRace(find(assemblies.PCl(2,:)));


signal = traces.all(assemblies.C0{1},:);
signal = signal(2,:);

%signal = signal(2:end,:);
windowSize = 200;


ev_trig_stagger(event, signal, windowSize,'trial');
%ev_trig_stagger(event, signal, windowSize,'cell');





